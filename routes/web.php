<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AdminProductClickController;
use App\Http\Controllers\AffiliateLinkController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductClickController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SitemapXmlController;

/*Route::get('/photo/{size}/{name}', function ($size = null, $name = null) {
    if (! is_null($size) && ! is_null($name)) {
        $size = explode('x', $size);
        $cache_image = Image::cache(function ($image) use ($size, $name) {
            return $image->make(url('/images/'.$name))->resize($size[0], $size[1]);
        }, 60 * 24); // cache for 24 hours

        return Response::make($cache_image, 200, ['Content-Type' => 'image']);
    } else {
        abort(404);
    }
});*/

Route::resource('categories', 'CategoryController');
Route::resource('amazon', 'AmazonController');
Route::resource('amazonlink', 'AmazonLinkController');
Route::resource('bol', 'BolController');
Route::resource('brands', 'BrandController');
Route::get('/affiliate-link/{id}', [AffiliateLinkController::class, 'create'])
    ->name('affiliate-link.create');
Route::post('/affiliate-link/{id}', [AffiliateLinkController::class, 'store'])
    ->name('affiliate-link.store');
Route::group(
  [
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
  ],
  function () {
      /* ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
      Route::resource('products', 'ProductController');
      Route::resource('amazon_products', 'AmazonProductController');
      Route::resource('sitestripe', 'SiteStripeController');
      Route::resource('articles', 'ArticleController');
      Route::resource('links', 'LinkController');
      Route::get('/', 'HomeController@show')->name('home');
      Route::resource('categories', 'CategoryController');
      Route::resource('brands', 'BrandController');
      Route::get('contact-us', 'ContactUSController@contactUS');
      Route::post('contact-us', ['as'=>'contactus.store', 'uses'=>'ContactUSController@contactUSPost']);
      Route::get('/search', [ProductController::class, 'search'])->name('products.search'); // Add this line
      Route::get('/products/tag/{tag}', [ProductController::class, 'byTag'])->name('products.by.tag');

  });

Auth::routes();

Route::resource('producttranslations', 'ProductTranslationsController');


Route::get('contact-us', 'ContactUSController@contactUS');
Route::get('browse-nodes', 'AmazonLinkController@showBrowseNodes');
Route::post('contact-us', ['as'=>'contactus.store', 'uses'=>'ContactUSController@contactUSPost']);
Route::post('cookie', 'CookieController@create');

Route::get('/sitemap.xml', [SitemapXmlController::class, 'index']);

Route::post('/track-click', [ProductClickController::class, 'trackClick'])
    ->name('track.click')
    ->withoutMiddleware(['auth']);


Route::group(['middleware' => ['auth']], function () {
    //Route::get('/admin/product-clicks', [AdminProductClickController::class, 'index'])
    //    ->name('admin.product-clicks');
    Route::get('/admin/popular-products', [AdminProductClickController::class, 'popularProducts'])->name('admin.popular-products');
    //Route::get('/admin/tracking-dashboard', [DashboardController::class, 'index'])->name('admin.analytics');
    Route::get('/admin/user-session/{id}', [AdminProductClickController::class, 'showUserSession'])->name('admin.user-session.show');
    Route::get('/admin/search-activity', [AdminProductClickController::class, 'searchActivity'])->name('admin.search-activity');
});


