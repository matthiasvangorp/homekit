<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('products')) {
            //Insert data into the products table
            DB::table('products')->insert(
          [
            [
              'title' => 'August Smart Lock',
              'description' => 'Never copy a key again. Create virtual keys for family and guests. Choose to grant access for a few weeks, a few hours, a few minutes, or even specific dates and times. Delete guest access any time in an instant.',
              'image' => 'august_smart_lock.jpg',
              'link' => 'https://www.amazon.de/August-Lock-Champagner-Bluetooth-aktiviert-Android-ASL-4/dp/B00QU7AMQ4/ref=sr_1_2?ie=UTF8&qid=1495090730&sr=8-2&keywords=august+smart+lock',
            ],
            [
              'title' => 'Ecobee3',
              'description' => 'The smarter wi-fi thermostat with room sensors. For homes with more than one room.',
              'image' => 'ecobee_3.jpg',
              'link' => 'https://www.amazon.de/ecobee3-Smarter-Thermostat-Generation-ecobee/dp/B00ZIRV39M/ref=sr_1_1?ie=UTF8&qid=1495090952&sr=8-1&keywords=ecobee',
            ],
          ]
        );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Empty the products database
        if (Schema::hasTable('products')) {
            DB::table('products')->delete();
        }
    }
}
