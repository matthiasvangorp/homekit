<?php

use App\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //set the category_id and brand_id
        Product::where('id', 1)->update(['category_id' => 3, 'brand_id' => 1]);
        Product::where('id', 2)->update(['category_id' => 1, 'brand_id' => 2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Product::where('id', 1)->update(['category_id' => 0, 'brand_id' => 0]);
        Product::where('id', 2)->update(['category_id' => 0, 'brand_id' => 0]);
    }
}
