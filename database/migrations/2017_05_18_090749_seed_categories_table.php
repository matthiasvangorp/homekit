<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('categories')) {
            //Insert data into the products table
            DB::table('categories')->insert(
        [
          [
            'name' => 'Thermostats',
            'description' => '',
            'image' => 'thermostats.jpg',
          ],
          [
            'title' => 'Lights',
            'description' => '',
            'image' => 'lights.jpg',
          ],
          [
            'title' => 'Locks',
            'description' => '',
            'image' => 'locks.jpg',
          ],
        ]
      );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Empty the categories database
        if (Schema::hasTable('categories')) {
            DB::table('categories')->delete();
        }
    }
}
