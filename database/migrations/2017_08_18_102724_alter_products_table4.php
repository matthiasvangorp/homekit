<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsTable4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->string('link_amazon', 1024)->default('')->after('link');
            $table->renameColumn('link', 'link_bol');
            $table->string('link_coolblue', 1024)->default('')->after('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->renameColumn('link_bol', 'link');
            $table->dropColumn('link_coolblue');
            $table->dropColumn('link_amazon');
        });
    }
}
