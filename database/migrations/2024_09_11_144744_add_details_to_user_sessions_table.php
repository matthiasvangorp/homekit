<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDetailsToUserSessionsTable extends Migration
{
    public function up()
    {
        Schema::table('user_sessions', function (Blueprint $table) {
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('browser_locale')->nullable();
            $table->string('browser')->nullable();
            $table->string('device_type')->nullable();
            $table->string('os')->nullable();
        });
    }

    public function down()
    {
        Schema::table('user_sessions', function (Blueprint $table) {
            $table->dropColumn(['country', 'city', 'browser_locale', 'browser', 'device_type', 'os']);
        });
    }
}