<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffiliateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('affiliate_links'); // drop table if it exists

        Schema::create('affiliate_links', function (Blueprint $table) {
            $table->id();
            $table->string('amazon_product_id');
            $table->string('url');
            $table->string('ean')->nullable();
            $table->string('asin')->nullable();
            $table->string('locale');
            $table->string('country');
            $table->string('currency');
            $table->decimal('price', 10, 2); // 10 digits with 2 decimal places
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliate_links');
    }
}

