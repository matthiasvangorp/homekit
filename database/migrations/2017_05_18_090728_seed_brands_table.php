<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('brands')) {
            //Insert data into the products table
            DB::table('brands')->insert(
          [
            [
              'name' => 'August',
              'description' => '',
              'image' => 'august_logo.jpg',
              'link' => 'http://august.com',
            ],
            [
              'title' => 'Ecobee',
              'description' => '',
              'image' => 'ecobee_logo.jpg',
              'link' => 'https://www.ecobee.com',
            ],
          ]
        );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Empty the brands database
        if (Schema::hasTable('brands')) {
            DB::table('brands')->delete();
        }
    }
}
