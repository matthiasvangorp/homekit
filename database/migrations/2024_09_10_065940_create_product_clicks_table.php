<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductClicksTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('amazon_products')) {
            throw new \Exception('The amazon_products table does not exist. Please run the amazon_products table migration first.');
        }

        Schema::create('product_clicks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('amazon_product_id');
            $table->string('country', 2);
            $table->timestamp('clicked_at');

            // Check if the amazon_products table has an 'id' column of the correct type
            if (Schema::hasColumn('amazon_products', 'id')) {
                $amazonProductsIdColumnType = Schema::getColumnType('amazon_products', 'id');
                if ($amazonProductsIdColumnType === 'bigint') {
                    $table->foreign('amazon_product_id')
                        ->references('id')
                        ->on('amazon_products')
                        ->onDelete('cascade');
                } else {
                    throw new \Exception("The 'id' column in the amazon_products table is not of type BIGINT. Found type: {$amazonProductsIdColumnType}");
                }
            } else {
                throw new \Exception("The 'id' column does not exist in the amazon_products table.");
            }
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_clicks');
    }
}