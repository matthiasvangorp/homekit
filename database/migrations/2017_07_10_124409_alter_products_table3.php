<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->double('score_amazon')->default(0)->after('link');
            $table->double('score_bol')->default(0)->after('link');
            $table->double('score_coolblue')->default(0)->after('link');
            $table->double('price_amazon')->default(0)->after('link');
            $table->double('price_bol')->default(0)->after('link');
            $table->double('price_coolblue')->default(0)->after('link');
            $table->boolean('needs_hub')->default(0)->after('link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['score_amazon', 'score_bol', 'score_coolblue', 'price_amazon', 'price_bol', 'price_coolblue', 'needs_hub']);
        });
    }
}
