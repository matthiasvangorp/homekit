<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_clicks', function (Blueprint $table) {
            $table->string('tracking_id', 36)->change();
        });
    }

    public function down()
    {
        Schema::table('product_clicks', function (Blueprint $table) {
            $table->string('tracking_id')->change();
        });
    }
};
