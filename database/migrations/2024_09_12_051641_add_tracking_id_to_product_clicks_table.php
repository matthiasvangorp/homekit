<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_clicks', function (Blueprint $table) {
            $table->uuid('tracking_id')->after('country');
        });
    }

    public function down()
    {
        Schema::table('product_clicks', function (Blueprint $table) {
            $table->dropColumn('tracking_id');
        });
    }
};
