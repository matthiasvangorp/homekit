<?php

use App\Models\AmazonProduct;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCountryToAmazonProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amazon_products', function (Blueprint $table) {
            $table->string('country')->after('locale')->nullable();
        });

        $products = AmazonProduct::where('locale', 'DE')->get();
        foreach ($products as $product) {
            $product->country = 'DE';
            $product->save();
        }
        $products = AmazonProduct::where('locale', 'NL')->get();
        foreach ($products as $product) {
            $product->country = 'DE';
            $product->save();
        }
        $products = AmazonProduct::where('locale', 'EN')->get();
        foreach ($products as $product) {
            $product->country = 'DE';
            $product->save();
        }
        $products = AmazonProduct::where('locale', 'FR')->get();
        foreach ($products as $product) {
            $product->country = 'FR';
            $product->save();
        }

        $products = AmazonProduct::where('locale', 'ES')->get();
        foreach ($products as $product) {
            $product->country = 'ES';
            $product->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amazon_products', function (Blueprint $table) {
            $table->dropColumn('country');
        });
    }
}
