<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IncreaseUrlLengthOnAmazonProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amazon_products', function (Blueprint $table) {
            $table->string('url', 2048)->change(); // Change the 2048 to your desired character limit
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amazon_products', function (Blueprint $table) {
            $table->string('url', 191)->change(); // Change the 2048 to your desired character limit
        });
    }
}
