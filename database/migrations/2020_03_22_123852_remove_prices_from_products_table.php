<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePricesFromProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['price_conrad', 'price_coolblue', 'price_bol', 'price_amazon']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->double('price_conrad')->after('needs_hub');
            $table->double('price_coolblue')->after('price_conrad');
            $table->double('price_bol')->after('price_coolblue');
            $table->double('price_amazon')->after('price_bol');
        });
    }
}
