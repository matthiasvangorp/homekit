<?php

use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    /**
     * HomeControllerTest constructor.
     */
    public function __construct()
    {
        // We have no interest in testing Eloquent
        $this->mock = Mockery::mock('Eloquent', 'ProductRepository');
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function testIndex()
    {
        $this->call('GET', '');
        $this->assertViewHas('products');
    }
}
