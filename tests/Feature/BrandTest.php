<?php

namespace Tests\Feature;

use App\Models\Brand;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class BrandTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return void
     */
    public function testBrandsWithMiddleware()
    {
        $response = $this->get('/brands');
        $response->assertStatus(302);
    }

    /**
     * @return void
     */
    public function testHomepageWithoutMiddleware()
    {
        $this->withoutMiddleware();
        $response = $this->get('/brands');
        $response->assertStatus(200)->assertSee('Create new brand');
    }

    /**
     * @return void
     */
    public function testCreateBrandWithoutMiddleWare()
    {
        $this->withoutMiddleware();
        $response = $this->json('POST', '/brands', ['name'=>'testbrand', 'description'=>'test description', 'link' => 'http://www.testbrand.com']);
        $this->assertDatabaseHas('brands', ['name' => 'testbrand']);
        $brands = Brand::where('name', 'testbrand')->get();
        foreach ($brands as $brand) {
            $this->assertTrue($brand->name == 'testbrand');
            $this->assertTrue($brand->description == 'test description');
            $this->assertTrue($brand->link == 'http://www.testbrand.com');
        }
    }

    /**
     * @return void
     */
    public function testCreateBrandWithoutMiddleWareFail()
    {
        $this->withoutMiddleware();
        $response = $this->json('POST', '/brands', ['qsdf'=>'testbrand', 'description'=>'test description fail', 'link' => 'http://www.testbrand.com']);
        $this->assertDatabaseMissing('brands', ['description' => 'test description fail']);
        $brands = Brand::where('name', 'testbrand')->get();
    }

    /**
     * @return void
     */
    public function testUpdateBrandWithoutMiddleWare()
    {
        $this->withoutMiddleware();
        $brands = Brand::where('name', 'testbrand')->get();

        foreach ($brands as $brand) {
            $id = $brand->id;
            $response = $this->json('PUT', '/brands/'.$id, ['name'=>'testbrand update', 'description'=>'test description', 'link' => 'http://www.test.com']);
            $updatedBrand = Brand::find($id);
            $this->assertTrue($updatedBrand->name == 'testbrand update');
        }
    }

    /**
     * @return void
     */
    public function testUpdateBrandWithoutMiddleWareFail()
    {
        $this->withoutMiddleware();
        $brands = Brand::where('name', 'testbrand update')->get();

        foreach ($brands as $brand) {
            $id = $brand->id;
            $response = $this->json('PUT', '/brands/'.$id, ['qsqss'=>'testbrand update fail', 'description'=>'test description fail', 'link' => 'http://www.test.com']);
            $this->assertDatabaseMissing('brands', ['description' => 'test description fail']);
        }
    }

    /**
     * @return void
     */
    public function testShowBrand()
    {
        $this->withoutMiddleware();
        $brands = Brand::where('name', 'testbrand update')->get();
        foreach ($brands as $brand) {
            $id = $brand->id;

            $this->get('/brands/'.$id)->assertSee('testbrand update');
        }
    }

    /**
     * @return void
     */
    public function testShowBrands()
    {
        $this->withoutMiddleware();
        $this->get('/brands/')->assertSee('Create new brand');
    }

    /**
     * @return void
     */
    public function testDeleteBrandWithoutMiddleWare()
    {
        $this->withoutMiddleware();
        $brands = Brand::where('name', 'testbrand update')->get();
        foreach ($brands as $brand) {
            $id = $brand->id;
            $response = $this->json('DELETE', '/brands/'.$id, []);
            $response->assertStatus(302)->assertSee('Redirecting to');
        }
    }

    /**
     * @return void
     */
    public function showCreateBrand()
    {
        $this->withoutMiddleware();
        $this->get('/brands/create')->assertSee('Create brand');
    }
}
