<?php

namespace Tests\Feature;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return void
     */
    public function testProductsWithMiddleware()
    {
        $response = $this->get('/products');
        $response->assertStatus(302);
    }

    /**
     * @return void
     */
    public function testHomepageWithoutMiddleware()
    {
        $this->withoutMiddleware();
        $response = $this->get('/products');
        $response->assertStatus(200)->assertSee('Create new product');
    }

    /**
     * @return void
     */
    public function testCreateProductWithoutMiddleWare()
    {
        $this->withoutMiddleware();
        $response = $this->json('POST', '/products', ['title'=>'testproduct', 'description'=>'test description', 'category' => '1', 'brand' => '1', 'link' => 'http://www.test.com']);
        $this->assertDatabaseHas('products', ['title' => 'testproduct']);
        $products = Product::where('title', 'testproduct')->get();
        foreach ($products as $product) {
            $this->assertTrue($product->title == 'testproduct');
            $this->assertTrue($product->description == 'test description');
            $this->assertTrue($product->link == 'http://www.test.com');
        }
    }

    /**
     * @return void
     */
    public function testUpdateProductWithoutMiddleWare()
    {
        $this->withoutMiddleware();
        $products = Product::where('title', 'testproduct')->get();

        foreach ($products as $product) {
            $id = $product->id;
            $response = $this->json('PUT', '/products/'.$id, ['title'=>'testproduct update', 'description'=>'test description', 'category' => '1', 'brand' => '1', 'link' => 'http://www.test.com']);
            $updatedProduct = Product::find($id);
            $this->assertTrue($updatedProduct->title == 'testproduct update');
        }
    }

    /**
     * @return void
     */
    public function testShowProduct()
    {
        $this->withoutMiddleware();
        $products = Product::where('title', 'testproduct update')->get();
        foreach ($products as $product) {
            $id = $product->id;

            $this->get('/products/'.$id)->assertSee('testproduct update');
        }
    }

    /**
     * @return void
     */
    public function testShowProducts()
    {
        $this->withoutMiddleware();
        $this->get('/products/')->assertSee('Create new product');
    }

    /**
     * @return void
     */
    public function testDeleteProductWithoutMiddleWare()
    {
        $this->withoutMiddleware();
        $products = Product::where('title', 'testproduct update')->get();
        foreach ($products as $product) {
            $id = $product->id;
            $response = $this->json('DELETE', '/products/'.$id, []);
            $response->assertStatus(302)->assertSee('Redirecting to');
        }
    }

    /**
     * @return void
     */
    public function showCreateProduct()
    {
        $this->withoutMiddleware();
        $this->get('/products/create')->assertSee('Create product');
    }
}
