$(document).ready(function() {
    $('.product').click(function () {
        var isMobile = window.matchMedia("only screen and (max-width: 760px)");

        $('#modal-title').text($(this).find('h4').text());
        var imageSrc = $(this).find('img').attr('src');
        var url = $(this).find('a').attr('href');
        var bodyHTMl = "<a href='"+url+"'>";
        var bodyHTMl = bodyHTMl + "<div><img src='"+imageSrc+"'/></div>";
        if (!isMobile.matches) {
            bodyHTMl = bodyHTMl.replace("320x320", "550x550");
        }
        bodyHTMl = bodyHTMl + $(this).find('#product-description').text() + "</a>";
        $('#modal-body').html(bodyHTMl);
        $('#productModal').modal('show');
    });
});

