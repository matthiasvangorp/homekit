$(document).ready(function() {
    $('#btn-add-brand').click(function(){
        //get the parent div
        var div = $(this).parent();
        $('#modalBrands').modal('show');
    });


    $('#btn-add-category').click(function(){
        //get the parent div
        var div = $(this).parent();
        $('#modalCategories').modal('show');
    });




    $(".ajaxbutton").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')

            }
        })

        e.preventDefault();

        switch (this.id){
            case "btn-save-category":
                var formData = {
                    name: $('#categoryName').val(),
                    description: $('#categoryDescription').val(),
                }
                var my_url = "/en/categories";
                var category = true;
                var selectbox = $('#category');
                break;
            case "btn-save-brand":
                var formData = {
                    name: $('#brandName').val(),
                    description: $('#brandDescription').val(),
                    link: $('#brandLink').val(),
                };
                var my_url = "/en/brands";
                var brand = true;
                var selectbox = $('#brand');
                break;
        }
        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $(this).val();

        var type = "POST"; //for creating new resource

        /*if (state == "update"){
            type = "PUT"; //for updating existing resource
            my_url += '/' + task_id;
        }*/

        console.log(formData);

        $.ajax({

            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data);

                //refill the selectbox
                $.ajax({
                    type: "GET",
                    url: my_url,
                    success: function (data) {
                        selectbox.empty();
                        var list = '';
                        $.each(data, function(index, element){
                            list += "<option value='" + element.id + "'>" + element.name +"</option>";
                        });
                        selectbox.html(list);
                        $('#modalBrands').modal('hide');
                        $('#modalCategories').modal('hide');
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                })

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });



    $('#countries').multiselect();

    if ($('#article').length) {
        CKEDITOR.replace('article');
    }


    var next = 1;
    $(".add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        next = next + 1;
        var newIn = $('div.affiliate_link:last');
        $("<hr>").insertBefore('.add-more');
        $(newIn).clone().insertBefore('.add-more');
        //set affiliate link id to "" so we know it's new

    });
});

function deleteLink(id){
    $('#'+id).remove();
}