$(document).ready(function() {
    $('#btn-localization').click(function (){
        $('#localizationModal').modal('show');
    });


    $("#btn-save-localization").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')

            }
        })

        e.preventDefault();

        var state = $(this).val();
        var type = "POST"; //for creating new resource

        var my_url = "/cookie";
        var return_url = $('#language').val();

        var formData = {
            country: $('#country').val(),
        };

        $.ajax({

            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                //redirect to localised url
                $(location).attr('href', return_url);
            },
            error: function (data) {
                $(location).attr('href', return_url);
            }
        });
    });
});




function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')

        }
    });


    //var state = $(this).val();
    var type = "POST"; //for creating new resource

    var my_url = "/cookie";


    $.ajax({

        type: type,
        url: my_url,
        data: {'country' : value},
        dataType: 'json',
        success: function (data) {
            alert(data)
        },
        error: function (data) {
            $("h1:first").after("<div class='alert alert-danger'>"+data.responseJSON+"</div>");
            console.log('Error:', data);
        }
    });
}