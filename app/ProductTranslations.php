<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTranslations extends Model
{
    protected $primaryKey = 'id';

    public $incrementing = false;

    //
    public function product()
    {
        return $this->belongsTo(\App\Product::class);
    }
}
