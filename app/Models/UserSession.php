<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSession extends Model
{
    protected $table = 'user_sessions';

    // This ensures that created_at and updated_at are automatically managed
    public $timestamps = true;

    // Define which attributes should be treated as dates
    protected $dates = [
        'entry_time',
        'created_at',
        'updated_at',
    ];

    // Define fillable attributes if you plan to use mass assignment
    protected $fillable = [
        'tracking_id',
        'entry_time',
        'entry_page',
        'referrer',
        'ip_address',
        'user_agent',
        'country',
        'city',
        'browser_locale',
        'browser',
        'device_type',
        'os',
    ];

    // You can add any additional methods or relationships here
}
