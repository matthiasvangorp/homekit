<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductClick extends Model
{
    use HasFactory;

    protected $fillable = ['amazon_product_id', 'country', 'tracking_id', 'clicked_at'];


    public $timestamps = false;

    public function amazonProduct()
    {
        return $this->belongsTo(AmazonProduct::class, 'amazon_product_id');
    }

    public function userSession()
    {
        return $this->belongsTo(UserSession::class, 'tracking_id', 'tracking_id');
    }

    public function pageVisits()
    {
        return $this->hasMany(PageVisit::class, 'tracking_id', 'tracking_id')
            ->where('timestamp', '<=', $this->clicked_at)
            ->orderBy('timestamp');
    }
}