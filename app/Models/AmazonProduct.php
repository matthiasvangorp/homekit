<?php

namespace App\Models;

use App\AffiliateLink;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\Tags\HasTags;
use Spatie\Tags\Tag;

class AmazonProduct extends Model
{
    use HasTags;


    /**
     * Class AmazonProduct
     * @package App
     * @property int $id
     * @property string $asin
     * @property string $title
     * @property string $url
     * @property string $description
     * @property string $image
     * @property float $score
     * @property float $price
     * @property int $brand_id
     * @property int $category_id
     * @property string $locale
     * @property string $country
     * @property bool $publish
     * @property string $created_at
     * @property string $updated_at
     * @property-read Brand $brand
     * @property-read Category $category
     */

    protected $fillable = ['asin', 'image', 'price', 'description', 'url', 'locale', 'country', 'title', 'brand_id', 'category_id', 'publish'];


    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }


    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    /**
     * @return false|string
     */
    public function getChartData() {
        $this->id;

        // Query all prices for the product
        $prices = AmazonProductPrice::where('amazon_product_id', $this->id)
            ->orderBy('created_at')
            ->pluck('price', 'created_at');

        // Format the dates and prices for use in the chart
        $chartData = [];
        foreach ($prices as $date => $price) {
            $carbonDate = Carbon::parse($date);
            $chartData[] = ['date' => $carbonDate->format('Y-m-d'), 'price' => $price];
        }


        return json_encode($chartData);
    }


    public function getMaxPriceOverLastMonth(){
        $startDate = Carbon::now()->subMonth();
        $endDate = Carbon::now();
        $maxPrice = AmazonProductPrice::where('amazon_product_id', $this->id)
            ->whereBetween('created_at', [$startDate, $endDate])
            ->max('price');
        return $maxPrice;
    }

    public function affiliateLink()
    {
        return $this->hasMany(AffiliateLink::class);
    }

    public function productClicks()
    {
        return $this->hasMany(ProductClick::class, 'amazon_product_id');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable')->withType('amazon-products');
    }

    public function getDebugTagsAttribute()
    {
        return $this->tags->pluck('name')->toArray();
    }
}
