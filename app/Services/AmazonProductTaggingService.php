<?php

namespace App\Services;

use App\Models\AmazonProduct;
use Spatie\Tags\Tag;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Collection;

class AmazonProductTaggingService
{
    private $translations = [
        'lightstrip' => [
            'en' => 'lightstrip',
            'fr' => 'bande lumineuse',
            'de' => 'Lichtleiste',
            'es' => 'tira de luz',
            'it' => 'striscia luminosa',
            'nl' => 'lichtstrip',
        ],
        // Add more translations as needed
    ];

    public function syncTagsAcrossStores(AmazonProduct $amazonProduct, array $tagNames)
    {
        // Get all products with the same ASIN
        $relatedProducts = AmazonProduct::where('asin', $amazonProduct->asin)->get();

        foreach ($relatedProducts as $product) {
            $localizedTags = $this->getLocalizedTags($tagNames, $product->locale);
            $product->syncTagsWithType($localizedTags, 'amazon-products');
        }
    }

    private function getLocalizedTags(array $tagNames, string $locale)
    {
        return collect($tagNames)->map(function ($tagName) use ($locale) {
            $tag = $this->findOrCreateTagWithTranslations($tagName);
            return $tag->getTranslation('name', $locale);
        })->toArray();
    }

    private function findOrCreateTagWithTranslations($tagName)
    {
        $tag = Tag::findOrCreate($tagName, 'amazon-products');

        foreach ($this->translations as $baseWord => $translations) {
            if (strcasecmp($tagName, $baseWord) === 0 || in_array(strtolower($tagName), array_map('strtolower', $translations))) {
                foreach ($translations as $locale => $translation) {
                    $tag->setTranslation('name', $locale, $translation);
                }
                $tag->save();
                break;
            }
        }

        return $tag;
    }

    public function getTranslatedTags(AmazonProduct $product): Collection
    {
        $locale = App::getLocale();
        return $product->tags->map(function ($tag) use ($locale) {
            return $tag->getTranslation('name', $locale);
        });
    }
}