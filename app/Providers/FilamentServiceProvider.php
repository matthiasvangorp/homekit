<?php

namespace App\Providers;

use App\Filament\Resources\ProductClickResource;
use Filament\Facades\Filament;
use Filament\Support\Assets\Asset;
use Filament\Support\Facades\FilamentAsset;
use Filament\Support\Assets\Js;
use Illuminate\Support\ServiceProvider;

class FilamentServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        FilamentAsset::register([
            Js::make('chart-js', 'https://cdn.jsdelivr.net/npm/chart.js'),
        ]);
    }


}