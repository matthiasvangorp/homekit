<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use GeoIp2\Database\Reader;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cookie;

class TrackUserActivity
{
    private $botPatterns = [
        'SemrushBot',
        'bot',
        'spider',
        'crawler',
        'Googlebot',
        'Bingbot',
        'Slurp',
        'DuckDuckBot',
        'Baiduspider',
        'YandexBot',
        'Sogou',
        'Exabot',
        'facebookexternalhit',
        'ia_archiver',
        'AhrefsBot',
        'MJ12bot',
        'PetalBot',
        'BLEXBot',
        'Applebot',
        'Twitterbot',
        'rogerbot',
        'Pingdom',
        'Uptimebot',
        'Headless',
        'HeadlessChrome',
        'PhantomJS',
        'Lighthouse',
        'GTmetrix',
    ];

    public function handle($request, Closure $next)
    {
        //Log::info('TrackUserActivity middleware started', ['url' => $request->fullUrl()]);

        if ($this->isBot($request->userAgent())) {
            Log::info('Bot '.$request->userAgent().' detected, skipping tracking');
            return $next($request);
        }

        $trackingId = $request->cookie('tracking_id');
        //Log::info('Initial tracking ID from cookie', ['tracking_id' => $trackingId]);

        if (!$trackingId || !Str::isUuid($trackingId)) {
            $trackingId = (string) Str::uuid();
            //Log::info('New tracking ID generated', ['tracking_id' => $trackingId]);
        }

        if ($request->hasSession() && $request->session()->isStarted()) {
            $session = $request->session();
            //Log::info('Session status', ['has_session' => true, 'session_started' => true]);

            $this->ensureUserSession($request, $trackingId);

            if (!$session->has('tracking_id')) {
                $session->put('tracking_id', $trackingId);
                //Log::info('Tracking ID set in session', ['tracking_id' => $trackingId]);
            } else {
                //Log::info('Existing tracking ID in session', ['tracking_id' => $session->get('tracking_id')]);
            }

            $this->logPageVisit($request, $trackingId);
        } else {
            //Log::info('Session status', ['has_session' => $request->hasSession(), 'session_started' => $request->session()->isStarted()]);
        }

        $response = $next($request);

        // Set an unencrypted cookie
        $response->withCookie(cookie()->forever('tracking_id', $trackingId, null, null, false, false));
        //Log::info('Unencrypted cookie set in response', ['tracking_id' => $trackingId]);

        //Log::info('TrackUserActivity middleware completed');

        return $response;
    }

    private function ensureUserSession($request, $trackingId)
    {
        $existingSession = DB::table('user_sessions')->where('tracking_id', $trackingId)->first();

        if (!$existingSession) {
            $agent = new Agent();
            $geoip = new Reader(storage_path('app/GeoLite2-City.mmdb'));

            try {
                $record = $geoip->city($request->ip());
                $country = $record->country->name;
                $city = $record->city->name;
            } catch (\Exception $e) {
                $country = null;
                $city = null;
            }

            $userAgent = $request->userAgent() ?? 'Unknown';
            $referer = $request->header('referer') ?? '';
            $browserLocale = $request->getPreferredLanguage() ?? 'Unknown';
            $city = $city ?? 'Unknown';
            $deviceType = $agent->deviceType() ?? 'Unknown';

            // Skip inserting if the device type is 'robot'
            if (strtolower($deviceType) === 'robot') {
                Log::info('Robot device type detected, skipping user session insertion', ['user_agent' => $userAgent]);
                return;
            }

            DB::table('user_sessions')->insert([
                'tracking_id' => $trackingId,
                'entry_time' => now(),
                'entry_page' => $request->path(),
                'referrer' => Str::limit($referer, 255, ''),
                'ip_address' => $request->ip(),
                'user_agent' => Str::limit($userAgent, 255, ''),
                'country' => $country,
                'city' => $city,
                'browser_locale' => $browserLocale,
                'browser' => $agent->browser() ?? 'Unknown',
                'device_type' => $deviceType,
                'os' => $agent->platform() ?? 'Unknown',
            ]);
        }
    }

    private function logPageVisit($request, $trackingId)
    {
        $url = $request->fullUrl();

        if (Str::contains($url, ['admin', 'amazon_products'])) {
            Log::info('Skipping page visit logging for excluded URL', ['url' => $url]);
            return;
        }

        DB::table('page_visits')->insert([
            'tracking_id' => $trackingId,
            'url' => $url,
            'timestamp' => now(),
        ]);

        Log::info('Page visit logged', ['url' => $url]);
    }

    private function isBot($userAgent)
    {
        $userAgent = strtolower($userAgent);
        foreach ($this->botPatterns as $pattern) {
            if (strpos($userAgent, strtolower($pattern)) !== false) {
                return true;
            }
        }
        return false;
    }
}