<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Torann\GeoIP\Facades\GeoIP;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Config;

class Localization
{
    public function __construct(
      Application $app,
      Redirector $redirector,
      Request $request
    ) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $test = $request->cookie('country');
        if ($request->hasCookie('country')) {
            return $next($request);
        } else {
            $ip = $request->ip();
            $location = GeoIP::getLocation($ip);
            $country = $location->getAttribute('iso_code');
            Log::info("ip : $ip");
            Log::info("country : $country");

            return $next($request)->withCookie(cookie()->forever('country', $country));
        }
    }
}
