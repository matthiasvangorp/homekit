<?php

namespace App\Http\Controllers;

use App\Models\AmazonProduct;
use App\Models\Brand;
use App\Models\Category;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class SitemapXmlController extends Controller
{
    public function index() {

        $locales = LaravelLocalization::getSupportedLocales();


        $categories = Category::all();
        $brands = Brand::all();

        $products = AmazonProduct::all();


        return response()->view('sitemaps.index', [
            'products' => $products,
            'categories' => $categories,
            'locales' => $locales,
            'brands' => $brands
        ])->header('Content-Type', 'text/xml');
    }
}
