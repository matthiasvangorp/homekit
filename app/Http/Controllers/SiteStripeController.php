<?php

namespace App\Http\Controllers;

use App\Models\AmazonProduct;
use App\Models\Brand;
use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class SiteStripeController extends Controller
{

    /**
     * @var \App\Repositories\Product\CategoryRepository
     */
    protected $categories;

    /**
     * @param  CategoryRepository  $categories
     */
    public function __construct(CategoryRepository $categories)
    {
        $this->middleware('auth')->except(['show']);
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        $products   = AmazonProduct::where('country', 'UK')
            ->orderBy('publish', 'desc')
            ->orderBy('category_id', 'asc')
            ->get();
        $categories = $this->categories->renderWithProducts();

        return View::make('sitestripe.index')->with(['products' => $products, 'categories' => $categories, 'numberOfProducts' => $products->count()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->middleware('auth');
        $brands         = Brand::orderBy('name')->pluck('name', 'id');
        $categoriesForSelect = Category::orderBy('name')->pluck('name', 'id');
        $categories     = $this->categories->renderWithProducts();

        return View::make('sitestripe.create')->with([
            'brands' => $brands, 'categories' => $categories, 'categoriesForSelect' => $categoriesForSelect
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->middleware('auth');
        $rules     = [
            'asin'    => 'required',
            'brand'    => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('sitestripe/create')
                ->withErrors($validator);
        } else {
            $product        = AmazonProduct::firstOrCreate(['asin' => $request->get('asin'), 'country' => 'IT']);
            Log::info($product->title);
            Log::info($product->locale);
            Log::info($product->id);
            if (!is_null($product->brand_id)){
                Log::error('It looks like this product already exists');
            } else {
                Log::info('looking for brand : '. $request->get('brand'));
                $brand = Brand::where('id', $request->get('brand'))->first();
                if (is_null($brand)){
                    Log::error('Brand not found');
                }
                else {
                    $product->description = $request->get('description');
                    $product->title = $request->get('title');
                    $product->image = $request->get('image');
                    $product->url = $request->get('url');
                    $product->price = $request->get('price');
                    $product->brand_id = $request->get('brand');
                    $product->category_id = $request->get('category');
                    $product->locale = 'IT';
                    $product->country = 'IT';
                    $product->save();
                }
            }

            //redirect
            Session::flash('message', 'Succesfully created product');

            return Redirect::to('sitestripe');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
