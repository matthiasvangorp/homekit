<?php

namespace App\Http\Controllers;

use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use App\Traits\ProductSortingTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{

    use ProductSortingTrait;
    /**
     * Create a new controller instance.
     * @param \App\Repositories\Product\ProductRepository $products
     * @param \App\Repositories\Category\CategoryRepository $categories
     */
    public function __construct(ProductRepository $products, CategoryRepository $categories)
    {
        $this->products = $products;
        $this->categories = $categories;
    }

    /**
     * @var \App\Repositories\Product\ProductRepository
     */
    protected $products;

    /**
     * @var \App\Repositories\Category\CategoryRepository
     */
    protected $categories;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->products->render();
        $categories = $this->categories->renderWithProducts();
        $route = route('home');

        return view::make('master')->with(['products' => $products, 'categories' => $categories, 'route' => $route]);
    }

    /**
     * Show the homepage.
     * @param  Request  $request
     */
    public function show(Request $request)
    {
        $locale = strtoupper(App::getLocale());
        $country            = Cookie::get('country');
        Log::info('Locale : ' . $locale);
        Log::info('Country : ' . $country);
        if ($locale !== "ES" && $locale !== "FR" && $locale !== "DE" && $locale !== "EN" && $locale !== "NL" && $locale !== "IT") {
            $locale = 'EN';
        }

        if ($country !== "ES" && $country !== "FR" && $country !== "DE" && $country !== "UK" && $country !== "IT" && $country !== "US") {
            $country = 'DE';
            $locale = 'EN';
        }

        if ($country === "ES"){
            $locale =  "ES";
        }

        if ($country === "FR"){
            $locale =  "FR";
        }

        $sortBy = $request->input('sort') ?? 'updated_desc';
        $products = $this->getSortedProducts($sortBy, $locale, $country);

        $categories = $this->categories->render();

        $title = trans('products.homekit_product_guide');
        $route = route('home');
        if ($country === 'UK') {
            $currency = render_currency('GPB');
        } elseif ($country === 'US'){
            $currency = render_currency('USD');
        }
        else {
            $currency = render_currency('EUR');
        }

        return view::make('master')->with(['products' => $products, 'categories' => $categories, 'title' => $title, 'route' => $route, 'currency' => $currency]);
    }
}
