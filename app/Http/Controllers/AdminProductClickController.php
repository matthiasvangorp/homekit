<?php

namespace App\Http\Controllers;

use App\Models\ProductClick;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminProductClickController extends Controller
{
    public function index(Request $request): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $startDate = $request->input('start_date', Carbon::now()->subDays(30)->startOfDay()->toDateTimeString());
        $endDate = $request->input('end_date', Carbon::now()->endOfDay()->toDateTimeString());

        $productClicks = ProductClick::select(
            'product_clicks.amazon_product_id',
            'product_clicks.country',
            'product_clicks.clicked_at',
            'product_clicks.tracking_id',
            DB::raw('COUNT(*) as click_count')
        )
            ->whereBetween('clicked_at', [$startDate, $endDate])
            ->groupBy('amazon_product_id', 'country', 'clicked_at', 'tracking_id')
            ->with('amazonProduct')
            ->orderBy('clicked_at', 'desc')
            ->get();

        // Prepare data for the chart
        $clicksByDayAndCountry = $productClicks->groupBy(function ($click) {
            return Carbon::parse($click->clicked_at)->format('Y-m-d');
        })->map(function ($dayGroup) {
            return $dayGroup->groupBy('country')->map(function ($countryGroup) {
                return $countryGroup->sum('click_count');
            });
        });

        // Sort the collection by keys (dates) in ascending order
        $clicksByDayAndCountry = $clicksByDayAndCountry->sortKeys();

        $allCountries = $clicksByDayAndCountry->flatMap(function ($day) {
            return $day->keys();
        })->unique()->values();

        $chartData = [
            'labels' => $clicksByDayAndCountry->keys()->toArray(),
            'datasets' => $allCountries->map(function ($country) use ($clicksByDayAndCountry) {
                $data = $clicksByDayAndCountry->map(function ($day) use ($country) {
                    return $day[$country] ?? 0;
                });

                return [
                    'label' => $country,
                    'data' => $data->values()->toArray(),
                    'backgroundColor' => $this->getRandomColor(),
                    'borderColor' => $this->getRandomColor(),
                    'borderWidth' => 1
                ];
            })->values()->toArray()
        ];


        foreach ($productClicks as $click) {
            $userSession = DB::table('user_sessions')
                ->where('tracking_id', $click->tracking_id)
                ->first();

            $pageVisits = DB::table('page_visits')
                ->where('tracking_id', $click->tracking_id)
                ->where('timestamp', '<', $click->clicked_at)
                ->orderBy('timestamp', 'asc')
                ->get();

            $click->entry_page = $userSession->entry_page ?? null;
            $click->entry_referrer = $userSession->referrer ?? null;
            $click->page_visits = $pageVisits;
            $click->user_session_id = $userSession->id ?? null;
        }

        return view('admin.product-clicks', compact('productClicks', 'startDate', 'endDate', 'chartData'));
    }

    private function getRandomColor()
    {
        return 'rgba(' . rand(0,255) . ',' . rand(0,255) . ',' . rand(0,255) . ',0.6)';
    }


    /**
     * @param $id
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function showUserSession($id): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $userSession = DB::table('user_sessions')->find($id);

        if (!$userSession) {
            abort(404);
        }

        $pageVisits = DB::table('page_visits')
            ->where('tracking_id', $userSession->tracking_id)
            ->orderBy('timestamp', 'asc')
            ->get();

        $productClicks = ProductClick::where('tracking_id', $userSession->tracking_id)
            ->with('amazonProduct')
            ->orderBy('clicked_at', 'asc')
            ->get();

        return view('admin.user-session-details', compact('userSession', 'pageVisits', 'productClicks'));
    }

    public function popularProducts(Request $request): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $startDate = $request->input('start_date', Carbon::now()->subDays(30)->startOfDay()->toDateTimeString());
        $endDate = $request->input('end_date', Carbon::now()->endOfDay()->toDateTimeString());

        $popularProducts = ProductClick::select(
            'product_clicks.amazon_product_id',
            'product_clicks.country',
            DB::raw('COUNT(*) as total_clicks')
        )
            ->whereBetween('clicked_at', [$startDate, $endDate])
            ->groupBy('amazon_product_id', 'country')
            ->with('amazonProduct')
            ->orderBy('total_clicks', 'desc')
            ->get()
            ->groupBy('country');

        return view('admin.popular-products', compact('popularProducts', 'startDate', 'endDate'));
    }


    /**
     * @param  Request  $request
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function searchActivity(Request $request): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $startDate = $request->input('start_date', Carbon::now()->subDays(30)->startOfDay()->toDateTimeString());
        $endDate = $request->input('end_date', Carbon::now()->endOfDay()->toDateTimeString());


        $searchQueries = DB::table('page_visits')
            ->select('page_visits.*', 'user_sessions.country', 'user_sessions.city')
            ->join('user_sessions', 'page_visits.tracking_id', '=', 'user_sessions.tracking_id')
            ->where('page_visits.url', 'like', '%search%')
            ->whereBetween('page_visits.timestamp', [$startDate, $endDate])
            ->orderBy('page_visits.timestamp', 'desc')
            ->get();


        $searchQueries = $searchQueries->map(function ($query) {
            $url = parse_url($query->url);
            parse_str($url['query'] ?? '', $params);
            $query->search_term = $params['query'] ?? '';
            return $query;
        });

        return view('admin.search-activity', compact('searchQueries', 'startDate', 'endDate'));
    }
}