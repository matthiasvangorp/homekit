<?php

namespace App\Http\Controllers;

use App\Bol;
use App\Jobs\BolPriceUpdateJob;
use App\Repositories\Link\AffiliateLinkRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BolController extends Controller
{

    protected $affiliateLinks;

    public function __construct(AffiliateLinkRepository $affiliateLinkRepository)
    {
        $this->affiliateLinks = $affiliateLinkRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //get all affiliate_links where store is bol and external_id is not null
        $links = $this->affiliateLinks->getBolAffiliateLinks();
        $counter = 0;
        foreach ($links as $link){
            BolPriceUpdateJob::dispatch($link, $this->affiliateLinks)->delay(now()->addSeconds($counter));
            $counter ++;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bol  $bol
     * @return Response
     */
    public function show(Bol $bol)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bol  $bol
     * @return Response
     */
    public function edit(Bol $bol)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bol  $bol
     * @return Response
     */
    public function update(Request $request, Bol $bol)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bol  $bol
     * @return Response
     */
    public function destroy(Bol $bol)
    {
        //
    }
}
