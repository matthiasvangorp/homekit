<?php

namespace App\Http\Controllers;



use Amazon\ProductAdvertisingAPI\v1\ApiException;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\api\DefaultApi;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\PartnerType;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\ProductAdvertisingAPIClientException;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\SearchItemsRequest;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\SearchItemsResource;
use Amazon\ProductAdvertisingAPI\v1\Configuration;
use App\Jobs\FrenchProducts;
use App\Jobs\SpanishProducts;
use App\Models\AmazonProduct;
use App\Models\Brand;
use GuzzleHttp\Client;


class AmazonLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        {
            $homekit_category = 'HomeKit-Compatible Products'; // The category name for HomeKit-compatible products on Amazon
            $keywords = 'lamp';

            $results = AmazonProduct::search($homekit_category, $keywords, 10)
                ->responseGroup('Images,ItemAttributes')
                ->paginate(10);

            $products = [];

            foreach ($results as $result) {
                $title = $result->getTitle();
                $image_url = $result->getImageUrl();
                $product_url = $result->getDetailPageUrl();

                $item_attributes = $result->get('ItemAttributes');
                $is_homekit_compatible = stristr($item_attributes->get('Feature'), 'HomeKit') !== false;

                $products[] = [
                    'title' => $title,
                    'image_url' => $image_url,
                    'product_url' => $product_url,
                    'is_homekit_compatible' => $is_homekit_compatible
                ];
            }

            return view('homekit-products', ['products' => $products]);
        }
    }


    public function showBrowseNodes (){
        //$brands = Brand::all();
        //foreach ($brands as $brand) {
            //$this->getSearchResultsByBrandAndQuery($brands[1], "homekit");
        //}


        //FrenchProducts::dispatch();
        //SpanishProducts::dispatch();
        $productsWithoutCategory = AmazonProduct::whereNull('category_id')->get();
        foreach ($productsWithoutCategory as $productWithoutCategory) {
            $goodProduct = AmazonProduct::where('locale', 'DE')->where('asin', $productWithoutCategory->asin)->first();
            $productWithoutCategory->category_id = $goodProduct->category_id;
            $productWithoutCategory->save();
        }

        //foreach ($amazonProducts as $amazonProduct){
            //$this->getFrenchProductInfo($amazonProducts[0]);
        //}

        die();

    }


//    private function getFrenchProductInfo(AmazonProduct $germanAmazonProduct){
//        $config = new Configuration();
//
//        /*
//         * Add your credentials
//         */
//        # Please add your access key here
//        $config->setAccessKey(env('AWS_ACCESS_KEY_ID'));
//        # Please add your secret key here
//        $config->setSecretKey(env('AWS_SECRET_ACCESS_KEY'));
//
//        # Please add your partner tag (store/tracking id) here
//        $partnerTag = env('AWS_ASSOCIATE_TAG_FR');
//
//        /*
//         * PAAPI host and region to which you want to send request
//         * For more details refer:
//         * https://webservices.amazon.com/paapi5/documentation/common-request-parameters.html#host-and-region
//         */
//        $config->setHost(env('AMAZON_HOST_FR'));
//        $config->setRegion(env('AMAZON_REGION'));
//
//        $apiInstance = new DefaultApi(
//        /*
//         * If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
//         * This is optional, `GuzzleHttp\Client` will be used as default.
//         */
//            new Client(),
//            $config
//        );
//
//        # Request initialization
//
//
//        /*
//         * Specify the category in which search request is to be made
//         * For more details, refer:
//         * https://webservices.amazon.com/paapi5/documentation/use-cases/organization-of-items-on-amazon/search-index.html
//         */
//        $searchIndex = "All";
//
//        # Specify item count to be returned in search result
//        $itemCount = 10;
//
//        /*
//         * Choose resources you want from SearchItemsResource enum
//         * For more details,
//         * refer: https://webservices.amazon.com/paapi5/documentation/search-items.html#resources-parameter
//         */
//        $resources = [
//            GetItemsResource::ITEM_INFOTITLE,
//            GetItemsResource::OFFERSLISTINGSPRICE,
//            GetItemsResource::ITEM_INFOFEATURES,
//            GetItemsResource::ITEM_INFOCONTENT_INFO,
//            GetItemsResource::IMAGESPRIMARYLARGE,
//            GetItemsResource::ITEM_INFOPRODUCT_INFO,
//            GetItemsResource::ITEM_INFOCONTENT_RATING,];
//
//        # Forming the request
//        $getItemsRequest = new GetItemsRequest();
//
//
//
//        $getItemsRequest->setPartnerTag($partnerTag);
//        $getItemsRequest->setPartnerType(PartnerType::ASSOCIATES);
//        $getItemsRequest->setItemIds([$germanAmazonProduct->asin]);
//        $getItemsRequest->setResources($resources);
//
//        # Validating request
//        $invalidPropertyList = $getItemsRequest->listInvalidProperties();
//        $length = count($invalidPropertyList);
//        if ($length > 0) {
//            echo "Error forming the request", PHP_EOL;
//            foreach ($invalidPropertyList as $invalidProperty) {
//                echo $invalidProperty, PHP_EOL;
//            }
//            return;
//        }
//
//        # Sending the request
//        try {
//            $getItemsResponse = $apiInstance->getItems($getItemsRequest);
//
//            //echo 'API called successfully', PHP_EOL;
//            //echo 'Complete Response: ', $searchItemsResponse, PHP_EOL;
//
//            # Parsing the response
//            if ($getItemsResponse->getItemsResult() !== null) {
//                //echo 'Printing first item information in SearchResult:', PHP_EOL;
//                $items = $getItemsResponse->getItemsResult()->getItems();
//
//
//
//                echo '<table>';
//                echo '<thead><tr><th>ASIN</th><th>DetailPageURL</th><th>Title</th><th>Byline</th><th>Image</th><th>Buying Price</th><th>Rating</th></tr></thead>';
//                echo '<tbody>';
//
//
//                foreach ($items as $item) {
//                    $homekitFound = false;
//                    if ($item !== null) {
//                        echo '<tr>';
//                        if ($item->getASIN() !== null) {
//                            echo '<td>' . $item->getASIN() . '</td>';
//                            $amazonProduct = AmazonProduct::firstOrCreate(['asin' => $item->getASIN(), 'locale' => 'FR']);
//                        } else {
//                            echo '<td></td>';
//                        }
//                        if ($item->getDetailPageURL() !== null) {
//                            echo '<td>' . $item->getDetailPageURL() . '</td>';
//                            $amazonProduct->url = $item->getDetailPageURL();
//                        } else {
//                            echo '<td></td>';
//                        }
//                        if ($item->getItemInfo() !== null
//                            and $item->getItemInfo()->getTitle() !== null
//                            and $item->getItemInfo()->getTitle()->getDisplayValue() !== null) {
//                            echo '<td>' . $item->getItemInfo()->getTitle()->getDisplayValue() . '</td>';
//                            $amazonProduct->title = $item->getItemInfo()->getTitle()->getDisplayValue();
//
//                            if (strpos(strtolower($item->getItemInfo()->getTitle()->getDisplayValue()), "homekit") !== false) {
//                                $homekitFound = true;
//                            }
//                        } else {
//                            echo '<td></td>';
//                        }
//
//                        if ($item->getItemInfo() !== null
//                            and $item->getItemInfo()->getFeatures() !== null
//                        ) {
//
//                            $displayValues = $item->getItemInfo()->getFeatures()->getDisplayValues();
//                            $displayValuesString = "<ul>";
//                            foreach ($displayValues as $displayValue){
//                                $displayValuesString .= "<li>".$displayValue."</li>";
//                            }
//                            $displayValuesString .= "</ul>";
//                            $amazonProduct->description = $displayValuesString;
//                            if (strpos(strtolower($displayValuesString), "homekit") !== false) {
//                                $homekitFound = true;
//                            }
//                            echo '<td>' . $displayValuesString . '</td>';
//                        } else {
//                            echo '<td></td>';
//                        }
//                        if ($item->getImages() !== null && $item->getImages()->getPrimary()->getLarge()->getURL() !== null) {
//                            echo '<td><img src="'.$item->getImages()->getPrimary()->getLarge()->getURL().'"></td>';
//                            $amazonProduct->image = $item->getImages()->getPrimary()->getLarge()->getURL();
//
//                        }
//                        if ($item->getOffers() !== null
//                            and $item->getOffers() !== null
//                            and $item->getOffers()->getListings() !== null
//                            and $item->getOffers()->getListings()[0]->getPrice() !== null
//                            and $item->getOffers()->getListings()[0]->getPrice()->getDisplayAmount() !== null) {
//                            echo '<td>' . $item->getOffers()->getListings()[0]->getPrice()->getDisplayAmount() . '</td>';
//                            $amazonProduct->price = $item->getOffers()->getListings()[0]->getPrice()->getAmount();
//                        } else {
//                            echo '<td></td>';
//                        }
//                        if ($item->getItemInfo() !== null
//                            and $item->getItemInfo()->getContentRating() !== null
//                            and $item->getItemInfo()->getContentRating()->getModelName() !== null) {
//                            echo '<td>' . $item->getItemInfo()->getContentRating()->getModelName() . '</td>';
//                        } else {
//                            echo '<td>test</td>';
//                        }
//                        echo '</tr>';
//                        $amazonProduct->brand_id = $germanAmazonProduct->brand_id;
//                        if ($homekitFound) {
//                            $amazonProduct->locale = 'FR';
//                            $amazonProduct->save();
//                        }
//                        else {
//                            $amazonProduct->delete();
//                        }
//                    }
//                }
//
//                echo '</tbody>';
//                echo '</table>';
//            }
//            if ($getItemsResponse->getErrors() !== null) {
//                echo PHP_EOL, 'Printing Errors:', PHP_EOL, 'Printing first error object from list of errors', PHP_EOL;
//                echo 'Error code: ', $getItemsResponse->getErrors()[0]->getCode(), PHP_EOL;
//                echo 'Error message: ', $getItemsResponse->getErrors()[0]->getMessage(), PHP_EOL;
//            }
//        } catch (ApiException $exception) {
//            echo "Error calling PA-API 5.0!", PHP_EOL;
//            echo "HTTP Status Code: ", $exception->getCode(), PHP_EOL;
//            echo "Error Message: ", $exception->getMessage(), PHP_EOL;
//            if ($exception->getResponseObject() instanceof ProductAdvertisingAPIClientException) {
//                $errors = $exception->getResponseObject()->getErrors();
//                foreach ($errors as $error) {
//                    echo "Error Type: ", $error->getCode(), PHP_EOL;
//                    echo "Error Message: ", $error->getMessage(), PHP_EOL;
//                }
//            } else {
//                echo "Error response body: ", $exception->getResponseBody(), PHP_EOL;
//            }
//        } catch (Exception $exception) {
//            echo "Error Message: ", $exception->getMessage(), PHP_EOL;
//        }
//    }




    private function getSearchResultsByBrandAndQuery(Brand $brand, string $query){
        $config = new Configuration();

        /*
         * Add your credentials
         */
        # Please add your access key here
        $config->setAccessKey(env('AWS_ACCESS_KEY_ID'));
        # Please add your secret key here
        $config->setSecretKey(env('AWS_SECRET_ACCESS_KEY'));

        # Please add your partner tag (store/tracking id) here
        $partnerTag = env('AWS_ASSOCIATE_TAG');

        /*
         * PAAPI host and region to which you want to send request
         * For more details refer:
         * https://webservices.amazon.com/paapi5/documentation/common-request-parameters.html#host-and-region
         */
        $config->setHost(env('AMAZON_HOST_DE'));
        $config->setRegion(env('AMAZON_REGION'));

        $apiInstance = new DefaultApi(
        /*
         * If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
         * This is optional, `GuzzleHttp\Client` will be used as default.
         */
            new Client(),
            $config
        );

        # Request initialization

        # Specify keywords
        $keyword = $query;

        /*
         * Specify the category in which search request is to be made
         * For more details, refer:
         * https://webservices.amazon.com/paapi5/documentation/use-cases/organization-of-items-on-amazon/search-index.html
         */
        $searchIndex = "All";

        # Specify item count to be returned in search result
        $itemCount = 10;

        /*
         * Choose resources you want from SearchItemsResource enum
         * For more details,
         * refer: https://webservices.amazon.com/paapi5/documentation/search-items.html#resources-parameter
         */
        $resources = [
            SearchItemsResource::ITEM_INFOTITLE,
            SearchItemsResource::OFFERSLISTINGSPRICE,
            SearchItemsResource::ITEM_INFOFEATURES,
            SearchItemsResource::ITEM_INFOCONTENT_INFO,
            SearchItemsResource::IMAGESPRIMARYLARGE,
            SearchItemsResource::ITEM_INFOPRODUCT_INFO,
            SearchItemsResource::ITEM_INFOCONTENT_RATING,];

        # Forming the request
        $searchItemsRequest = new SearchItemsRequest();
        $searchItemsRequest->setSearchIndex($searchIndex);
        $searchItemsRequest->setKeywords($keyword);
        $searchItemsRequest->setItemCount($itemCount);
        $searchItemsRequest->setPartnerTag($partnerTag);
        $searchItemsRequest->setPartnerType(PartnerType::ASSOCIATES);
        $searchItemsRequest->setBrand($brand->name);
        $searchItemsRequest->setResources($resources);

        # Validating request
        $invalidPropertyList = $searchItemsRequest->listInvalidProperties();
        $length = count($invalidPropertyList);
        if ($length > 0) {
            echo "Error forming the request", PHP_EOL;
            foreach ($invalidPropertyList as $invalidProperty) {
                echo $invalidProperty, PHP_EOL;
            }
            return;
        }

        # Sending the request
        try {
            $searchItemsResponse = $apiInstance->searchItems($searchItemsRequest);

            //echo 'API called successfully', PHP_EOL;
            //echo 'Complete Response: ', $searchItemsResponse, PHP_EOL;

            # Parsing the response
            if ($searchItemsResponse->getSearchResult() !== null) {
                //echo 'Printing first item information in SearchResult:', PHP_EOL;
                $items = $searchItemsResponse->getSearchResult()->getItems();



                echo '<table>';
                echo '<thead><tr><th>ASIN</th><th>DetailPageURL</th><th>Title</th><th>Byline</th><th>Image</th><th>Buying Price</th><th>Rating</th></tr></thead>';
                echo '<tbody>';


                foreach ($items as $item) {
                    $homekitFound = false;
                    if ($item !== null) {
                        echo '<tr>';
                        if ($item->getASIN() !== null) {
                            echo '<td>' . $item->getASIN() . '</td>';
                            $amazonProduct = AmazonProduct::firstOrCreate(['asin' => $item->getASIN()]);
                        } else {
                            echo '<td></td>';
                        }
                        if ($item->getDetailPageURL() !== null) {
                            echo '<td>' . $item->getDetailPageURL() . '</td>';
                            $amazonProduct->url = $item->getDetailPageURL();
                        } else {
                            echo '<td></td>';
                        }
                        if ($item->getItemInfo() !== null
                            and $item->getItemInfo()->getTitle() !== null
                            and $item->getItemInfo()->getTitle()->getDisplayValue() !== null) {
                            echo '<td>' . $item->getItemInfo()->getTitle()->getDisplayValue() . '</td>';
                            $amazonProduct->title = $item->getItemInfo()->getTitle()->getDisplayValue();

                            if (strpos(strtolower($item->getItemInfo()->getTitle()->getDisplayValue()), "homekit") !== false) {
                                $homekitFound = true;
                            }
                        } else {
                            echo '<td></td>';
                        }

                        if ($item->getItemInfo() !== null
                            and $item->getItemInfo()->getFeatures() !== null
                            ) {

                            $displayValues = $item->getItemInfo()->getFeatures()->getDisplayValues();
                            $displayValuesString = "<ul>";
                            foreach ($displayValues as $displayValue){
                                $displayValuesString .= "<li>".$displayValue."</li>";
                            }
                            $displayValuesString .= "</ul>";
                            $amazonProduct->description = $displayValuesString;
                            if (strpos(strtolower($displayValuesString), "homekit") !== false) {
                                $homekitFound = true;
                            }
                            echo '<td>' . $displayValuesString . '</td>';
                        } else {
                            echo '<td></td>';
                        }
                        if ($item->getImages() !== null && $item->getImages()->getPrimary()->getLarge()->getURL() !== null) {
                            echo '<td><img src="'.$item->getImages()->getPrimary()->getLarge()->getURL().'"></td>';
                            $amazonProduct->image = $item->getImages()->getPrimary()->getLarge()->getURL();

                        }
                        if ($item->getOffers() !== null
                            and $item->getOffers() !== null
                            and $item->getOffers()->getListings() !== null
                            and $item->getOffers()->getListings()[0]->getPrice() !== null
                            and $item->getOffers()->getListings()[0]->getPrice()->getDisplayAmount() !== null) {
                            echo '<td>' . $item->getOffers()->getListings()[0]->getPrice()->getDisplayAmount() . '</td>';
                            $amazonProduct->price = $item->getOffers()->getListings()[0]->getPrice()->getAmount();
                        } else {
                            echo '<td></td>';
                        }
                        if ($item->getItemInfo() !== null
                            and $item->getItemInfo()->getContentRating() !== null
                            and $item->getItemInfo()->getContentRating()->getModelName() !== null) {
                            echo '<td>' . $item->getItemInfo()->getContentRating()->getModelName() . '</td>';
                        } else {
                            echo '<td>test</td>';
                        }
                        echo '</tr>';
                        $amazonProduct->brand_id = $brand->id;
                        if ($homekitFound) {
                            $amazonProduct->save();
                        }
                        else {
                            $amazonProduct->delete();
                        }
                    }
                }

                echo '</tbody>';
                echo '</table>';
            }
            if ($searchItemsResponse->getErrors() !== null) {
                echo PHP_EOL, 'Printing Errors:', PHP_EOL, 'Printing first error object from list of errors', PHP_EOL;
                echo 'Error code: ', $searchItemsResponse->getErrors()[0]->getCode(), PHP_EOL;
                echo 'Error message: ', $searchItemsResponse->getErrors()[0]->getMessage(), PHP_EOL;
            }
        } catch (ApiException $exception) {
            echo "Error calling PA-API 5.0!", PHP_EOL;
            echo "HTTP Status Code: ", $exception->getCode(), PHP_EOL;
            echo "Error Message: ", $exception->getMessage(), PHP_EOL;
            if ($exception->getResponseObject() instanceof ProductAdvertisingAPIClientException) {
                $errors = $exception->getResponseObject()->getErrors();
                foreach ($errors as $error) {
                    echo "Error Type: ", $error->getCode(), PHP_EOL;
                    echo "Error Message: ", $error->getMessage(), PHP_EOL;
                }
            } else {
                echo "Error response body: ", $exception->getResponseBody(), PHP_EOL;
            }
        } catch (Exception $exception) {
            echo "Error Message: ", $exception->getMessage(), PHP_EOL;
        }
    }
}
