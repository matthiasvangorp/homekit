<?php

namespace App\Http\Controllers;

use App\Models\AmazonProduct;
use App\Models\Brand;
use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class AmazonProductController extends Controller
{

    /**
     * @var CategoryRepository
     */
    protected $categories;


    public function __construct(CategoryRepository $categories)
    {
        $this->middleware('auth')->except(['show']);
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(): \Illuminate\Contracts\View\View
    {
        $this->middleware('auth');
        $products   = AmazonProduct::where('locale', 'DE')
            ->orderBy('publish', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();
        $dutchProductsCount   = AmazonProduct::where('locale', 'NL')
            ->orderBy('publish', 'desc')
            ->orderBy('category_id', 'asc')
            ->count();
        $frenchProductsCount   = AmazonProduct::where('locale', 'FR')
            ->orderBy('publish', 'desc')
            ->orderBy('category_id', 'asc')
            ->count();
        $spanishProductsCount   = AmazonProduct::where('locale', 'ES')
            ->orderBy('publish', 'desc')
            ->orderBy('category_id', 'asc')
            ->count();
        $italianProductsCount   = AmazonProduct::where('locale', 'IT')
            ->orderBy('publish', 'desc')
            ->orderBy('category_id', 'asc')
            ->count();
        $ukProductsCount   = AmazonProduct::where('locale', 'EN')->where('country', 'UK')
            ->orderBy('publish', 'desc')
            ->orderBy('category_id', 'asc')
            ->count();
        $usProductsCount   = AmazonProduct::where('locale', 'EN')->where('country', 'US')
            ->orderBy('publish', 'desc')
            ->orderBy('category_id', 'asc')
            ->count();
        $categories = $this->categories->renderWithProducts();

        return View::make('amazonproducts.index')->with([
            'products' => $products,
            'categories' => $categories,
            'numberOfProducts' => $products->count(),
            'dutchProductsCount' => $dutchProductsCount,
            'frenchProductsCount' => $frenchProductsCount,
            'spanishProductsCount' => $spanishProductsCount,
            'italianProductsCount' => $italianProductsCount,
            'ukProductsCount' => $ukProductsCount,
            'usProductsCount' => $usProductsCount,
        ]);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        //
        $this->middleware('auth');
        $brands         = Brand::orderBy('name')->pluck('name', 'id');
        $categories     = $this->categories->renderWithProducts();

        return View::make('amazonproducts.create')->with([
             'brands' => $brands, 'categories' => $categories
        ]);
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        //validate
        $this->middleware('auth');
        $rules     = [
            'asin'    => 'required',
            'brand'    => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('amazon_products/create')
                ->withErrors($validator);
        } else {
            $product        = AmazonProduct::firstOrCreate(['asin' => $request->get('asin')]);
            if (!is_null($product->brand_id)){
                Log::error('It looks like this product already exists');
            } else {
                Log::info('looking for brand : '. $request->get('brand'));
                $brand = Brand::where('id', $request->get('brand'))->first();
                if (is_null($brand)){
                    Log::error('Brand not found');
                }
                else {
                    Log::info('Calling products:byasin to create product');
                    Artisan::call('products:byasin', ['brand' => $brand->name, 'asin' => $request->get('asin')]);
                }
            }

            //redirect
            Session::flash('message', 'Succesfully created product');

            return Redirect::to('amazon_products');
        }
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        //
        $this->middleware('auth');
        $product        = AmazonProduct::find($id);
        $categories     = $this->categories->renderWithProducts();
        $categoriesList = Category::pluck('name', 'id');
        $brands         = Brand::pluck('name', 'id');


        return View::make('amazonproducts.edit')->with([
            'product' => $product, 'categories' => $categories, 'categoriesList' => $categoriesList,
            'brands'  => $brands,
            'stores'  => config('stores')
        ]);
    }




    public function update(Request $request, $id)
    {
        $this->middleware('auth');
        //validate
        $rules     = [
            'category' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('amazon_products/edit')
                ->withErrors($validator);
        } else {
            $product        = AmazonProduct::find($id);


            $product->category_id    = $request->get('category');
            $product->brand_id    = $request->get('brand');
            if (!$request->get('publish')) {
                $product->publish = 0;
            } else {
                $product->publish = $request->get('publish');
            }
            $product->save();

            //update the category id for the same asin but different locale

            $otherProducts = AmazonProduct::where('asin', $product->asin)->whereNotIn('locale', [$product->locale])->get();

            foreach ($otherProducts as $otherProduct) {
                $otherProduct->category_id = $product->category_id;
                $otherProduct->save();
            }


            //redirect
            Session::flash('message', 'Succesfully updated product');

            return Redirect::to('amazon_products');
        }
    }
}
