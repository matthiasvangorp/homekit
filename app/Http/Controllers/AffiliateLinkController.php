<?php

namespace App\Http\Controllers;

use App\AffiliateLink;
use App\Models\AmazonProduct;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\Request;


class AffiliateLinkController extends Controller
{

    /**
     * @var \App\Repositories\Product\CategoryRepository
     */
    protected $categories;

    public function __construct(CategoryRepository $categories)
    {
        $this->middleware('auth');
        $this->categories = $categories;
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(int $id)
    {
        $amazonProduct = AmazonProduct::find($id);
        $categories = $this->categories->renderWithProducts();
        return view('affiliate-links.create', compact('amazonProduct',  'categories'));
    }


    public function store(int $id, Request $request)
    {
        // Validate the input data
        $validatedData = $request->validate([
            'url' => 'required|url',
            'locale' => 'required',
            'country' => 'required',
            'currency' => 'required',
            'price' => 'required|numeric|min:0',
        ]);

        // Create the new AffiliateLink
        $affiliateLink = new AffiliateLink($validatedData);
        $affiliateLink->url .= '?idp=16242&banner_id=53565';
        $affiliateLink->locale = 'EN';

        $amazonProduct = AmazonProduct::find($id);
        $affiliateLink->asin = $amazonProduct->asin;

        // Associate the AffiliateLink with the AmazonProduct
        $amazonProduct->affiliateLink()->save($affiliateLink);

        //create another one for Hungary
        $affiliateLinkHU = new AffiliateLink();
        $affiliateLinkHU->amazon_product_id = $affiliateLink->amazon_product_id;
        $affiliateLinkHU->ean = $affiliateLink->ean;
        $affiliateLinkHU->asin = $affiliateLink->asin;
        $affiliateLinkHU->locale = 'HU';
        $affiliateLinkHU->country = $affiliateLink->country;
        $affiliateLinkHU->currency = $affiliateLink->currency;
        $affiliateLinkHU->price = $affiliateLink->price;
        $affiliateLinkHU->url = str_replace('/EN/', '/HU/',$affiliateLink->url);
        $affiliateLinkHU->save();


        // Redirect to the AmazonProduct page
        return redirect()->route('home');
    }
}
