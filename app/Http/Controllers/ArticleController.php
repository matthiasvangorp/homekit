<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Article\ArticleRepository;

class ArticleController extends Controller
{
    /**
     * ArticleController constructor.
     * @param \App\Repositories\Article\ArticleRepository $articles
     */
    public function __construct(ArticleRepository $articles)
    {
        $this->middleware('auth')->except(['index', 'show']);
        $this->articles = $articles;
    }

    /**
     * @var \App\Repositories\Product\ArticleRepository
     */
    protected $articles;

    public function index()
    {
        $articles = $this->articles->render();

        return View::make('articles.index')->with(['articles'=> $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->middleware('auth');

        return View::make('articles.create');
    }

    public function store(Request $request)
    {
        $this->articles->create($request->input());
        $articles = $this->articles->render();

        return View::make('articles.index')->with(['articles'=> $articles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->middleware('auth');
        $article = $this->articles->renderOne($id);

        return View::make('articles.edit')->with(['article' => $article]);
    }

    public function update(Request $request, $id)
    {
        $this->middleware('auth');
        $this->articles->update($request->input(), $id);

        //redirect
        Session::flash('message', 'Succesfully updated product');

        return Redirect::to('articles');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        //
        $article = $this->articles->renderOne($id);

        return View::make('articles.show')
          ->with(['article' => $article, 'title' => $article->title]);
    }
}
