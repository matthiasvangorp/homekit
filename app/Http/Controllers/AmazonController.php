<?php

namespace App\Http\Controllers;

use App\Amazon;
use App\Jobs\AmazonPriceUpdateJob;
use App\Repositories\Link\AffiliateLinkRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AmazonController extends Controller
{
    protected $affiliateLinks;

    public function __construct(AffiliateLinkRepository $affiliateLinkRepository)
    {
        $this->affiliateLinks = $affiliateLinkRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //get all affiliate_links where store is amazon and asin is not null
        $links = $this->affiliateLinks->getAmazonAffiliateLinks();
        $counter = 0;
        foreach ($links as $link){
            AmazonPriceUpdateJob::dispatch($link, $this->affiliateLinks)->delay(now()->addSeconds($counter * 5));
            $counter ++;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Amazon  $amazon
     * @return Response
     */
    public function show(Amazon $amazon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Amazon  $amazon
     * @return Response
     */
    public function edit(Amazon $amazon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Amazon  $amazon
     * @return Response
     */
    public function update(Request $request, Amazon $amazon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Amazon  $amazon
     * @return Response
     */
    public function destroy(Amazon $amazon)
    {
        //
    }
}
