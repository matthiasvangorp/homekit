<?php

namespace App\Http\Controllers;

use App\Models\AmazonProduct;
use App\Models\ProductClick;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductClickController extends Controller
{
    public function trackClick(Request $request)
    {
        Log::info('Track click method called', [
            'request' => $request->all(),
            'headers' => $request->headers->all(),
        ]);

        try {
            $validatedData = $request->validate([
                'product_id' => 'required|integer',
                'country' => 'required|string|size:2',
                'tracking_id' => 'required|string|uuid',
            ]);

            // Check if the Amazon product exists
            $amazonProduct = AmazonProduct::find($validatedData['product_id']);
            if (!$amazonProduct) {
                Log::warning('Attempted to track click for non-existent Amazon product', [
                    'product_id' => $validatedData['product_id'],
                ]);
                return response()->json(['success' => false, 'error' => 'Amazon product not found'], 404);
            }

            $click = ProductClick::create([
                'amazon_product_id' => $validatedData['product_id'],
                'country' => $validatedData['country'],
                'tracking_id' => $validatedData['tracking_id'],
                'clicked_at' => now(),
            ]);

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            Log::error('Error tracking product click', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
            return response()->json(['success' => false, 'error' => 'An error occurred while tracking the click.'], 500);
        }
    }
}