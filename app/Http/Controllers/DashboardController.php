<?php

namespace App\Http\Controllers;

use App\Models\UserSession;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $startDate = $request->input('start_date')
            ? Carbon::parse($request->input('start_date'))->startOfDay()
            : Carbon::now()->subDays(1)->startOfDay();

        $endDate = $request->input('end_date')
            ? Carbon::parse($request->input('end_date'))->endOfDay()
            : Carbon::now()->endOfDay();

        $totalVisitors = UserSession::whereBetween('entry_time', [$startDate, $endDate])->count();

        $pageViews = UserSession::whereBetween('entry_time', [$startDate, $endDate])->count(); // Assuming each session is a page view

        $topPages = UserSession::select('entry_page as url', DB::raw('count(*) as total'))
            ->whereBetween('entry_time', [$startDate, $endDate])
            ->groupBy('entry_page')
            ->orderByDesc('total')
            ->limit(10)
            ->get();

        $host = $request->getHost();
        $wwwHost = 'www.' . $host;
        $nonWwwHost = preg_replace('/^www\./', '', $host);

        $topReferrers = UserSession::select('referrer', \DB::raw('count(*) as total'))
            ->whereNotNull('referrer')
            ->whereBetween('entry_time', [$startDate, $endDate])
            ->where(function ($query) use ($host, $wwwHost, $nonWwwHost) {
                $query->where('referrer', 'not like', '%' . $host . '%')
                    ->where('referrer', 'not like', '%' . $wwwHost . '%')
                    ->where('referrer', 'not like', '%' . $nonWwwHost . '%');
            })
            ->groupBy('referrer')
            ->orderByDesc('total')
            ->limit(10)
            ->get();

        $topCountries = UserSession::select('country', \DB::raw('count(*) as total'))
            ->whereNotNull('country')
            ->whereBetween('entry_time', [$startDate, $endDate])
            ->groupBy('country')
            ->orderByDesc('total')
            ->limit(10)
            ->get();

        $topCities = UserSession::select(\DB::raw("CONCAT(city, ', ', country) as location"), \DB::raw('count(*) as total'))
            ->whereNotNull('city')
            ->whereNotNull('country')
            ->whereBetween('entry_time', [$startDate, $endDate])
            ->groupBy('city', 'country')
            ->orderByDesc('total')
            ->limit(10)
            ->get();

        $topBrowsers = UserSession::select('browser', \DB::raw('count(*) as total'))
            ->whereBetween('entry_time', [$startDate, $endDate])
            ->groupBy('browser')
            ->orderByDesc('total')
            ->limit(5)
            ->get();

        $topLocales = UserSession::select('browser_locale', \DB::raw('count(*) as total'))
            ->whereBetween('entry_time', [$startDate, $endDate])
            ->groupBy('browser_locale')
            ->orderByDesc('total')
            ->limit(5)
            ->get();

        $dailyVisitorData = $this->getDailyVisitorData($startDate, $endDate);


        return view('admin.dashboard', compact(
            'totalVisitors', 'pageViews', 'topPages', 'topReferrers', 'topCountries', 'topCities', 'topBrowsers', 'topLocales', 'startDate', 'endDate', 'dailyVisitorData'
        ));
    }


    private function getDailyVisitorData($startDate, $endDate)
    {
        return UserSession::select(DB::raw('DATE(entry_time) as date'), DB::raw('COUNT(DISTINCT ip_address) as visitors'))
            ->whereBetween('entry_time', [$startDate, $endDate])
            ->groupBy(DB::raw('DATE(entry_time)'))
            ->orderBy('date')
            ->get();
    }
}