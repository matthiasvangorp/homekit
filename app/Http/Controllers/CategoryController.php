<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use App\Traits\ProductSortingTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class CategoryController extends Controller
{

    use ProductSortingTrait;
    /**
     * CategoryController constructor.
     * @param  \App\Repositories\Product\ProductRepository    $products
     * @param  \App\Repositories\Category\CategoryRepository  $categories
     */
    public function __construct(ProductRepository $products, CategoryRepository $categories)
    {
        $this->products   = $products;
        $this->categories = $categories;
        $this->middleware('auth')->except('show');
    }

    /**
     * @var \App\Repositories\Product\ProductRepository
     */
    protected $products;

    /**
     * @var \App\Repositories\Category\CategoryRepository
     */
    protected $categories;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $categories = $this->categories->renderWithProducts();
        if ($request->ajax()) {
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent($categories);

            return $response;
        } else {
            return view::make('categories.index')
                ->with('categories', $categories);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->categories->render();

        return View::make('categories.create')->with(['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $rules     = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            if ($request->ajax()) {
                return new Response(['errors' => $validator->getMessageBag()->toArray], 400);
            } else {
                return Redirect::to('categories/create')
                    ->withErrors($validator);
            }
        } else {
            $category              = new Category();
            $category->name        = $request->get('name');
            $category->description = $request->get('description', '');
            $category->image       = '';
            $category->save();

            //redirect
            if ($request->ajax()) {
                $response = new Response();
                $response->setStatusCode(200);
                $response->setContent(json_encode($category));

                return $response;
            } else {
                Session::flash('message', 'Succesfully created category');

                return Redirect::to('categories');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @param  int      $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, int $id)
    {
        Log::info("Entering category show function. Here's the category id : ". $id);
        $locale = strtoupper(App::getLocale());
        $country            = Cookie::get('country');
        if ($locale !== "ES" && $locale !== "FR" && $locale !== "DE" && $locale !== "EN" && $locale !== "NL" && $locale !== "IT") {
            $locale = 'EN';
        }



        if ($country !== "ES" && $country !== "FR" && $country !== "DE" && $country !== "UK" && $country !== "IT" && $country !== "US") {
            $country = 'DE';
            $locale = 'EN';
        }

        if ($country === "ES"){
            $locale =  "ES";
        }

        if ($country === "FR"){
            $locale =  "FR";
        }

        $sortBy = $request->input('sort') ?? 'updated_desc';
        $products = $this->getSortedProducts($sortBy, $locale,  $country, $id);
        $category = Category::find($id);

        $categories = $this->categories->render();

        $route = route('categories.show', ['category' => $id]);

        $availabilityString = $this->createAvailabilityString();
        $title              = trans('categories.homekit_compatible') . ' ' . strtolower(trans('categories.' . strtolower(str_replace(' ',
                    '_', $category->name)))) . ' ' . $availabilityString;

        if ($country === 'UK') {
            $currency = render_currency('GPB');
        } elseif ($country === 'US'){
            $currency = render_currency('USD');
        }
        else {
            $currency = render_currency('EUR');
        }

        return view::make('master')->with(['products'     => $products, 'categories' => $categories, 'title' => $title,
                                           'categoryName' => strtolower($category->name), 'route' => $route, 'currency' => $currency
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $category = Category::find($id);
        $category->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the category!');

        return Redirect::to('categories');
    }

    /**
     * @return string
     */
    private function createAvailabilityString()
    {
        $country            = Cookie::get('country');
        if (is_null($country)) {
            $availabilityString = "";
        }
        else {
            $availabilityString = strtolower(trans('products.available_in')) . ' ' . trans('countries.' . $country);
        }
        return $availabilityString;
    }
}
