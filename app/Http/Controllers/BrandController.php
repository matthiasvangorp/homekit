<?php

namespace App\Http\Controllers;

use App\Models\AmazonProduct;
use App\Models\Brand;
use App\Models\Category;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class BrandController extends Controller
{
    /**
     * BrandController constructor.
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->products = $productRepository;
        $this->middleware('auth')->except('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $brands     = Brand::all();
        $categories = Category::all();
        if ($request->ajax()) {
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent($brands);

            return $response;
        } else {
            return view::make('brands.index')->with(['brands' => $brands, 'categories' => $categories]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $brands     = Brand::all();
        $categories = Category::all();

        return View::make('brands.create')->with(['brands' => $brands, 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $rules     = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            if ($request->ajax()) {
                $response = new Response();
                $response->setStatusCode(400);
                $response->setContent(['errors' => $validator->getMessageBag()->toArray]);
                //return Response::json(['errors' => $validator->getMessageBag()->toArray], 400);
            } else {
                return Redirect::to('brands/create')
                    ->withErrors($validator);
            }
        } else {
            $brand       = new Brand();
            $input       = $request->all();
            $brand->name = $request->get('name');
            if (is_null($request->get('description'))) {
                $brand->description = '';
            } else {
                $brand->description = $request->get('description');
            }
            $brand->image = '';
            $brand->link  = $request->get('link');
            $brand->save();

            //redirect
            if ($request->get('ajax')) {
                $response = new Response();
                $response->setStatusCode(200);
                $response->setContent(json_encode($brand));

                return $response;
            } else {
                Session::flash('message', 'Succesfully created brand');

                return Redirect::to('brands');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $locale = strtoupper(App::getLocale());
        $country            = Cookie::get('country');
        if ($locale !== "ES" && $locale !== "FR" && $locale !== "DE" && $locale !== "EN" && $locale !== "NL" && $locale !== "IT") {
            $locale = 'EN';
        }



        if ($country !== "ES" && $country !== "FR" && $country !== "DE" && $country !== "UK" && $country !== "IT" && $country !== "US") {
            $country = 'DE';
            $locale = 'EN';
        }


        if ($country === "ES"){
            $locale =  "ES";
        }

        if ($country === "FR"){
            $locale =  "FR";
        }


        $products = AmazonProduct::where('brand_id', $id)->where('locale', $locale)->where('country', $country)->paginate(30);
        $brand    = Brand::find($id);


        if ($country === 'UK') {
            $currency = render_currency('GPB');
        } elseif ($country === 'US'){
            $currency = render_currency('USD');
        }
        else {
            $currency = render_currency('EUR');
        }

        return View::make('brands.show')
            ->with(['brand' => $brand, 'products' => $products, 'currency' => $currency]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $brand          = Brand::find($id);
        $categories     = Category::all();
        $categoriesList = Category::pluck('name', 'id');
        $brands         = Brand::pluck('name', 'id');

        return View::make('brands.edit')->with(['brand'          => $brand, 'categories' => $categories,
                                                'categoriesList' => $categoriesList, 'brands' => $brands
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate
        $rules     = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('brands/create')
                ->withErrors($validator);
        } else {
            $input       = $request->all();
            $brand       = Brand::find($id);
            $brand->name = $request->get('name');
            if (is_null($request->get('description'))) {
                $brand->description = '';
            } else {
                $brand->description = $request->get('description');
            }
            $brand->link = $request->get('link');
            $brand->save();

            //redirect
            Session::flash('message', 'Succesfully updated brand');

            return Redirect::to('brands');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $brand = Brand::find($id);
        $brand->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the product!');

        return Redirect::to('brands');
    }
}
