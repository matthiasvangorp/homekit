<?php

namespace App\Http\Controllers;

use App\AffiliateLink;
use App\Models\AmazonProduct;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Country;
use App\Product;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Spatie\Tags\Tag;

class ProductController extends Controller
{
    /**
     * ProductController constructor.
     * @param  ProductRepository   $products
     * @param  CategoryRepository  $categories
     */
    public function __construct(ProductRepository $products, CategoryRepository $categories)
    {
        $this->middleware('auth')->except(['show', 'search', 'byTag']);
        $this->products   = $products;
        $this->categories = $categories;
    }

    /**
     * @var ProductRepository
     */
    protected $products;

    /**
     * @var \App\Repositories\Product\CategoryRepository
     */
    protected $categories;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        $products   = $this->products->renderForBackend();
        $categories = $this->categories->renderWithProducts();

        return View::make('products.index')->with(['products' => $products, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->middleware('auth');
        $categories     = $this->categories->renderWithProducts();
        $categoriesList = Category::orderBy('name')->pluck('name', 'id');
        $brands         = Brand::orderBy('name')->pluck('name', 'id');

        foreach (config('countries') as $key => $value) {
            $countries[$key] = trans('countries.' . $key);
        }

        return View::make('products.create')->with([
            'categories' => $categories, 'categoriesList' => $categoriesList, 'brands' => $brands,
            'countries'  => $countries, 'stores' => config('stores')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->middleware('auth');
        $rules     = [
            'title'    => 'required',
            'brand'    => 'required',
            'category' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('products/create')
                ->withErrors($validator);
        } else {
            $product        = new Product();
            $product->title = $request->get('title');
            if (is_null($request->get('description'))) {
                $product->description = '';
            } else {
                $product->description = $request->get('description');
            }
            if ($request->file('image')) {
                $filename       = (str_replace(' ', '-', strtolower($product->title)));
                $filename       = preg_replace('/[^A-Za-z0-9 ]/', '_', $filename);
                $imageDirectory = public_path() . '/images/';
                $imageName      = $filename . '.' .
                    $request->file('image')->getClientOriginalExtension();

                $request->file('image')->move($imageDirectory, $imageName);
                $product->image = $imageName;
            } else {
                $product->image = '';
            }

            $product->link_coolblue  = $request->get('link_coolblue');
            $product->link_bol       = $request->get('link_bol');
            $product->link_amazon    = $request->get('link_amazon');
            $product->category_id    = $request->get('category');
            $product->brand_id       = $request->get('brand');
            $product->score_coolblue = $request->get('score_coolblue');
            $product->score_bol      = $request->get('score_bol');
            $product->score_amazon   = $request->get('score_amazon');
            if (!$request->get('needs_hub')) {
                $product->needs_hub = 0;
            } else {
                $product->needs_hub = $request->get('needs_hub');
            }

            if (!$request->get('unavailable')) {
                $product->unavailable = 0;
            } else {
                $product->unavailable = $request->get('unavailable');
            }

            if (!$request->get('thread')) {
                $product->thread = 0;
            } else {
                $product->thread = $request->get('thread');
            }
            $product->save();

            //save the countries
            foreach ($request->get('countries') as $countryCode) {
                $country             = new Country();
                $country->code       = $countryCode;
                $country->product_id = $product->id;
                $country->save();
            }

            //save the affiliate url's

            $urls                = $request->get('urls');
            $stores              = $request->get('stores');
            $affiliate_countries = $request->get('affiliate_countries');
            $languages           = $request->get('languages');
            $price               = $request->get('price');
            $currency            = $request->get('currency');
            $external_id         = $request->get('external_id');

            foreach ($urls as $key => $url) {
                $affiliate_link              = new AffiliateLink();
                $affiliate_link->url         = $url;
                $affiliate_link->store       = $stores[$key];
                $affiliate_link->external_id = $product->external_id;
                $affiliate_link->language    = $languages[$key];
                $affiliate_link->country     = $affiliate_countries[$key];
                $affiliate_link->price       = $price[$key];
                $affiliate_link->currency    = $currency[$key];
                $affiliate_link->product_id  = $product->id;
                $affiliate_link->save();
            }

            //redirect
            Session::flash('message', 'Succesfully created product');

            return Redirect::to('products');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        Log::info("Trying to show product with id " .$id ." to visitor");
        $locale = strtoupper(App::getLocale());
        $country            = Cookie::get('country');

        $product = AmazonProduct::find($id);
        if ($product) {
            Log::info("Visitor looking at product with id ". $id . " (". $product->title . ")");
            if ($product->locale === 'NL') {
                $otherProducts = AmazonProduct::where('asin', $product->asin)->whereNotIn('locale',
                    [$product->locale])->whereNotIn('locale',
                    ['DE'])->whereNotIn('locale',
                    ['EN'])->get();
            }
            if ($product->locale === 'EN') {
                $otherProducts = AmazonProduct::where('asin', $product->asin)->whereNotIn('locale',
                    [$product->locale])->whereNotIn('locale',
                    ['NL'])->whereNotIn('locale',
                    ['DE'])->get();
            }
            if ($product->locale === 'DE') {
                $otherProducts = AmazonProduct::where('asin', $product->asin)->whereNotIn('locale',
                    [$product->locale])->whereNotIn('locale',
                    ['NL'])->whereNotIn('locale',
                    ['EN'])->get();
            }
            if ($product->locale === 'FR') {
                $otherProducts = AmazonProduct::where('asin', $product->asin)->whereNotIn('locale',
                    [$product->locale])->whereNotIn('locale',
                    ['DE'])->whereNotIn('locale',
                    ['NL'])->get();
            }
            if ($product->locale === 'ES') {
                $otherProducts = AmazonProduct::where('asin', $product->asin)->whereNotIn('locale',
                    [$product->locale])->whereNotIn('locale', ['DE'])->whereNotIn('locale',
                    ['NL'])->get();
            }
            if ($product->locale === 'IT') {
                $otherProducts = AmazonProduct::where('asin', $product->asin)->whereNotIn('locale',
                    [$product->locale])->whereNotIn('locale',
                    ['DE'])->whereNotIn('locale',
                    ['NL'])->get();
            }
            if ($product->locale === 'EN' && $product->country === 'UK') {
                $otherProducts = AmazonProduct::where('asin', $product->asin)->whereNotIn('locale',
                    ['NL'])->whereNotIn('country', ['UK'])->whereNotIn('locale', ['EN'])->get();
            }

            $categories = $this->categories->render();

            return View::make('products.show')
                ->with([
                    'product'           => $product,
                    'otherProducts'     => $otherProducts,
                    'categories'        => $categories,
                    'title'             => $product->brand->name . ' ' . $product->title . ' - ' . trans('products.homekit_product_guide'),
                ]);
        } else {
            //product not found. Redirect
            return \redirect('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->middleware('auth');
        $product        = Product::find($id);
        $categories     = $this->categories->renderWithProducts();
        $categoriesList = Category::pluck('name', 'id');
        $brands         = Brand::pluck('name', 'id');

        foreach (config('countries') as $key => $value) {
            $countries[$key] = trans('countries.' . $key);
        }

        $selectedCountries = $product->countries()->pluck('code');

        return View::make('products.edit')->with([
            'product' => $product, 'categories' => $categories, 'categoriesList' => $categoriesList,
            'brands'  => $brands, 'countries' => $countries, 'selectedCountries' => $selectedCountries,
            'stores'  => config('stores')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->middleware('auth');
        //validate
        $rules     = [
            'title'    => 'required',
            'brand'    => 'required',
            'category' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('products/create')
                ->withErrors($validator);
        } else {
            $product        = Product::find($id);
            $product->title = $request->get('title');
            if (is_null($request->get('description'))) {
                $product->description = '';
            } else {
                $product->description = $request->get('description');
            }
            if ($request->file('image')) {
                $filename       = (str_replace(' ', '-', strtolower($product->title)));
                $filename       = preg_replace('/[^A-Za-z0-9 ]/', '_', $filename);
                $imageDirectory = public_path() . '/images/';
                $imageName      = $filename . '.' .
                    $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move($imageDirectory, $imageName);
                $product->image = $imageName;
            }
            $product->link_coolblue  = $request->get('link_coolblue');
            $product->link_bol       = $request->get('link_bol');
            $product->link_amazon    = $request->get('link_amazon');
            $product->category_id    = $request->get('category');
            $product->brand_id       = $request->get('brand');
            $product->score_conrad   = $request->get('score_conrad');
            $product->score_coolblue = $request->get('score_coolblue');
            $product->score_bol      = $request->get('score_bol');
            $product->score_amazon   = $request->get('score_amazon');
            if (!$request->get('needs_hub')) {
                $product->needs_hub = 0;
            } else {
                $product->needs_hub = $request->get('needs_hub');
            }

            if (!$request->get('unavailable')) {
                $product->unavailable = 0;
            } else {
                $product->unavailable = $request->get('unavailable');
            }

            if (!$request->get('thread')) {
                $product->thread = 0;
            } else {
                $product->thread = $request->get('thread');
            }
            $product->save();

            //remove all currently saved countries
            $product->countries()->delete();

            foreach ($request->get('countries') as $countryCode) {
                $country             = new Country();
                $country->code       = $countryCode;
                $country->product_id = $product->id;
                $country->save();
            }

            //save the affiliate url's
            $urls                = $request->get('urls');
            $ids                 = $request->get('affiliate_link_ids');
            $stores              = $request->get('stores');
            $affiliate_countries = $request->get('affiliate_countries');
            $languages           = $request->get('languages');
            $price               = $request->get('price');
            $currency            = $request->get('currency');
            $external_id         = $request->get('external_id');

            $lastFound = "";

            foreach ($urls as $key => $url) {
                if ($lastFound == $ids[$key]) {
                    $affiliate_link = new AffiliateLink();
                } else {
                    $affiliate_link = AffiliateLink::find($ids[$key]);
                }

                $affiliate_link->url         = $url;
                $affiliate_link->store       = $stores[$key];
                $affiliate_link->external_id = $external_id[$key];
                $affiliate_link->language    = $languages[$key];
                $affiliate_link->country     = $affiliate_countries[$key];
                $affiliate_link->currency    = $currency[$key];
                $affiliate_link->price       = $price[$key];
                $affiliate_link->product_id  = $product->id;
                $affiliate_link->save();

                $lastFound = $affiliate_link->id;
            }

            //redirect
            Session::flash('message', 'Succesfully updated product');

            return Redirect::to('products');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->middleware('auth');
        // delete
        $product = Product::find($id);
        $product->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the product!');

        return Redirect::to('products');
    }

    public function search(Request $request)
    {
        $query = $request->input('query');
        $locale = strtoupper(App::getLocale());
        $country = $request->cookie('country');
        Log::info('Locale : ' . $locale);
        Log::info('Country : ' . $country);
        if ($locale !== "ES" && $locale !== "FR" && $locale !== "DE" && $locale !== "EN" && $locale !== "NL" && $locale !== "IT") {
            $locale = 'EN';
        }

        if ($country !== "ES" && $country !== "FR" && $country !== "DE" && $country !== "UK" && $country !== "IT" && $country !== "US") {
            $country = 'DE';
            $locale = 'EN';
        }

        if ($country === "ES"){
            $locale =  "ES";
        }

        if ($country === "FR"){
            $locale =  "FR";
        }

        $products = AmazonProduct::query()
            ->where('locale', $locale)
            ->where('country', $country)
            ->where(function ($q) use ($query) {
                $q->where('title', 'LIKE', "%{$query}%")
                    ->orWhere('description', 'LIKE', "%{$query}%");
            })
            ->where('publish', 1)
            ->paginate(10);

        $products->appends(['query' => $query]);

        return view('products.search', [
            'products' => $products,
            'query' => $query,
            'categories' => $this->categories->render(),
        ]);
    }

    public function byTag(Request $request, $tagSlug)
    {
        $locale = app()->getLocale();
        $sort = $request->input('sort', 'updated_desc');
        $country = $request->cookie('country');

        // Find the tag
        $tag = Tag::where('slug->en', $tagSlug)
            ->where('type', 'amazon-products')
            ->first();

        if (!$tag) {
            abort(404, 'Tag not found');
        }

        // Get the IDs of products associated with this tag
        $productIds = DB::table('taggables')
            ->where('tag_id', $tag->id)
            ->pluck('taggable_id');

        // Start building the query
        $query = AmazonProduct::whereIn('id', $productIds)
            ->where(function ($query) use ($locale) {
                $query->where('locale', $locale)
                    ->orWhereNull('locale');
            })
            ->where('publish', true);

        // Apply country-based filtering
        if ($country === 'US' || $country === 'UK') {
            $query->where('country', $country);
        } else {
            // For other countries, we need to get the lowest price for each ASIN
            // excluding US and UK products
            $lowestPriceProducts = AmazonProduct::whereIn('id', $productIds)
                ->whereNotIn('country', ['US', 'UK'])
                ->groupBy('asin')
                ->selectRaw('MIN(price) as min_price, asin')
                ->pluck('min_price', 'asin');

            $query->where(function ($q) use ($lowestPriceProducts) {
                foreach ($lowestPriceProducts as $asin => $price) {
                    $q->orWhere(function ($subQ) use ($asin, $price) {
                        $subQ->where('asin', $asin)
                            ->where('price', $price)
                            ->whereNotIn('country', ['US', 'UK']);
                    });
                }
            });
        }

        // Apply sorting
        switch ($sort) {
            case 'price_asc':
                $query->orderBy('price', 'asc');
                break;
            case 'price_desc':
                $query->orderBy('price', 'desc');
                break;
            case 'updated_asc':
                $query->orderBy('updated_at', 'asc');
                break;
            case 'updated_desc':
                $query->orderBy('updated_at', 'desc');
                break;
            case 'name_asc':
                $query->orderBy('title', 'asc');
                break;
            case 'name_desc':
                $query->orderBy('title', 'desc');
                break;
        }

        $products = $query->paginate(50);

        return view('products.by_tag', [
            'tag' => $tag,
            'products' => $products,
            'categories' => $this->categories->render(),
            'currentSort' => $sort,
            'userCountry' => $country,
        ]);
    }

}
