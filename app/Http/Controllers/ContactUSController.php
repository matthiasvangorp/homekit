<?php

namespace App\Http\Controllers;

use App\ContactUS;
use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUSController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactUS()
    {
        return view('contactUs');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactUSPost(Request $request)
    {
        $this->validate($request, [
      'name' => 'required',
      'email' => 'required|email',
      'message' => 'required',
    ]);

        $contact = ContactUS::create($request->all());

        Mail::to($request->input('email'))->send(new Contact($contact));

        return back()->with('success', 'Thanks for contacting us!');
    }
}
