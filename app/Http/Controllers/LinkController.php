<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Link\LinkRepository;

class LinkController extends Controller
{
    /**
     * @var \App\Repositories\Product\LinkRepository
     */
    protected $links;

    /**
     * LinkController constructor.
     * @param \App\Repositories\Link\LinkRepository $links
     */
    public function __construct(LinkRepository $links)
    {
        $this->middleware('auth')->except(['index', 'show']);
        $this->links = $links;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = $this->links->render();

        return View::make('links.index')->with(['links'=> $links]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('links.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->links->create($request->input());
        $links = $this->links->render();

        return Redirect::to('links');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $link = $this->links->renderOne($id);

        return View::make('links.edit')->with(['link' => $link]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->links->update($request->input(), $id);

        //redirect
        return Redirect::to('links');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
