<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Cookie\CookieJar;

class CookieController extends Controller
{
    //

    public function create(CookieJar $cookieJar, Request $request)
    {
        $cookieJar->queue(cookie()->forever('country', $request->country));
        $response = new Response();
        $response->setStatusCode(200);

        return $response;
    }
}
