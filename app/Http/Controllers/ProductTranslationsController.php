<?php

namespace App\Http\Controllers;

use App\Models\AmazonProduct;
use App\Models\Brand;
use App\Models\Category;
use App\ProductTranslations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ProductTranslationsController extends Controller
{
    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = ProductTranslations::all();
        $categories = Category::all();

        return View::make('producttranslations.index')->with(['products'=> $products, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = AmazonProduct::find($id);
        $categories = Category::all();
        $categoriesList = Category::pluck('name', 'id');
        $brands = Brand::pluck('name', 'id');

        return View::make('producttranslations.edit')->with(['product' => $product, 'categories' => $categories, 'categoriesList' => $categoriesList, 'brands' => $brands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        //validate
        $rules = [
          'title' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('products/create')
            ->withErrors($validator);
        } else {
            //check if this translation already exists
            $existingProduct = AmazonProduct::find($id);
            $newProduct = $existingProduct->replicate();
            $newProduct->title = $request->title;
            $newProduct->description = $request->description;
            $newProduct->url = $request->url;
            $newProduct->price = $request->price;
            $newProduct->locale = 'EN';
            $newProduct->country = 'UK';
            $newProduct->save();

            return Redirect::to('amazon_products');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
