<?php

use Illuminate\Support\Facades\Log;

/**
 * @param string $url
 * @return string
 */
function render_amazon(string $url): string {
    Log::info("Rendering amazon : " . $url);
    $arrUrl = explode('/', $url);

    // Check if the array has at least 3 elements
    if (count($arrUrl) >= 3) {
        $name = $arrUrl[2];
        $name = str_replace('www.amazon', 'Amazon', $name);
        return $name;
    } else {
        // Handle the case where the URL doesn't have the expected format
        Log::error("Unexpected URL format: " . $url);
        return "Amazon"; // or return some default value
    }
}

function render_currency(string $currency): string
{
    if ($currency == 'EUR') {
        return '€';
    }
    if ($currency == 'GPB') {
        return '£';
    }
    if ($currency == 'USD') {
        return '$';
    }

    if ($currency == 'HUF') {
        return 'HUF';
    }
}
