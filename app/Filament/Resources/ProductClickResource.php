<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ProductClickResource\Pages;
use App\Models\ProductClick;
use Filament\Forms;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Filament\Tables\Filters\Filter;
use Filament\Forms\Components\DatePicker;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Support\Facades\DB;

class ProductClickResource extends Resource
{
    protected static ?string $model = ProductClick::class;

    protected static ?string $navigationIcon = 'heroicon-o-cursor-arrow-rays';

    protected static ?string $navigationGroup = 'Analytics';

    public static function table(Tables\Table $table): Tables\Table
    {
        return $table
            ->columns([
                TextColumn::make('clicked_at')
                    ->label('Date and Time')
                    ->dateTime('d/m/Y H:i')  // Format: Day/Month/Year Hour:Minute
                    ->sortable(),
                ImageColumn::make('amazonProduct.image')
                    ->label('Product Image')
                    ->square()
                    ->defaultImageUrl(url('/path/to/default-image.jpg')),
                TextColumn::make('amazon_product_id')
                    ->label('Product ID')
                    ->searchable(),
                TextColumn::make('amazonProduct.title')
                    ->label('Product Name')
                    ->limit(50)
                    ->searchable(),
                TextColumn::make('country')
                    ->searchable(),
                TextColumn::make('click_count')
                    ->label('Click Count')
                    ->sortable(),
            ])
            ->filters([
                Filter::make('date_range')
                    ->form([
                        DatePicker::make('from'),
                        DatePicker::make('until'),
                    ])
                    ->query(function (Builder $query, array $data): Builder {
                        return $query
                            ->when(
                                $data['from'],
                                fn (Builder $query, $date): Builder => $query->whereDate('clicked_at', '>=', $date),
                            )
                            ->when(
                                $data['until'],
                                fn (Builder $query, $date): Builder => $query->whereDate('clicked_at', '<=', $date),
                            );
                    }),
                SelectFilter::make('country')
                    ->options(function () {
                        return ProductClick::distinct()
                            ->orderBy('country')
                            ->pluck('country', 'country')
                            ->toArray();
                    })
                    ->multiple()
                    ->label('Country'),
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ])
            ->modifyQueryUsing(function (Builder $query) {
                return $query
                    ->select('amazon_product_id', 'country', 'clicked_at')  // Include full clicked_at
                    ->addSelect(DB::raw('COUNT(*) as click_count'))
                    ->addSelect(DB::raw('MAX(id) as id'))
                    ->groupBy('amazon_product_id', 'country', 'clicked_at')
                    ->orderBy('clicked_at', 'desc');
            });
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListProductClicks::route('/'),
            'view' => Pages\ViewProductClick::route('/{record}'),
        ];
    }
}