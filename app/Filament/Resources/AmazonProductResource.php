<?php

namespace App\Filament\Resources;

use App\Filament\Resources\AmazonProductResource\Pages;
use App\Models\AmazonProduct;
use App\Models\Brand;
use App\Services\AmazonProductTaggingService;
use Filament\Forms;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\HtmlString;
use Spatie\Tags\Tag;
use Illuminate\Support\Facades\Log;

class AmazonProductResource extends Resource
{
    protected static ?string $model = AmazonProduct::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Placeholder::make('image')
                    ->content(
                        fn (Model $record) => new HtmlString('<img src="' . $record->image . '" alt="" />')),
                Forms\Components\TextInput::make('asin')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->maxLength(255)
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('url')
                    ->required()
                    ->maxLength(2048)
                    ->url(),
                Forms\Components\RichEditor::make('description')
                    ->maxLength(65535)
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('image')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('score')
                    ->numeric()
                    ->step(0.01),
                Forms\Components\TextInput::make('price')
                    ->numeric()
                    ->step(0.01),
                Forms\Components\Select::make('brand_id')
                    ->relationship('brand', 'name')
                    ->searchable()
                    ->preload()
                    ->createOptionForm([
                        Forms\Components\TextInput::make('name')
                            ->required()
                            ->maxLength(255),
                    ]),
                Forms\Components\Select::make('category_id')
                    ->relationship('category', 'name')
                    ->searchable()
                    ->preload(),
                Forms\Components\Select::make('locale')
                    ->options([
                        'DE' => 'German',
                        'FR' => 'French',
                        'EN' => 'English',
                        'ES' => 'Spanish',
                        'IT' => 'Italian',
                        'NL' => 'Dutch',
                    ])
                    ->required(),
                Forms\Components\Select::make('country')
                    ->options([
                        'DE' => 'Germany',
                        'FR' => 'France',
                        'UK' => 'United Kingdom',
                        'ES' => 'Spain',
                        'IT' => 'Italy',
                        'US' => 'United States',
                    ])
                    ->required(),
                Forms\Components\Toggle::make('publish')
                    ->required(),
                Forms\Components\TagsInput::make('tags')
                    ->separator(',')
                    ->suggestions(function () {
                        $suggestions = Tag::getWithType('amazon-products')
                            ->pluck('name', 'name')
                            ->filter()
                            ->toArray();
                        Log::info('Tag suggestions loaded', ['suggestions' => $suggestions]);
                        return $suggestions;
                    })
                    ->afterStateHydrated(function (Forms\Components\TagsInput $component, $state) {
                        Log::info('afterStateHydrated called', ['state' => $state]);
                        if ($state instanceof AmazonProduct) {
                            $tags = $state->tags->pluck('name')->toArray();
                            Log::info('Hydrating tags for product', ['product_id' => $state->id, 'tags' => $tags]);
                            $component->state($tags);
                        } elseif (is_array($state)) {
                            Log::info('State is already an array', ['state' => $state]);
                            $component->state($state);
                        } else {
                            Log::warning('Unexpected state type in afterStateHydrated', ['state_type' => gettype($state)]);
                        }
                    })
                    ->afterStateUpdated(function ($state, $record, AmazonProductTaggingService $taggingService) {
                        Log::info('afterStateUpdated called', ['state' => $state, 'record_id' => $record ? $record->id : null]);
                        if ($record) {
                            $taggingService->syncTagsAcrossStores($record, $state ?? []);
                        }
                    })
                    ->loadStateFromRelationshipsUsing(function (Forms\Components\TagsInput $component, AmazonProduct $record) {
                        $tags = $record->tags->pluck('name')->toArray();
                        Log::info('Loading tags from relationship', ['product_id' => $record->id, 'tags' => $tags]);
                        $component->state($tags);
                    })
                    ->saveRelationshipsUsing(function (AmazonProduct $record, $state, AmazonProductTaggingService $taggingService) {
                        Log::info('Saving tags relationship', ['product_id' => $record->id, 'tags' => $state]);
                        $taggingService->syncTagsAcrossStores($record, $state);
                    })
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\ImageColumn::make('image')
                    ->square()
                    ->defaultImageUrl(url('/path/to/default-image.jpg')),
                Tables\Columns\TextColumn::make('asin')
                    ->searchable(),
                Tables\Columns\TextColumn::make('title')
                    ->searchable()
                    ->limit(50)
                    ->tooltip(function (Tables\Columns\TextColumn $column): ?string {
                        $state = $column->getState();
                        return strlen($state) > 50 ? $state : null;
                    }),
                Tables\Columns\TextColumn::make('price')
                    ->money('EUR')
                    ->sortable(),
                Tables\Columns\TextColumn::make('brand.name')
                    ->sortable(),
                Tables\Columns\TextColumn::make('category.name')
                    ->sortable(),
                Tables\Columns\TextColumn::make('locale')
                    ->sortable(),
                Tables\Columns\TextColumn::make('country')
                    ->sortable(),
                Tables\Columns\IconColumn::make('publish')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('tags')
                    ->getStateUsing(function (AmazonProduct $record): string {
                        $tags = $record->tags()->pluck('name')->filter()->toArray();
                        return implode(', ', $tags);
                    })
            ])
            ->defaultSort('created_at', 'desc')
            ->persistSortInSession('amazon_products_sort')
            ->filters([
                SelectFilter::make('brand')
                    ->label('Brand')
                    ->multiple()
                    ->preload()
                    ->relationship('brand', 'name'),
                SelectFilter::make('category')
                    ->relationship('category', 'name'),
                SelectFilter::make('locale')
                    ->options([
                        'DE' => 'German',
                        'FR' => 'French',
                        'EN' => 'English',
                        'ES' => 'Spanish',
                        'IT' => 'Italian',
                        'NL' => 'Dutch',
                    ]),
                SelectFilter::make('country')
                    ->options([
                        'DE' => 'Germany',
                        'FR' => 'France',
                        'UK' => 'United Kingdom',
                        'ES' => 'Spain',
                        'IT' => 'Italy',
                        'US' => 'United States',
                    ]),
                Tables\Filters\TernaryFilter::make('publish'),
                SelectFilter::make('tags')
                    ->multiple()
                    ->options(function () {
                        return Tag::getWithType('amazon-products')
                            ->pluck('name', 'id')
                            ->filter()
                            ->toArray();
                    })
                    ->query(function (Builder $query, array $data): Builder {
                        return $query->when(
                            $data['values'],
                            function (Builder $query, array $values) {
                                return $query->whereHas('tags', function ($q) use ($values) {
                                    $q->whereIn('id', $values);
                                });
                            }
                        );
                    })
            ])
            ->modifyQueryUsing(function (Builder $query) {
                $query->with('tags');
            })
            ->persistFiltersInSession()
            ->actions([
                Tables\Actions\EditAction::make()
                    ->url(fn ($record) => static::getUrl('edit', ['record' => $record, 'referrer-query-string' => urlencode(http_build_query(request()->except('page')))]))
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }


    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAmazonProducts::route('/'),
            'create' => Pages\CreateAmazonProduct::route('/create'),
            'edit' => Pages\EditAmazonProduct::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        $query = parent::getEloquentQuery();

        // Simplify the query to just select all products
        $query->select('amazon_products.*');

        // Add logging to see the generated SQL query and the count of results
        Log::info('AmazonProductResource query', [
            'sql' => $query->toSql(),
            'bindings' => $query->getBindings(),
        ]);

        // Log the count of products
        $count = $query->count();
        Log::info('AmazonProductResource product count', ['count' => $count]);

        // Log the first few products
        $sampleProducts = $query->limit(5)->get();
        Log::info('Sample products', ['products' => $sampleProducts->toArray()]);

        return $query;
    }


    public static function getGloballySearchableAttributes(): array
    {
        return ['tags.name'];
    }
}