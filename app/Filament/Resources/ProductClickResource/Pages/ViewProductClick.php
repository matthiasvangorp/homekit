<?php

namespace App\Filament\Resources\ProductClickResource\Pages;

use App\Filament\Resources\ProductClickResource;
use Filament\Infolists\Components\ImageEntry;
use Filament\Resources\Pages\ViewRecord;
use Filament\Infolists\Components\Section;
use Filament\Infolists\Components\TextEntry;
use Filament\Infolists\Components\RepeatableEntry;
use Filament\Infolists\Infolist;

class ViewProductClick extends ViewRecord
{
    protected static string $resource = ProductClickResource::class;

    public function infolist(Infolist $infolist): Infolist
    {
        return $infolist
            ->schema([
                TextEntry::make('clicked_at')
                    ->state(fn ($record) => "Clicked At: {$record->clicked_at}")
                    ->label('Click Details'),
                TextEntry::make('amazonProduct.title')
                    ->state(fn ($record) => "Product: {$record->amazonProduct->title}")
                    ->label(''),
                ImageEntry::make('amazonProduct.image')
                    ->label('Product Image')
                    ->height(300)
                    ->width(300)
                    ->extraImgAttributes(['style' => 'object-fit: contain;'])
                    ->defaultImageUrl(url('/path/to/default-image.jpg')),
                TextEntry::make('country')
                    ->state(fn ($record) => "Country: {$record->country}")
                    ->label(''),
                TextEntry::make('tracking_id')
                    ->state(fn ($record) => "Tracking ID: {$record->tracking_id}")
                    ->label(''),
                TextEntry::make('userSession.entry_time')
                    ->state(fn ($record) => "Session Start: {$record->userSession->entry_time}")
                    ->label('User Session'),
                TextEntry::make('userSession.entry_page')
                    ->state(fn ($record) => "Entry Page: {$record->userSession->entry_page}")
                    ->label(''),
                TextEntry::make('userSession.referrer')
                    ->state(fn ($record) => "Referrer: " . ($record->userSession->referrer ?? 'N/A'))
                    ->label(''),
                TextEntry::make('userSession.ip_address')
                    ->state(fn ($record) => "IP Address: {$record->userSession->ip_address}")
                    ->label(''),
                TextEntry::make('userSession.country')
                    ->state(fn ($record) => "Country: " . ($record->userSession->country ?? 'N/A'))
                    ->label(''),
                TextEntry::make('userSession.city')
                    ->state(fn ($record) => "City: " . ($record->userSession->city ?? 'N/A'))
                    ->label(''),
                TextEntry::make('userSession.browser')
                    ->state(fn ($record) => "Browser: {$record->userSession->browser}")
                    ->label(''),
                TextEntry::make('userSession.device_type')
                    ->state(fn ($record) => "Device Type: {$record->userSession->device_type}")
                    ->label(''),
                RepeatableEntry::make('pageVisits')
                    ->schema([
                        TextEntry::make('timestamp')
                            ->state(fn ($record) => "Timestamp: {$record->timestamp}")
                            ->label(''),
                        TextEntry::make('url')
                            ->state(fn ($record) => "URL: {$record->url}")
                            ->label(''),
                    ])
                    ->columnSpanFull()
                    ->label('Page Visits'),
            ]);
    }
}