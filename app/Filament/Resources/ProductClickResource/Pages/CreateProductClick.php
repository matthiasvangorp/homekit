<?php

namespace App\Filament\Resources\ProductClickResource\Pages;

use App\Filament\Resources\ProductClickResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateProductClick extends CreateRecord
{
    protected static string $resource = ProductClickResource::class;
}
