<?php

namespace App\Filament\Resources\ProductClickResource\Pages;

use App\Filament\Resources\ProductClickResource;
use App\Filament\Widgets\ProductClicksChart;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Log;

class ListProductClicks extends ListRecords
{
    protected static string $resource = ProductClickResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }


    protected function getFooterWidgets(): array
    {
        return [
            ProductClicksChart::class,
        ];
    }
}