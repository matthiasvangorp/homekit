<?php

namespace App\Filament\Resources\AmazonProductResource\Pages;

use App\Filament\Resources\AmazonProductResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateAmazonProduct extends CreateRecord
{
    protected static string $resource = AmazonProductResource::class;
}
