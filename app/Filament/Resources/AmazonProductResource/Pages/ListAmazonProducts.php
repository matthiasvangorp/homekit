<?php

namespace App\Filament\Resources\AmazonProductResource\Pages;

use App\Filament\Resources\AmazonProductResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use Filament\Actions\Action;
use App\Jobs\CrawlAmazonProduct;
use Filament\Notifications\Notification;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Select;

class ListAmazonProducts extends ListRecords
{
    protected static string $resource = AmazonProductResource::class;

    protected ?string $maxContentWidth = 'full';


    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Action::make('scrapeProduct')
                ->label('Scrape Product')
                ->form([
                    TextInput::make('productUrl')
                        ->label('Amazon Product URL')
                        ->required()
                        ->url(),
                    Select::make('store')
                        ->label('Store')
                        ->options([
                            'UK' => 'UK',
                            'US' => 'US',
                            'IT' => 'IT',
                            'DE' => 'Germany',
                            'FR' => 'France',
                            'ES' => 'Spain',
                        ])
                        ->required(),
                ])
                ->action(function (array $data): void {
                    CrawlAmazonProduct::dispatch($data['productUrl'], $data['store']);
                    Notification::make()
                        ->title('Product crawling job has been queued')
                        ->success()
                        ->send();
                }),
        ];
    }
}
