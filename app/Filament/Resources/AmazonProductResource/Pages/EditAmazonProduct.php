<?php

namespace App\Filament\Resources\AmazonProductResource\Pages;

use App\Filament\Resources\AmazonProductResource;
use Filament\Actions;
use Filament\Notifications\Notification;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Support\Facades\Request;

class EditAmazonProduct extends EditRecord
{
    protected static string $resource = AmazonProductResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }


    protected function afterSave(): void
    {
        Notification::make()
            ->success()
            ->title('Amazon Product updated')
            ->body('The Amazon Product has been updated successfully.')
            ->send();

        $url = $this->getResource()::getUrl('index');
        $this->redirect( $url);
    }

    public function save(bool $shouldRedirect = true, bool $shouldSendSavedNotification = true): void
    {
        $originalUpdatedAt = $this->record->updated_at;

        parent::save($shouldRedirect, $shouldSendSavedNotification);

        $this->record->touch();

        \Log::info('Amazon Product Updated', [
            'id' => $this->record->id,
            'original_updated_at' => $originalUpdatedAt,
            'new_updated_at' => $this->record->fresh()->updated_at,
            'changes' => $this->record->getChanges(),
        ]);
    }
}
