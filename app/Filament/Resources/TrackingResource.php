<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TrackingResource\Pages;
use App\Models\UserSession;
use Filament\Forms;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Tables\Filters\Filter;
use Filament\Forms\Components\DatePicker;
use Illuminate\Support\Arr;
use Spatie\Tags\Tag;

class TrackingResource extends Resource
{
    protected static ?string $model = UserSession::class;

    protected static ?string $navigationIcon = 'heroicon-o-chart-bar';

    protected static ?string $navigationGroup = 'Analytics';

    public static function form(Forms\Form $form): Forms\Form
    {
        return $form
            ->schema([
                // Add form fields if needed
            ]);
    }

    public static function table(Tables\Table $table): Tables\Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('tracking_id'),
                Tables\Columns\TextColumn::make('entry_time'),
                Tables\Columns\TextColumn::make('entry_page'),
                Tables\Columns\TextColumn::make('referrer'),
                Tables\Columns\TextColumn::make('country'),
                Tables\Columns\TextColumn::make('city'),
                Tables\Columns\TextColumn::make('browser'),
                Tables\Columns\TextColumn::make('browser_locale'),
                Tables\Columns\TextColumn::make('device_type'),
                Tables\Columns\TextColumn::make('os'),
            ])
            ->filters([
                Filter::make('date_range')
                    ->form([
                        DatePicker::make('start_date'),
                        DatePicker::make('end_date'),
                    ])
                    ->query(function (Builder $query, array $data): Builder {
                        return $query
                            ->when(
                                $data['start_date'],
                                fn (Builder $query, $date): Builder => $query->whereDate('entry_time', '>=', $date),
                            )
                            ->when(
                                $data['end_date'],
                                fn (Builder $query, $date): Builder => $query->whereDate('entry_time', '<=', $date),
                            );
                    }),
                SelectFilter::make('country')
                    ->options(fn () => self::getFilterOptions('country')),
                SelectFilter::make('browser')
                    ->options(fn () => self::getFilterOptions('browser')),
                SelectFilter::make('browser_locale')
                    ->options(fn () => self::getFilterOptions('browser_locale')),
                SelectFilter::make('os')
                    ->options(fn () => self::getFilterOptions('os')),
            ])
            ->defaultSort('created_at', 'desc')
            ->actions([
                // Add actions if needed
            ])
            ->bulkActions([
                // Add bulk actions if needed
            ]);
    }

    private static function getFilterOptions(string $column): array
    {
        $options = UserSession::query()
            ->selectRaw("COALESCE($column, 'Unknown') as value")
            ->distinct()
            ->pluck('value')
            ->filter()
            ->mapWithKeys(function ($value) {
                return [$value => $value];
            })
            ->toArray();

        return Arr::sortRecursive($options);
    }

    public static function getRelations(): array
    {
        return [
            // Add relations if needed
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTrackings::route('/'),
            'create' => Pages\CreateTracking::route('/create'),
            'edit' => Pages\EditTracking::route('/{record}/edit'),
        ];
    }
}