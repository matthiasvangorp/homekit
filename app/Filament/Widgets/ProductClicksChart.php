<?php

namespace App\Filament\Widgets;

use App\Models\ProductClick;
use Filament\Widgets\ChartWidget;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Filament\Support\RawJs;

class ProductClicksChart extends ChartWidget
{
    protected static ?string $heading = 'Product Clicks by Country';

    protected int | string | array $columnSpan = 'full';

    protected static ?string $maxHeight = '300px';

    public ?string $filter = 'all';

    protected function getData(): array
    {
        Log::info('ProductClicksChart: getData method called', ['filter' => $this->filter]);

        $query = ProductClick::select('country', DB::raw('DATE(clicked_at) as date'), DB::raw('COUNT(*) as count'))
            ->when($this->filter, function ($query) {
                return match ($this->filter) {
                    'day' => $query->where('clicked_at', '>=', now()->subDay()),
                    'week' => $query->where('clicked_at', '>=', now()->subWeek()),
                    'month' => $query->where('clicked_at', '>=', now()->subMonth()),
                    'year' => $query->where('clicked_at', '>=', now()->subYear()),
                    default => $query
                };
            })
            ->groupBy('country', 'date')
            ->orderBy('date');

        Log::info('ProductClicksChart: Query', ['sql' => $query->toSql(), 'bindings' => $query->getBindings()]);

        $data = $query->get();

        Log::info('ProductClicksChart: Raw data fetched', ['count' => $data->count(), 'first_item' => $data->first()]);

        $datasets = [];
        $countries = $data->pluck('country')->unique();

        Log::info('ProductClicksChart: Unique countries', ['countries' => $countries->toArray()]);

        $colors = $this->generateColors(count($countries));

        foreach ($countries as $index => $country) {
            $countryData = $data->where('country', $country)->map(function ($item) {
                return [
                    'x' => $item->date,
                    'y' => $item->count
                ];
            })->values()->toArray();

            // Ensure we have a color for this country
            $color = $colors[$index] ?? $this->getRandomColor();

            $datasets[] = [
                'label' => $country,
                'data' => $countryData,
                'backgroundColor' => $this->hexToRgba($color, 0.2),
                'borderColor' => $color,
                'borderWidth' => 2,
            ];

            Log::info("ProductClicksChart: Dataset for country", ['country' => $country, 'data_count' => count($countryData), 'color' => $color]);
        }

        Log::info('ProductClicksChart: Final dataset', [
            'dataset_count' => count($datasets),
        ]);

        return [
            'datasets' => $datasets,
        ];
    }

    protected function getType(): string
    {
        return 'bar';
    }

    protected function getOptions(): RawJs
    {
        return RawJs::make(<<<JS
        {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Product Clicks by Country'
                }
            },
            scales: {
                x: {
                    type: 'time',
                    time: {
                        unit: 'day'
                    },
                    stacked: false
                },
                y: {
                    stacked: false,
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Number of Clicks'
                    }
                }
            }
        }
        JS);
    }

    protected function getFilters(): ?array
    {
        return [
            'all' => 'All Time',
            'year' => 'This Year',
            'month' => 'Last 30 Days',
            'week' => 'Last 7 Days',
            'day' => 'Last 24 hours',
        ];
    }

    private function generateColors(int $count): array
    {
        $colors = [
            '#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0', '#9966FF',
            '#FF9F40', '#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0'
        ];

        // If we need more colors than in our predefined list, generate random ones
        while (count($colors) < $count) {
            $colors[] = $this->getRandomColor();
        }

        return array_slice($colors, 0, $count);
    }

    private function getRandomColor(): string
    {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    private function hexToRgba(string $hex, float $alpha = 1): string
    {
        $hex = str_replace('#', '', $hex);

        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }

        return "rgba($r, $g, $b, $alpha)";
    }
}