<?php

namespace App\Filament\Pages;

use Filament\Forms\Components\TextInput;
use Filament\Pages\Page;
use App\Models\UserSession;
use Carbon\Carbon;
use Filament\Support\Facades\FilamentAsset;
use Illuminate\Support\Facades\DB;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Grid;
use Filament\Forms\Contracts\HasForms;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Table;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Http\Request;
use Filament\Tables\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;




class TrackingDashboard extends Page implements HasForms, HasTable
{
    use InteractsWithForms, InteractsWithTable;

    protected static ?string $navigationIcon = 'heroicon-o-presentation-chart-line';

    protected static string $view = 'filament.pages.tracking-dashboard';

    protected static ?string $navigationGroup = 'Analytics';

    public $startDate;
    public $endDate;
    public $data;

    protected function getHeaderActions(): array
    {
        FilamentAsset::registerScriptData([
            'chart-js',
        ]);

        return [];
    }

    public function mount(Request $request)
    {
        $this->startDate = $request->input('startDate', Carbon::now()->subDays(30)->startOfDay()->toDateString());
        $this->endDate = $request->input('endDate', Carbon::now()->endOfDay()->toDateString());
        $this->data = $this->getData();
    }

    protected function getFormSchema(): array
    {
        return [
            Grid::make(2)->schema([
                DatePicker::make('startDate'),
                DatePicker::make('endDate'),
            ]),
        ];
    }

    public function table(Table $table): Table
    {
        return $table
            ->query(
                UserSession::query()
                    ->select('entry_page as url', DB::raw('count(*) as total'))
                    ->whereBetween('entry_time', [$this->startDate, $this->endDate])
                    ->groupBy('entry_page')
            )
            ->columns([
                TextColumn::make('url')
                    ->label('URL')
                    ->searchable(query: function (Builder $query, string $search): Builder {
                        return $query->where('entry_page', 'like', "%{$search}%");
                    }),
                TextColumn::make('total')->label('Total Views')->sortable(),
            ])
            ->filters([
                Filter::make('url')
                    ->form([
                        TextInput::make('url')
                            ->label('Filter URL')
                            ->placeholder('Enter URL to filter'),
                    ])
                    ->query(function (Builder $query, array $data): Builder {
                        return $query
                            ->when(
                                $data['url'],
                                fn (Builder $query, string $url): Builder => $query->where('entry_page', 'like', "%{$url}%")
                            );
                    })
            ])
            ->defaultPaginationPageOption(10)
            ->defaultSort('total', 'desc')
            ->recordUrl(null);
    }

    public function getTableRecordKey($record): string
    {
        return base64_encode($record->url);
    }

    public function getData()
    {
        $startDate = Carbon::parse($this->startDate)->startOfDay();
        $endDate = Carbon::parse($this->endDate)->endOfDay();

        $totalVisitors = UserSession::whereBetween('entry_time', [$startDate, $endDate])
            ->distinct('tracking_id')
            ->count('tracking_id');

        $pageViews = UserSession::whereBetween('entry_time', [$startDate, $endDate])
            ->count();

        $dailyVisitorData = $this->getDailyVisitorData($startDate, $endDate);
        $topCities = $this->getTopData('city', 'country', 10);
        $topCountries = $this->getTopData('country', null, 10);
        $topReferrers = $this->getTopReferrers(10);

        return [
            'totalVisitors' => $totalVisitors,
            'pageViews' => $pageViews,
            'dailyVisitorData' => $dailyVisitorData,
            'topCities' => $topCities,
            'topCountries' => $topCountries,
            'topReferrers' => $topReferrers,
        ];
    }

    private function getDailyVisitorData($startDate, $endDate)
    {
        return UserSession::select(DB::raw('DATE(entry_time) as date'), DB::raw('COUNT(DISTINCT tracking_id) as visitors'))
            ->whereBetween('entry_time', [$startDate, $endDate])
            ->groupBy(DB::raw('DATE(entry_time)'))
            ->orderBy('date')
            ->get();
    }

    private function getTopData($column, $additionalColumn = null, $limit = 10)
    {
        $query = UserSession::select($column, DB::raw('count(*) as total'))
            ->whereBetween('entry_time', [$this->startDate, $this->endDate])
            ->whereNotNull($column)
            ->groupBy($column)
            ->orderByDesc('total')
            ->limit($limit);

        if ($additionalColumn) {
            $query->addSelect($additionalColumn);
            $query->groupBy($additionalColumn);
        }

        return $query->get();
    }

    private function getTopReferrers($limit = 10)
    {
        return UserSession::select('referrer', DB::raw('count(*) as total'))
            ->whereNotNull('referrer')
            ->whereBetween('entry_time', [$this->startDate, $this->endDate])
            ->where(function ($query) {
                $query->where('referrer', 'not like', '%' . request()->getHost() . '%');
            })
            ->groupBy('referrer')
            ->orderByDesc('total')
            ->limit($limit)
            ->get();
    }
}