<?php

namespace App;

use App\Models\AmazonProduct;
use Illuminate\Database\Eloquent\Model;

class AffiliateLink extends Model
{
    protected $fillable = [
        'amazon_product_id',
        'url',
        'ean',
        'asin',
        'locale',
        'country',
        'currency',
        'price',
    ];

    public function amazonProduct()
    {
        return $this->belongsTo(AmazonProduct::class);
    }
}

