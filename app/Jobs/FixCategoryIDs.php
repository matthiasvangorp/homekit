<?php

namespace App\Jobs;

use App\Models\AmazonProduct;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class FixCategoryIDs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //first delete all empty products
        $emptyProducts = AmazonProduct::whereNull('title')->get();
        foreach ($emptyProducts as $emptyProduct){
            $emptyProduct->delete();
        }

        $productsWithoutCategory = AmazonProduct::whereNull('category_id')->get();
        foreach ($productsWithoutCategory as $productWithoutCategory) {
            Log::info("Looking for german product with asin " . $productWithoutCategory->asin);
            $goodProduct = AmazonProduct::where('locale', 'DE')->where('asin', $productWithoutCategory->asin)->first();
            $productWithoutCategory->category_id = $goodProduct->category_id;
            $productWithoutCategory->save();
        }

        $amazonProducts = AmazonProduct::all();
        foreach ($amazonProducts as $amazonProduct){
            $score = rand(40000, 50000) / 10000;
            if ($score < 4.5) {
                $score = 4;
            }
            else {
                $score = 5;
            }
            $amazonProduct->score = $score;
            $amazonProduct->save();
        }

        //set products to unpublished
        $germanUnpublishedProducts = AmazonProduct::where('locale', 'DE')
            ->where('publish', false)
            ->get();
        foreach ($germanUnpublishedProducts as $germanUnpublishedProduct) {
            //Find other products with the same asin but a different locale
            $badProducts = AmazonProduct::where('locale', '<>', 'DE')
                ->where('asin', $germanUnpublishedProduct->asin)
                ->get();
            foreach ($badProducts as $badProduct){
                $badProduct->publish = false;
                $badProduct->save();
            }

        }
    }
}
