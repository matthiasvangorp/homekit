<?php

namespace App\Jobs;

use Amazon\ProductAdvertisingAPI\v1\ApiException;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\api\DefaultApi;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\GetItemsRequest;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\GetItemsResource;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\PartnerType;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\ProductAdvertisingAPIClientException;
use Amazon\ProductAdvertisingAPI\v1\Configuration;
use App\Models\AmazonProduct;
use App\Models\AmazonProductPrice;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class Product implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $germanAmazonProduct;
    private $locale;
    private $amazonLocale;
    private $countryCode;
    private $host;
    private $partnerTag;
    private $region;
    private $asins = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $amazonProducts, string $locale)
    {
        $this->germanAmazonProducts = $amazonProducts;


        foreach ($this->germanAmazonProducts as $germanAmazonProduct){
            array_push($this->asins, $germanAmazonProduct['asin']);
        }
        $this->locale = $locale;
        $this->amazonLocale = 'en_GB';
        if ($this->locale === 'NL'){
            $this->amazonLocale = 'nl_NL';
            $this->host = env('AMAZON_HOST_DE');
            $this->partnerTag = env('AWS_ASSOCIATE_TAG');
            $this->countryCode = 'DE';
            $this->region = env('AMAZON_REGION_EU');
        }
        if ($this->locale === 'DE'){
            $this->amazonLocale = 'de_DE';
            $this->host = env('AMAZON_HOST_DE');
            $this->partnerTag = env('AWS_ASSOCIATE_TAG');
            $this->countryCode = 'DE';
        }
        if ($this->locale === 'EN'){
            $this->amazonLocale = 'en_GB';
            $this->host = env('AMAZON_HOST_DE');
            $this->partnerTag = env('AWS_ASSOCIATE_TAG');
            $this->countryCode = 'DE';
            $this->region = env('AMAZON_REGION_EU');
        }

        if ($this->locale === 'FR'){
            $this->amazonLocale = 'fr_FR';
            $this->host = env('AMAZON_HOST_FR');
            $this->partnerTag = env('AWS_ASSOCIATE_TAG_FR');
            $this->countryCode = 'FR';
            $this->region = env('AMAZON_REGION_EU');
        }
        if ($this->locale === 'ES'){
            $this->amazonLocale = 'es_ES';
            $this->host = env('AMAZON_HOST_ES');
            $this->partnerTag = env('AWS_ASSOCIATE_TAG_ES');
            $this->countryCode = 'ES';
            $this->region = env('AMAZON_REGION_EU');
        }
        if ($this->locale === 'US'){
            $this->amazonLocale = 'en_US';
            $this->host = env('AMAZON_HOST_US');
            $this->partnerTag = env('AWS_ASSOCIATE_TAG_US');
            $this->countryCode = 'US';
            $this->region = env('AMAZON_REGION_US');
        }

        if ($this->locale === 'IT'){
            $this->amazonLocale = 'it_IT';
            $this->host = env('AMAZON_HOST_IT');
            $this->partnerTag = env('AWS_ASSOCIATE_TAG_IT');
            $this->countryCode = 'IT';
            $this->region = env('AMAZON_REGION_EU');
        }

        if ($this->locale === 'UK'){
            $this->amazonLocale = 'en_GB';
            $this->host = env('AMAZON_HOST_UK');
            $this->partnerTag = env('AWS_ASSOCIATE_TAG_UK');
            $this->countryCode = 'UK';
            $this->region = env('AMAZON_REGION_EU');
        }



    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $config = new Configuration();

        /*
         * Add your credentials
         */
        # Please add your access key here
        $config->setAccessKey(env('AWS_ACCESS_KEY_ID'));
        # Please add your secret key here
        $config->setSecretKey(env('AWS_SECRET_ACCESS_KEY'));

        # Please add your partner tag (store/tracking id) here
        $partnerTag = $this->partnerTag;

        /*
         * PAAPI host and region to which you want to send request
         * For more details refer:
         * https://webservices.amazon.com/paapi5/documentation/common-request-parameters.html#host-and-region
         */
        $config->setHost($this->host);
        $config->setRegion($this->region);

        $apiInstance = new DefaultApi(
        /*
         * If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
         * This is optional, `GuzzleHttp\Client` will be used as default.
         */
            new Client(),
            $config
        );

        # Request initialization


        /*
         * Specify the category in which search request is to be made
         * For more details, refer:
         * https://webservices.amazon.com/paapi5/documentation/use-cases/organization-of-items-on-amazon/search-index.html
         */
        $searchIndex = "All";

        # Specify item count to be returned in search result
        $itemCount = 10;

        /*
         * Choose resources you want from SearchItemsResource enum
         * For more details,
         * refer: https://webservices.amazon.com/paapi5/documentation/get-items.html
         */
        $resources = [
            GetItemsResource::ITEM_INFOTITLE,
            GetItemsResource::OFFERSLISTINGSPRICE,
            GetItemsResource::ITEM_INFOFEATURES,
            GetItemsResource::ITEM_INFOCONTENT_INFO,
            GetItemsResource::IMAGESPRIMARYLARGE,
            GetItemsResource::ITEM_INFOPRODUCT_INFO,
            GetItemsResource::ITEM_INFOCONTENT_RATING,];

        # Forming the request
        $getItemsRequest = new GetItemsRequest();



        $getItemsRequest->setPartnerTag($partnerTag);
        $getItemsRequest->setPartnerType(PartnerType::ASSOCIATES);
        $getItemsRequest->setItemIds($this->asins);
        $getItemsRequest->setLanguagesOfPreference([$this->amazonLocale]);
        $getItemsRequest->setResources($resources);

        # Validating request
        $invalidPropertyList = $getItemsRequest->listInvalidProperties();
        $length = count($invalidPropertyList);
        if ($length > 0) {
            Log::info("Error forming the request");
            foreach ($invalidPropertyList as $invalidProperty) {
                Log::error($invalidProperty);
            }
            return;
        }

        # Sending the request
        try {
            Log::info("API call for ". $this->locale."  product with ASIN ");
            $getItemsResponse = $apiInstance->getItems($getItemsRequest);




            # Parsing the response
            if ($getItemsResponse->getItemsResult() !== null) {

                $items = $getItemsResponse->getItemsResult()->getItems();


                foreach ($items as $item) {
                    $homekitFound = false;
                    if ($item !== null) {
                        if ($item->getASIN() !== null) {
                            Log::info($item->getASIN());
                            $amazonProduct = AmazonProduct::firstOrCreate(['asin' => $item->getASIN(), 'locale' => $this->locale]);
                            if(is_null($amazonProduct->url)){
                                Log::info("Creating new product with ASIN ". $item->getASIN());
                            }
                        }

                        if ($item->getDetailPageURL() !== null) {
                            Log::info($item->getDetailPageURL());
                            $amazonProduct->url = $item->getDetailPageURL();
                        }
                        if ($item->getItemInfo() !== null
                            and $item->getItemInfo()->getTitle() !== null
                            and $item->getItemInfo()->getTitle()->getDisplayValue() !== null) {
                            Log::info($item->getItemInfo()->getTitle()->getDisplayValue());
                            $amazonProduct->title = $item->getItemInfo()->getTitle()->getDisplayValue();

                            if (strpos(strtolower($item->getItemInfo()->getTitle()->getDisplayValue()), "homekit") !== false) {
                                $homekitFound = true;
                            }
                        }

                        if ($item->getItemInfo() !== null
                            and $item->getItemInfo()->getFeatures() !== null
                        ) {

                            $displayValues = $item->getItemInfo()->getFeatures()->getDisplayValues();
                            $displayValuesString = "<ul>";
                            foreach ($displayValues as $displayValue){
                                $displayValuesString .= "<li>".$displayValue."</li>";
                            }
                            $displayValuesString .= "</ul>";
                            $amazonProduct->description = $displayValuesString;
                            if (strpos(strtolower($displayValuesString), "homekit") !== false) {
                                $homekitFound = true;
                            }
                        }
                        if ($item->getImages() !== null && $item->getImages()->getPrimary()->getLarge()->getURL() !== null) {
                            Log::info($item->getImages()->getPrimary()->getLarge()->getURL());
                            $amazonProduct->image = $item->getImages()->getPrimary()->getLarge()->getURL();
                        }
                        if ($item->getOffers() !== null
                            and $item->getOffers() !== null
                            and $item->getOffers()->getListings() !== null
                            and $item->getOffers()->getListings()[0]->getPrice() !== null
                            and $item->getOffers()->getListings()[0]->getPrice()->getDisplayAmount() !== null) {
                            Log::info($item->getOffers()->getListings()[0]->getPrice()->getDisplayAmount());
                            $amazonProduct->price = $item->getOffers()->getListings()[0]->getPrice()->getAmount();
                        }
                        if ($item->getItemInfo() !== null
                            and $item->getItemInfo()->getContentRating() !== null
                            and $item->getItemInfo()->getContentRating()->getModelName() !== null) {
                            Log::info($item->getItemInfo()->getContentRating()->getModelName());
                        }

                        //get the brandId from the german product
                        $tmpProduct = AmazonProduct::where('asin', $item->getASIN())->where('locale', 'DE')->first();
                        if($tmpProduct) {
                            $amazonProduct->brand_id = $tmpProduct->brand_id;
                            Log::info("Assigning brand_id " . $tmpProduct->brand_id ." to product ". $amazonProduct->id  );
                        }
                        {
                            Log::error("German product not found");
                        }

                        if ($homekitFound) {
                            $amazonProduct->country = $this->countryCode;
                            $amazonProduct->locale = $this->locale;
                            $amazonProduct->save();

                            $amazonProductPrice = new AmazonProductPrice();
                            $amazonProductPrice->price = $amazonProduct->price;
                            $amazonProductPrice->amazon_product_id = $amazonProduct->id;
                            $amazonProductPrice->save();

                            // Check if there is an existing entry for this amazon_product_id today
                            $existingPrice = AmazonProductPrice::where('amazon_product_id', $amazonProduct->id)
                                ->whereDate('created_at', today())
                                ->first();
                            if (!$existingPrice) {
                                // No existing entry for today, create a new one
                                $amazonProductPrice->save();
                            }
                        }
                        else {
                            $amazonProduct->delete();
                        }
                    }
                }

            }
            if ($getItemsResponse->getErrors() !== null) {
                Log::error('Printing Errors: Printing first error object from list of errors');
                Log::error('Error code: ' . $getItemsResponse->getErrors()[0]->getCode());
                Log::error('Error message: '. $getItemsResponse->getErrors()[0]->getMessage());
            }
        } catch (ApiException $exception) {
            Log::error("Error calling PA-API 5.0!");
            Log::error("HTTP Status Code: ". $exception->getCode());
            Log::error("Error Message: ". $exception->getMessage());
            if ($exception->getResponseObject() instanceof ProductAdvertisingAPIClientException) {
                $errors = $exception->getResponseObject()->getErrors();
                foreach ($errors as $error) {
                    Log::error("Error Type: ". $error->getCode());
                    Log::error("Error Message: ". $error->getMessage());
                }
            } else {
                Log::error("Error response body: ". $exception->getResponseBody());
            }
        } catch (Exception $exception) {
            Log::error("Error Message: ". $exception->getMessage());
        }
    }
}
