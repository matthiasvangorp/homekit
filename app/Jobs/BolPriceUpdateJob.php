<?php

namespace App\Jobs;

use App\AffiliateLink;
use App\Repositories\Link\AffiliateLinkRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class BolPriceUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $link;

    protected $affiliateLinks;

    /**
     * Create a new job instance.
     *
     * @param  string                   $url
     * @param  AffiliateLink            $link
     * @param  AffiliateLinkRepository  $affiliateLinkRepository
     */
    public function __construct(AffiliateLink $link, AffiliateLinkRepository $affiliateLinkRepository)
    {
        $this->link = $link;
        $this->affiliateLinks = $affiliateLinkRepository;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        try {
            $response = $client->get('https://api.bol.com/catalog/v4/products/' . $this->link->external_id . '?apikey=' . config('bol-api.api_key') . '&offers=cheapest&includeAttributes=false&format=json');
            $body     = json_decode($response->getBody());
            $price    = 0;
            foreach ($body->products as $product) {
                if (property_exists($product->offerData, 'offers')) {
                    $offers = $product->offerData->offers;
                    foreach ($offers as $offer) {
                        if (property_exists($offer, 'bestOffer') && $offer->bestOffer) {
                            $price = $offer->price;
                        }
                    }
                }
            }
            if ($price > 0) {
                $this->affiliateLinks->saveAffiliateLinkPrice($this->link->id, $price);
                Log::info('updated price for BOL affiliate link with id : ' . $this->link->id);
            }
        }
        catch (ClientException $e){
            Log::warning("ClientException for link with id : ". $this->link->id);
            Log::warning($e->getMessage());
        }
    }
}
