<?php

namespace App\Jobs;

use App\Models\AmazonProduct;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Products implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $locale;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $locale)
    {
        $this->locale = $locale;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $amazonProducts = AmazonProduct::where('locale', 'DE')->get();
        $counter = 1;
        $productChunks = array_chunk($amazonProducts->toArray(), 10); // split into arrays of 10 products each
        foreach ($productChunks as $chunk) {
            $products = collect($chunk);
            Product::dispatch($products, $this->locale)->delay(now()->addMinutes($counter));
            $counter++;
        }

    }
}
