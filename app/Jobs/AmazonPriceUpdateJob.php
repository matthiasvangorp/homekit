<?php

namespace App\Jobs;

use App\AffiliateLink;
use App\Repositories\Link\AffiliateLinkRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class AmazonPriceUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $link;

    protected $affiliateLinks;

    /**
     * Create a new job instance.
     *
     * @param  AffiliateLink            $link
     * @param  AffiliateLinkRepository  $affiliateLinks
     */
    public function __construct(AffiliateLink $link, AffiliateLinkRepository $affiliateLinks)
    {
        $this->link = $link;
        $this->affiliateLinks = $affiliateLinks;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $host = config('amazon-product.host_de');
        $associateTag =  config('amazon-product.associate_tag_de');
        $apiKey = config('amazon-product.api_key_de');
        $apiSecretKey = config('amazon-product.api_secret_key_de');
        $config = [
            'api_key'        => $apiKey,
            'api_secret_key' => $apiSecretKey,
            'associate_tag'  => $associateTag,
            'host'           => $host,
            'region'         => config('amazon-product.region')
        ];
        //currently only works for german store
        $response = AmazonProduct::item($this->link->external_id);
        //save the price
        if (isset($response['ItemsResult']['Items'][0]['Offers'])) {
            $this->affiliateLinks->saveAffiliateLinkPrice($this->link->id,
                $response['ItemsResult']['Items'][0]['Offers']['Listings'][0]['Price']['Amount']);
            Log::info('updated price for amazon affiliate link with id : ' . $this->link->id);
        }
    }
}
