<?php

namespace App\Jobs;

use App\Models\Brand;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SpanishProductsByBrands implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $brands = Brand::all();
        $counter = 0;
        Log::info('Starting spanish products by brand job');
        foreach ($brands as $brand){
            Log::info('Delay timestamp: ' . now()->addMinutes($counter)->timestamp);
            SpanishProductsByBrand::dispatch($brand)->delay(now()->addMinutes($counter));
            $counter++;
        }
    }
}
