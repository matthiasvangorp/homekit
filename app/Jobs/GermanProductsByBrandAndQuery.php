<?php

namespace App\Jobs;

use Amazon\ProductAdvertisingAPI\v1\ApiException;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\api\DefaultApi;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\PartnerType;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\ProductAdvertisingAPIClientException;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\SearchItemsRequest;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\SearchItemsResource;
use Amazon\ProductAdvertisingAPI\v1\Configuration;
use App\Models\AmazonProduct;
use App\Models\Brand;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class GermanProductsByBrandAndQuery implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $brand;

    protected $query;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Brand $brand, $query)
    {
        $this->brand = $brand;
        $this->query = $query;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $config = new Configuration();

        Log::info('starting job for brand ' . $this->brand->name . " and query " . $this->query);

        /*
         * Add your credentials
         */
        # Please add your access key here
        $config->setAccessKey(env('AWS_ACCESS_KEY_ID'));
        # Please add your secret key here
        $config->setSecretKey(env('AWS_SECRET_ACCESS_KEY'));

        # Please add your partner tag (store/tracking id) here
        $partnerTag = env('AWS_ASSOCIATE_TAG');
        Log::info('Partnertag '. $partnerTag);

        /*
         * PAAPI host and region to which you want to send request
         * For more details refer:
         * https://webservices.amazon.com/paapi5/documentation/common-request-parameters.html#host-and-region
         */
        $config->setHost(env('AMAZON_HOST_DE'));
        $config->setRegion(env('AMAZON_REGION_EU'));

        $apiInstance = new DefaultApi(
        /*
         * If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
         * This is optional, `GuzzleHttp\Client` will be used as default.
         */
            new Client(),
            $config
        );

        # Request initialization

        # Specify keywords
        $keyword = $this->query;

        /*
         * Specify the category in which search request is to be made
         * For more details, refer:
         * https://webservices.amazon.com/paapi5/documentation/use-cases/organization-of-items-on-amazon/search-index.html
         */
        $searchIndex = "All";

        # Specify item count to be returned in search result
        $itemCount = 50;

        /*
         * Choose resources you want from SearchItemsResource enum
         * For more details,
         * refer: https://webservices.amazon.com/paapi5/documentation/search-items.html#resources-parameter
         */
        $resources = [
            SearchItemsResource::ITEM_INFOTITLE,
            SearchItemsResource::OFFERSLISTINGSPRICE,
            SearchItemsResource::ITEM_INFOFEATURES,
            SearchItemsResource::ITEM_INFOCONTENT_INFO,
            SearchItemsResource::IMAGESPRIMARYLARGE,
            SearchItemsResource::ITEM_INFOPRODUCT_INFO,
            SearchItemsResource::ITEM_INFOCONTENT_RATING,];

        # Forming the request
        $searchItemsRequest = new SearchItemsRequest();
        $searchItemsRequest->setSearchIndex($searchIndex);
        $searchItemsRequest->setKeywords($keyword);
        $searchItemsRequest->setItemCount($itemCount);
        $searchItemsRequest->setPartnerTag($partnerTag);
        $searchItemsRequest->setPartnerType(PartnerType::ASSOCIATES);
        $searchItemsRequest->setBrand($this->brand->name);
        $searchItemsRequest->setResources($resources);

        # Validating request
        $invalidPropertyList = $searchItemsRequest->listInvalidProperties();
        $length = count($invalidPropertyList);
        if ($length > 0) {
            Log::error("Error forming the request");
            foreach ($invalidPropertyList as $invalidProperty) {
                Log::error($invalidProperty);
            }
            return;
        }

        # Sending the request
        try {
            $searchItemsResponse = $apiInstance->searchItems($searchItemsRequest);

            //echo 'API called successfully', PHP_EOL;
            //echo 'Complete Response: ', $searchItemsResponse, PHP_EOL;

            # Parsing the response
            if ($searchItemsResponse->getSearchResult() !== null) {
                //echo 'Printing first item information in SearchResult:', PHP_EOL;
                $items = $searchItemsResponse->getSearchResult()->getItems();

                foreach ($items as $item) {
                    $homekitFound = false;
                    if ($item !== null) {
                        if ($item->getASIN() !== null) {
                            Log::info($item->getASIN());
                            $amazonProduct = AmazonProduct::firstOrCreate(['asin' => $item->getASIN()]);
                        }
                        if ($item->getDetailPageURL() !== null) {
                            Log::info($item->getDetailPageURL());
                            $amazonProduct->url = $item->getDetailPageURL();
                        }


                        if ($item->getItemInfo() !== null
                            and $item->getItemInfo()->getTitle() !== null
                            and $item->getItemInfo()->getTitle()->getDisplayValue() !== null) {
                            Log::info($item->getItemInfo()->getTitle()->getDisplayValue());
                            $amazonProduct->title = $item->getItemInfo()->getTitle()->getDisplayValue();

                            if (strpos(strtolower($item->getItemInfo()->getTitle()->getDisplayValue()), "homekit") !== false) {
                                $homekitFound = true;
                            }
                        }



                        if ($item->getItemInfo() !== null
                            and $item->getItemInfo()->getFeatures() !== null
                        ) {

                            $displayValues = $item->getItemInfo()->getFeatures()->getDisplayValues();
                            $displayValuesString = "<ul>";
                            foreach ($displayValues as $displayValue){
                                $displayValuesString .= "<li>".$displayValue."</li>";
                            }
                            $displayValuesString .= "</ul>";
                            $amazonProduct->description = $displayValuesString;
                            if (strpos(strtolower($displayValuesString), "homekit") !== false) {
                                $homekitFound = true;
                            }
                            Log::info($displayValuesString);
                        }
                        if ($item->getImages() !== null && $item->getImages()->getPrimary()->getLarge()->getURL() !== null) {
                            Log::info($item->getImages()->getPrimary()->getLarge()->getURL());
                            $amazonProduct->image = $item->getImages()->getPrimary()->getLarge()->getURL();

                        }
                        if ($item->getOffers() !== null
                            and $item->getOffers() !== null
                            and $item->getOffers()->getListings() !== null
                            and $item->getOffers()->getListings()[0]->getPrice() !== null
                            and $item->getOffers()->getListings()[0]->getPrice()->getDisplayAmount() !== null) {
                            Log::info($item->getOffers()->getListings()[0]->getPrice()->getDisplayAmount());
                            $amazonProduct->price = $item->getOffers()->getListings()[0]->getPrice()->getAmount();
                        }

                        $amazonProduct->brand_id = $this->brand->id;
                        if (strtolower($keyword === 'hue')){
                            $homekitFound = true;
                        }
                        if ($homekitFound) {
                            $amazonProduct->locale = 'DE';
                            $amazonProduct->save();
                        }
                        else {
                            $amazonProduct->delete();
                        }
                    }
                }

            }
            if ($searchItemsResponse->getErrors() !== null) {
                Log::error( 'Printing Errors: Printing first error object from list of errors');
                Log::error('Error code: '. $searchItemsResponse->getErrors()[0]->getCode());
                Log::error('Error message: '. $searchItemsResponse->getErrors()[0]->getMessage());
            }
        } catch (ApiException $exception) {
            Log::error("Error calling PA-API 5.0!");
            Log::error("HTTP Status Code: ".  $exception->getCode());
            Log::error("Error Message: ".  $exception->getMessage());
            if ($exception->getResponseObject() instanceof ProductAdvertisingAPIClientException) {
                $errors = $exception->getResponseObject()->getErrors();
                foreach ($errors as $error) {
                    Log::error("Error Type: ".  $error->getCode());
                    Log::error("Error Message: ".  $error->getMessage());
                }
            } else {
                Log::error("Error response body: ".  $exception->getResponseBody());
            }
        } catch (Exception $exception) {
            Log::error("Error Message: ".  $exception->getMessage());
        }
    }
}
