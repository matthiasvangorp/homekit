<?php

namespace App\Jobs;

use App\Models\AmazonProduct;
use App\Models\Brand;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\Log;

class CrawlAmazonProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $url;
    protected $store;

    public function __construct($url, $store)
    {
        $this->url = $url;
        $this->store = $store;
    }

    public function handle()
    {
        Log::info("Starting to crawl URL: " . $this->url);

        $client = new Client();
        try {
            $response = $client->get($this->url, [
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
                ]
            ]);
            $html = $response->getBody()->getContents();
        } catch (\Exception $e) {
            Log::error("Failed to fetch URL: " . $e->getMessage());
            return;
        }

        $crawler = new Crawler($html);

        $asin = $this->extractContent($crawler, '#productDetails_detailBullets_sections1 .prodDetAttrValue', 'ASIN');
        if (!$asin) {
            $asin = $this->extractAsinFromUrl($this->url);
        }        $image = $this->extractAttribute($crawler, '.imgTagWrapper img', 'src', 'Image URL');
        $title = $this->extractContent($crawler, '#productTitle', 'Title');
        $price = $this->extractPrice($crawler);
        $description = $this->extractDescription($crawler);
        $brandId = $this->detectBrand($title, $description);


        /*if (!$asin || !$image || !$title || !$price) {
            Log::error("Failed to extract all required fields from the page.");
            return;
        }*/

        if (!$asin) {
            Log::error("Failed to extract asin from the page.");
            return;
        }

        if (!$image) {
            Log::error("Failed to extract image from the page.");
            return;
        }

        if (!$title) {
            Log::error("Failed to extract title from the page.");
            return;
        }

        if (!$price) {
            Log::error("Failed to extract price from the page.");
            return;
        }

        $locale = 'EN';

        if ($this->store === 'UK'){
            $locale = 'EN';
        }

        if ($this->store === 'US'){
            $locale = 'EN';
        }

        if ($this->store === 'IT'){
            $locale = 'IT';
        }

        // Check if the product already exists
        $existingProduct = AmazonProduct::where('asin', $asin)
            ->where('country', $this->store)
            ->first();

        if ($existingProduct) {
            Log::info("Product already exists for ASIN: $asin in store: {$this->store}");
            return;
        }

        try {
            AmazonProduct::create([
                'asin' => trim($asin),
                'image' => $image,
                'title' => $title,
                'price' => $price,
                'description' => $description,
                'url' => $this->url,
                'locale' => $locale,
                'country' => $this->store,  // You might want to map this differently
                'publish' => true,  // Set default publish status
                'brand_id' => $brandId, // Add this line
            ]);
            Log::info("Successfully created new AmazonProduct with ASIN: $asin");
        } catch (\Exception $e) {
            Log::error("Failed to create AmazonProduct: " . $e->getMessage());
        }
    }

    private function extractContent(Crawler $crawler, $selector, $fieldName)
    {
        try {
            $element = $crawler->filter($selector);
            if ($element->count() > 0) {
                $content = trim($element->text());
                Log::info("Extracted $fieldName: " . substr($content, 0, 50) . "...");
                return $content;
            }
        } catch (\Exception $e) {
            Log::warning("Failed to extract $fieldName: " . $e->getMessage());
        }
        return null;
    }

    private function extractAttribute(Crawler $crawler, $selector, $attribute, $fieldName)
    {
        try {
            $element = $crawler->filter($selector);
            if ($element->count() > 0) {
                $content = $element->attr($attribute);

                // Modify image URL if it's an Amazon image
                if ($fieldName === 'Image URL' && strpos($content, 'amazon.com/images/') !== false) {
                    // Extract the image ID and append _SL500_.jpg
                    if (preg_match('/I\/([A-Za-z0-9]+)\./', $content, $matches)) {
                        $imageId = $matches[1];
                        $content = "https://m.media-amazon.com/images/I/{$imageId}._SL500_.jpg";
                    }
                }

                Log::info("Extracted $fieldName: " . substr($content, 0, 50) . "...");
                return $content;
            }
        } catch (\Exception $e) {
            Log::warning("Failed to extract $fieldName: " . $e->getMessage());
        }
        return null;
    }

    private function extractPrice(Crawler $crawler)
    {
        try {
            $wholePart = $crawler->filter('.a-price-whole')->text();
            $wholePart = preg_replace('/[^0-9]/', '', $wholePart);
            Log::info("Whole (cleaned): " . $wholePart);

            $fractionPart = $crawler->filter('.a-price-fraction')->text();
            $fractionPart = preg_replace('/[^0-9]/', '', $fractionPart);
            Log::info("Fraction Part (cleaned): " . $fractionPart);

            // Ensure fraction part is always two digits
            $fractionPart = str_pad($fractionPart, 2, '0', STR_PAD_RIGHT);
            $fractionPart = substr($fractionPart, 0, 2);

            $price = $wholePart . '.' . $fractionPart;
            Log::info("Extracted Price: " . $price);

            return floatval($price);
        } catch (\Exception $e) {
            Log::warning("Failed to extract Price: " . $e->getMessage());
        }
        return null;
    }

    private function extractDescription(Crawler $crawler)
    {
        try {
            $element = $crawler->filter('.a-unordered-list.a-vertical.a-spacing-mini');
            if ($element->count() > 0) {
                $html = $element->html();

                // Remove class attributes
                $cleanHtml = preg_replace('/\s*class=".*?"/i', '', $html);

                // Remove empty attributes
                $cleanHtml = preg_replace('/\s*\w+=""/i', '', $cleanHtml);

                // Remove extra whitespace
                $cleanHtml = preg_replace('/\s+/', ' ', $cleanHtml);

                Log::info("Extracted Description: " . substr($cleanHtml, 0, 50) . "...");
                $cleanHtml = "<ul>".$cleanHtml."</ul>";
                return trim($cleanHtml);
            }
        } catch (\Exception $e) {
            Log::warning("Failed to extract Description: " . $e->getMessage());
        }
        return null;
    }

    private function extractAsinFromUrl($url)
    {
        $pattern = '/\/dp\/([A-Z0-9]{10})/';
        if (preg_match($pattern, $url, $matches)) {
            $asin = $matches[1];
            Log::info("Extracted ASIN from URL: $asin");
            return $asin;
        }
        Log::warning("Failed to extract ASIN from URL");
        return null;
    }


    private function detectBrand($title, $description)
    {
        // Get all brands from the database, excluding Apple
        $brands = Brand::where('name', '!=', 'Apple')->pluck('name')->toArray();

        // Combine title and description
        $text = $title . ' ' . $description;

        foreach ($brands as $brand) {
            // Use word boundary regex for whole word matching
            $pattern = '/\b' . preg_quote($brand, '/') . '\b/i';

            if (preg_match($pattern, $text)) {
                // Found a match
                $brandModel = Brand::where('name', $brand)->first();
                Log::info("Detected brand: " . $brandModel->name);
                return $brandModel->id;
            }
        }

        Log::info("No brand detected");
        return null;
    }
}