<?php

namespace App\Repositories\Article;

use App\Article;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/**
 * Class ArticleRepository.
 */
class ArticleRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Article::class;

    private $paginationType = 'simplePaginate';

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */

    /**
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function render($limit = null, $paginate = true, $pagination = 10)
    {
        $articles = Article::orderBy('created_at', 'desc')->where('locale', LaravelLocalization::getCurrentLocale())->get();

        return $articles;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function renderOne($id)
    {
        $article = Article::find($id);

        return $article;
    }

    /**
     * @param $query
     * @param $limit
     * @param $paginate
     * @param $pagination
     *
     * @return mixed
     */
    public function buildPagination($query, $limit, $paginate, $pagination)
    {
        if ($paginate && is_numeric($pagination)) {
            return $query->{$this->paginationType}($pagination);
        } else {
            if ($limit && is_numeric($limit)) {
                $query->take($limit);
            }

            return $query->get();
        }
    }

    /**
     * @param array $input
     */
    public function create($input)
    {
        $article = new Article();
        $article->title = $input['title'];
        $article->subtitle = $input['subtitle'];
        $article->article = $input['article'];
        $article->locale = LaravelLocalization::getCurrentLocale();

        $article->save();
    }

    /**
     * @param array $input
     * @param $id
     */
    public function update($input, $id)
    {
        $article = Article::find($id);
        $article->title = $input['title'];
        $article->subtitle = $input['subtitle'];
        $article->article = $input['article'];
        $article->locale = LaravelLocalization::getCurrentLocale();

        $article->save();
    }
}
