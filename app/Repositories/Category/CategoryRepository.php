<?php

namespace App\Repositories\Category;

use App\Exceptions\GeneralException;
use App\Models\Category;
use App\Repositories\Product\ProductRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class CategoryRepository.
 */
class CategoryRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Category::class;

    private $paginationType = 'simplePaginate';

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */

    /**
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function render($limit = null, $paginate = true, $pagination = 10)
    {
        $categories = Category::orderBy('name', 'asc')->get();

        return $categories;
    }

    public function renderWithProducts()
    {
        $categories = Category::orderBy('name', 'asc')->get();
        $productRepository = new ProductRepository();
        foreach ($categories as $key => $category) {
            $test = $category->id;
            $products = $productRepository->renderByCategoryId(null, true, 10, $category->id);
            if ($products->count() == 0) {
                //remove the category
                $categories->forget($key);
            }
        }

        return $categories;
    }

    public function renderOne($id)
    {
        $product = Category::find($id);
        $product->average_rating = $this->calculateAverageRating($product);
        $product = $product->translation(App::getLocale());

        return $product;
    }

    /**
     * @param $query
     * @param $limit
     * @param $paginate
     * @param $pagination
     *
     * @return mixed
     */
    public function buildPagination($query, $limit, $paginate, $pagination)
    {
        if ($paginate && is_numeric($pagination)) {
            return $query->{$this->paginationType}($pagination);
        } else {
            if ($limit && is_numeric($limit)) {
                $query->take($limit);
            }

            return $query->get();
        }
    }

    /**
     * @param array $input
     */
    public function create($input)
    {
        $data = $input['data'];
        $images = $input['images'];

        $truck = $this->createCategoryStub($data);

        DB::transaction(function () use ($truck, $data, $images) {
            if ($truck->save()) {
                //truck saved, now save the images
                $imageString = '';
                foreach ($images as $image) {
                    if ($image->isValid()) {
                        $imageDirectory = public_path().'/images/trucks/'.$truck->id.'/';
                        $extension = $image->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111, 99999).'.'.$extension; // renameing image
            $image->move($imageDirectory, $fileName);
                        $imageString .= $fileName.';';
                    }
                }

                $imageString = substr($imageString, 0, -1); //remove the last ;

                $year = date_create_from_format('d/m/Y', $data['year']);

                $truck->images = $imageString;
                $truck->engine = $data['engine'];
                $truck->color = $data['color'];
                $truck->year = $year->format('Y-m-d');
                $truck->mileage = $data['mileage'];
                $truck->construction = $data['construction'];
                $truck->dimensions = $data['dimensions'];
                $truck->tailgate = $data['tailgate'];
                $truck->mtm = $data['mtm'];
                $truck->empty_weight = $data['empty_weight'];
                $truck->horsepower = $data['horsepower'];
                $truck->transmission = $data['transmission'];
                $truck->drivetrain = $data['drivetrain'];
                $truck->used = $data['used'];

                $truck->save();

                $event = new \stdClass();
                $event->truck = $truck;
                $event->user = Auth::user();
                event(new CategoryCreated($event));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
        });
    }

    /**
     * @param array $input
     */
    public function update($input)
    {
        $truck = $input['truck'];
        $data = $input['data'];
        $images = $input['images'];

        DB::transaction(function () use ($truck, $data, $images) {
            $imageString = '';
            if (! empty($images)) {
                foreach ($images as $image) {
                    if ($image->isValid()) {
                        $imageDirectory = public_path().'/images/trucks/'.$truck->id.'/';
                        $extension = $image->getClientOriginalExtension(); // getting image extension
                        $fileName = rand(11111,
                    99999).'.'.$extension; // renameing image
                        $image->move($imageDirectory, $fileName);
                        $imageString .= $fileName.';';
                    }
                }
            }

            $imageString = substr($imageString, 0, -1); //remove the last ;

            $year = date_create_from_format('d/m/Y', $data['year']);

            if (! empty($images)) {
                $truck->images = $imageString;
            }

            $truck->truckbrand_id = $data['brand'];
            $truck->engine = $data['engine'];
            $truck->color = $data['color'];
            $truck->year = $year->format('Y-m-d');
            $truck->mileage = $data['mileage'];
            $truck->construction = $data['construction'];
            $truck->dimensions = $data['dimensions'];
            $truck->tailgate = $data['tailgate'];
            $truck->mtm = $data['mtm'];
            $truck->empty_weight = $data['empty_weight'];
            $truck->horsepower = $data['horsepower'];
            $truck->transmission = $data['transmission'];
            $truck->drivetrain = $data['drivetrain'];
            $truck->used = $data['used'];

            $truck->save();

            $event = new \stdClass();
            $event->truck = $truck;
            $event->user = Auth::user();
            event(new CategoryUpdated($event));

            return true;
        });
    }

    protected function calculateAverageRating($product)
    {
        $scores = [$product->score_bol, $product->score_coolblue, $product->score_amazon];
        $counter = 0;
        $totalScore = 0;
        foreach ($scores as $score) {
            if ($score > 0) {
                $totalScore += $score;
                $counter++;
            }
        }

        if ($counter > 0) {
            return round(($totalScore / $counter) / 2, 0);
        } else {
            return;
        }
    }
}
