<?php

namespace App\Repositories\Link;

use App\AffiliateLink;
use App\AffiliateLinkPrice;
use App\Link;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/**
 * Class LinkRepository.
 */
class AffiliateLinkRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = AffiliateLink::class;

    private $paginationType = 'simplePaginate';


    /**
     * @return mixed
     */
    public function getAmazonAffiliateLinks(){
        $links = AffiliateLink::whereNotNull('external_id')->where('store', 'amazon')->where('country', 'DE')->orderBy('updated_at', 'desc')->get();
        return $links;
    }


    public function getBolAffiliateLinks(){
        $links = AffiliateLink::whereNotNull('external_id')->where('store', 'bol')->orderBy('updated_at', 'desc')->get();
        return $links;
    }


    /**
     * @param  int    $affiliateLinkId
     * @param  float  $price
     */
    public function saveAffiliateLinkPrice(int $affiliateLinkId, float $price){
        $affiliateLinkPrice = new AffiliateLinkPrice();
        $affiliateLinkPrice->affiliate_link_id = $affiliateLinkId;
        $affiliateLinkPrice->price = $price;
        $affiliateLinkPrice->save();
    }

}
