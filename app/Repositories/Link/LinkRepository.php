<?php

namespace App\Repositories\Link;

use App\Link;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/**
 * Class LinkRepository.
 */
class LinkRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Link::class;

    private $paginationType = 'simplePaginate';

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */

    /**
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function render($limit = null, $paginate = true, $pagination = 10)
    {
        $links = Link::orderBy('created_at', 'desc')->where('locale', LaravelLocalization::getCurrentLocale())->get();

        return $links;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function renderOne($id)
    {
        $link = Link::find($id);

        return $link;
    }

    /**
     * @param $query
     * @param $limit
     * @param $paginate
     * @param $pagination
     *
     * @return mixed
     */
    public function buildPagination($query, $limit, $paginate, $pagination)
    {
        if ($paginate && is_numeric($pagination)) {
            return $query->{$this->paginationType}($pagination);
        } else {
            if ($limit && is_numeric($limit)) {
                $query->take($limit);
            }

            return $query->get();
        }
    }

    /**
     * @param array $input
     */
    public function create($input)
    {
        $link = new Link();
        $link->title = $input['title'];
        $link->link = $input['link'];
        $link->locale = LaravelLocalization::getCurrentLocale();

        $link->save();
    }

    /**
     * @param array $input
     * @param $id
     */
    public function update($input, $id)
    {
        $link = Link::find($id);
        $link->title = $input['title'];
        $link->link = $input['link'];
        $link->locale = LaravelLocalization::getCurrentLocale();

        $link->save();
    }
}
