<?php

namespace App\Repositories\Product;

use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/**
 * Class ProductRepository.
 */
class ProductRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Product::class;

    private $paginationType = 'simplePaginate';

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */

    /**
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function render($limit = null, $paginate = true, $pagination = 10)
    {
        $products = Product::orderBy('unavailable', 'asc')->orderBy('updated_at', 'desc')->paginate(15);
        $europeanCountries = config('countries');
        $userCountry = Cookie::get('country');
        if (in_array($userCountry, $europeanCountries)) {
            //throw away products that are not available in the users country
            foreach ($products as $key => &$product) {
                $test = $product->countries();
            }
        }

        foreach ($products as $key => &$product) {
            $products[$key] = $product->translation(App::getLocale());
            $product->average_rating = $this->calculateAverageRating($product);
        }

        return $products;
    }

    /**
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function renderForBackend()
    {
        $products = Product::orderBy('unavailable', 'asc')->orderBy('updated_at', 'desc')->get();
        $europeanCountries = config('countries');
        $userCountry = Cookie::get('country');
        if (in_array($userCountry, $europeanCountries)) {
            //throw away products that are not available in the users country
            foreach ($products as $key => &$product) {
                $test = $product->countries();
            }
        }

        foreach ($products as $key => &$product) {
            $products[$key] = $product->translation(App::getLocale());
            $product->average_rating = $this->calculateAverageRating($product);
        }

        return $products;
    }


    /**
     * @param $limit
     * @param $paginate
     * @param $pagination
     * @param $id
     * @return mixed
     */
    public function renderByCategoryId($limit, $paginate, $pagination, $id)
    {
        $products = Product::where('category_id', $id)->orderBy('unavailable', 'asc')->orderBy('updated_at', 'desc')->paginate($pagination);
        foreach ($products as $key => &$product) {
            $products[$key] = $product->translation(App::getLocale());
            $product->average_rating = $this->calculateAverageRating($product);
        }

        return $products;
    }

    public function renderByBrandId($limit, $paginate, $pagination, $id)
    {
        $products = Product::where('brand_id', $id)->orderBy('created_at', 'desc')->get();
        foreach ($products as $key => &$product) {
            $products[$key] = $product->translation(App::getLocale());
            $product->average_rating = $this->calculateAverageRating($product);
        }

        return $products;
    }

    public function renderOne($id)
    {
        $product = Product::find($id);
        $product->average_rating = $this->calculateAverageRating($product);
        $product = $product->translation(App::getLocale());

        //get the appropriate affiliate links
        $country = Cookie::get('country');
        $language = LaravelLocalization::getCurrentLocale();

        $linksToKeep = [];
        foreach (config('stores') as $key => $store) {
            $foundCountry = false;
            $foundLanguage = false;
            foreach ($product->affiliateLinks()->get() as $linkKey => $link) {
                if ($link->store == $key) {
                    if ($key == 'coolblue') {
                        if ($link->language == $language && $link->country == $country) {
                            $linksToKeep[$key] = $linkKey;

                            break;
                        } else {
                            if ($link->country == $country) {
                                $linksToKeep[$key] = $linkKey;
                                $foundCountry = true;

                                break;
                            } else {
                                if ($link->language == $language) {
                                    $linksToKeep[$key] = $linkKey;
                                    $foundLanguage = true;

                                    break;
                                } else {
                                    if (! $foundLanguage && ! $foundCountry) {
                                        $linksToKeep[$key] = $linkKey;
                                    }
                                }
                            }
                        }
                    } else {
                        if ($key != 'amazon') {
                            if ($link->language == $language && $link->country == $country) {
                                $linksToKeep[$key] = $linkKey;

                                break;
                            } else {
                                if ($link->language == $language) {
                                    $linksToKeep[$key] = $linkKey;
                                    $foundLanguage = true;
                                } else {
                                    if ($link->country == $country) {
                                        $linksToKeep[$key] = $linkKey;
                                        $foundCountry = true;
                                    } else {
                                        if (! $foundLanguage && ! $foundCountry) {
                                            $linksToKeep[$key] = $linkKey;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //throw away what we don't need
        foreach ($product->affiliateLinks()->get() as $linkKey => $link) {
            foreach ($linksToKeep as $store => $key) {
                if ($link->store == $store && $linkKey != $key) {
                    if ($product->store != 'amazon') {
                        $product->affiliateLinks->forget($linkKey);
                    }
                }
            }
        }

        return $product;
    }

    /**
     * @param $query
     * @param $limit
     * @param $paginate
     * @param $pagination
     *
     * @return mixed
     */
    public function buildPagination($query, $limit, $paginate, $pagination)
    {
        if ($paginate && is_numeric($pagination)) {
            return $query->{$this->paginationType}($pagination);
        } else {
            if ($limit && is_numeric($limit)) {
                $query->take($limit);
            }

            return $query->get();
        }
    }

    protected function calculateAverageRating($product)
    {
        $scores = [$product->score_bol, $product->score_amazon];
        $counter = 0;
        $totalScore = 0;
        foreach ($scores as $score) {
            if ($score > 0) {
                $totalScore += $score;
                $counter++;
            }
        }

        if ($counter > 0) {
            return round(($totalScore / $counter) / 2, 0);
        } else {
            return;
        }
    }
}
