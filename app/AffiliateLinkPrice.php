<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliateLinkPrice extends Model
{
    public function affiliateLink()
    {
        return $this->belongsTo(\App\AffiliateLink::class);
    }
}
