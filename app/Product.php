<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    public function category()
    {
        return $this->belongsTo(Models\Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Models\Brand::class);
    }

    public function countries()
    {
        return $this->hasMany(Models\Country::class);
    }

    public function affiliateLinks()
    {
        return $this->hasMany(\App\AffiliateLink::class);
    }

    public function translation($language = null)
    {
        if ($language == null) {
            $language = App::getLocale();
        }
        $product = $this->hasMany(\App\ProductTranslations::class)->where('language', '=', $language)->get()->first();
        if ($product) {
            $this->title = $product->title;
            $this->link = $product->link;
            $this->description = $product->description;
        }

        return $this;
    }
}
