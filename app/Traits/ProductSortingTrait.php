<?php

namespace App\Traits;

use App\Models\AmazonProduct;
use Illuminate\Support\Facades\Log;

trait ProductSortingTrait
{


    /**
     * @param  string    $sortBy
     * @param  string    $locale
     * @param  int|null  $categoryID
     * @return mixed
     */
    public function getSortedProducts(string $sortBy, string $locale, string $country, int $categoryID = null)
    {
        $sortByArr = explode('_', $sortBy);
        $numberOfResultsPerPage = 30;
        Log::info($sortByArr[0]);
        Log::info($sortByArr[1]);
        if (!is_null($categoryID)){
            Log::info($categoryID);
        }
        if(is_null($categoryID)) {
            switch ($sortByArr[0]) {
                case 'name':
                    Log::info("Sorting by name");
                    return AmazonProduct::where('price', '>', 0)->where('publish', '!=', 0)->where('locale', $locale)->where('country', $country)->orderBy('title', $sortByArr[1])->paginate($numberOfResultsPerPage);
                case 'price':
                    Log::info("Sorting by price");
                    return AmazonProduct::where('price', '>', 0)->where('publish', '!=', 0)->where('locale', $locale)->where('country', $country)->orderBy('price', $sortByArr[1])->paginate($numberOfResultsPerPage);
                case 'updated':
                    return AmazonProduct::where('price', '>', 0)->where('publish', '!=', 0)->where('locale', $locale)->where('country', $country)->orderBy('updated_at', $sortByArr[1])->paginate($numberOfResultsPerPage);
                default:
                    return AmazonProduct::where('price', '>', 0)->where('publish', '!=', 0)->where('locale', $locale)->where('country', $country)->orderBy('updated_at', 'desc')->paginate($numberOfResultsPerPage);
            }
        }else {
            switch ($sortByArr[0]) {
                case 'name':
                    Log::info("Sorting by name");
                    return AmazonProduct::where('price', '>', 0)->where('publish', '!=', 0)->where('locale', $locale)->where('country', $country)->where('category_id', $categoryID)->orderBy('title', $sortByArr[1])->paginate($numberOfResultsPerPage);
                case 'price':
                    Log::info("Sorting by price");
                    return AmazonProduct::where('price', '>', 0)->where('publish', '!=', 0)->where('locale', $locale)->where('country', $country)->where('category_id', $categoryID)->orderBy('price', $sortByArr[1])->paginate($numberOfResultsPerPage);
                case 'updated':
                    return AmazonProduct::where('price', '>', 0)->where('publish', '!=', 0)->where('locale', $locale)->where('country', $country)->where('category_id', $categoryID)->orderBy('updated_at', $sortByArr[1])->paginate($numberOfResultsPerPage);
                default:
                    return AmazonProduct::where('price', '>', 0)->where('publish', '!=', 0)->where('locale', $locale)->where('country', $country)->where('category_id', $categoryID)->orderBy('updated_at', 'desc')->paginate($numberOfResultsPerPage);
            }
        }
    }
}
