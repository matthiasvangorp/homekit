<?php

namespace App\Console\Commands;

use App\Jobs\GermanProductsByBrandAndQuery;
use App\Models\Brand;
use Illuminate\Console\Command;

class GermanProductsByParametersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:brand-and-query {brand} {query}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a job to retrieve products by brand and query';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $brand = Brand::where('name', $this->argument('brand'))->first();
        $query = $this->argument('query');
        dispatch(new GermanProductsByBrandAndQuery($brand, $query));

        $this->info('The GermanProductsByBrands job has been dispatched.');
    }
}
