<?php

namespace App\Console\Commands;

use App\Jobs\FixCategoryIDs;
use Illuminate\Console\Command;

class FixCategoryIDsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a job to set category ids';
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new FixCategoryIDs());

        $this->info('The FixCategoryIDsCommand job has been dispatched.');
    }
}
