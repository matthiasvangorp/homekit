<?php

namespace App\Console\Commands;

use App\Jobs\ProductByAsin;
use App\Models\Brand;
use Illuminate\Console\Command;

class ProductByAsinCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:byasin {brand} {asin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to get a product by Asin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $brand = Brand::where('name', $this->argument('brand'))->first();
        $asin = $this->argument('asin');

        dispatch(new ProductByAsin($brand, $asin));

        $this->info('The ProductByAsin job has been dispatched with ASIN' . $asin . " for brand ". $brand->name);
    }
}
