<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\GermanProductsByBrands;

class GermanProductsByBrandsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'german:products-by-brands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a job to retrieve German products by brands.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new GermanProductsByBrands());

        $this->info('The GermanProductsByBrands job has been dispatched.');
    }
}
