<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SpanishProductsByBrands;

class SpanishProductsByBrandsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spanish:products-by-brands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a job to retrieve Spanish products by brands.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new SpanishProductsByBrands());

        $this->info('The SpanishProductsByBrands job has been dispatched.');
    }
}
