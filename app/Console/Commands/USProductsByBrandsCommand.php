<?php

namespace App\Console\Commands;

use App\Jobs\USProductsByBrands;
use Illuminate\Console\Command;


class USProductsByBrandsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'us:products-by-brands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a job to retrieve US products by brands.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new USProductsByBrands());

        $this->info('The USProductsByBrands job has been dispatched.');
    }
}
