<?php

namespace App\Console\Commands;


use App\Jobs\Products;
use Illuminate\Console\Command;

class ProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     * e
     * @var string
     */
    protected $signature = 'products:get {locale}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a job to retrieve products by locale';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $locale = strtoupper($this->argument('locale'));
        dispatch(new Products($locale));

        $this->info('The GetProductsCommand job has been dispatched.');
    }
}
