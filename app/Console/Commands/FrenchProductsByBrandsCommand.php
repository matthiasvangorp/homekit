<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\FrenchProductsByBrands;

class FrenchProductsByBrandsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'french:products-by-brands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a job to retrieve French products by brands.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new FrenchProductsByBrands());

        $this->info('The FrenchProductsByBrands job has been dispatched.');
    }
}
