<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('german:products-by-brands')->weeklyOn(7, '11:50');
        $schedule->command('products:get nl')->weeklyOn(7, '13:00');
        $schedule->command('products:get fr')->weeklyOn(7, '14:00');
        $schedule->command('products:get en')->weeklyOn(7, '15:00');
        $schedule->command('products:get es')->weeklyOn(7, '16:00');
        $schedule->command('fix:categories')->weeklyOn(7, '17:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
