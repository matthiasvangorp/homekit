<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{!! trans('meta.homepage.description') !!}">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{!! $title !!}</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/languages.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/flag-icon.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/shop-homepage.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">


@if(isset($otherProducts))
        @foreach($otherProducts as $otherProduct)
            <link rel="alternate" hreflang="{{ $otherProduct->locale }}" href="{{ env('APP_URL') }}/{{strtolower($otherProduct->locale)}}/products/{{$otherProduct->id}}" />
        @endforeach
    @else
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
            <link rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, url()->current(), [], true) }}/" />
        @endforeach
    @endif

    <link rel="canonical" href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), url()->current(), [], true) }}" />


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>



    <![endif]-->
    @if($product->getChartData() != "[]")
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>

    <script>
        // Set up the chart data from the PHP data

        $(document).ready(function() {
            var chartData = {!! $product->getChartData(); !!};

            // Find the highest price
            var highestPrice = Math.max(...chartData.map(d => d.price));

            // Calculate the Y-axis maximum (40% higher than the highest price)
            var yAxisMax = highestPrice * 1.40;

            // Set up the chart configuration
            var chartConfig = {
                type: 'line',
                scaleX: {
                    labels: chartData.map(function(d) { return d.date; }),
                    "item": { "font-size": 10 }
                },
                scaleY: {
                    "format": "€%v",
                    "max-value": yAxisMax,  // Set the maximum value for Y-axis
                    "step": yAxisMax / 5    // Adjust step size for better readability
                },
                series: [{
                    values: chartData.map(function(d) { return [d.date, d.price]; })
                }],
                tooltip: {
                    text: '€%v',
                    backgroundColor: '#000',
                    borderColor: '#000',
                    borderRadius: '5px',
                    fontColor: '#fff'
                }
            };

            // Render the chart
            zingchart.render({
                id: 'chart',
                data: chartConfig,
                height: '500',
                width: '100%'
            });
        });
    </script>
    @endif

    <script type="text/javascript">
        function getTrackingId() {
            const cookies = document.cookie.split(';').map(cookie => cookie.trim());


            const trackingIdCookie = cookies.find(cookie => cookie.startsWith('tracking_id='));


            if (trackingIdCookie) {
                const trackingId = trackingIdCookie.split('=')[1];
                return trackingId;
            } else {
                return null;
            }
        }

        function trackProductClick(productId, country) {
            const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            const trackingId = getTrackingId();
            if (!trackingId) {

                return;
            }


            fetch('/track-click', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': csrfToken
                },
                body: JSON.stringify({
                    product_id: productId,
                    country: country,
                    tracking_id: trackingId
                }),
                credentials: 'same-origin', // This ensures cookies are sent with the request
                redirect: 'follow'
            }).then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
                .then(data => {
                    if (data.success) {

                    } else {

                    }
                })
                .catch(error => {
                });
        }
    </script>

    <!-- <script src="https://letsdothis.live/build/assets/feedback-widget-BPNUstOq.js"></script> -->
    <script>
        function loadFeedbackWidget() {
            const clientIdentifier = 'V0F0ZGTOFR';
            fetch('https://letsdothis.live/api/feedback-widget-url', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({ client_identifier: clientIdentifier })
            })
                .then(response => {
                    if (!response.ok) {
                        return response.text().then(text => {
                            throw new Error(`HTTP error! status: ${response.status}, body: ${text}`);
                        });
                    }
                    return response.json();
                })
                .then(data => {
                    if (data.error) {
                        console.error('Error loading feedback widget:', data.error);
                        return;
                    }
                    console.log('Received widget URL:', data.url);
                    const script = document.createElement('script');
                    script.src = data.url;
                    script.onload = function() {
                        console.log('Widget script loaded successfully');
                        if (typeof initFeedbackWidgetWithClient === 'function') {
                            initFeedbackWidgetWithClient(clientIdentifier);
                        } else {
                            console.error('initFeedbackWidgetWithClient function not found');
                        }
                    };
                    script.onerror = function() {
                        console.error('Failed to load widget script');
                    };
                    document.head.appendChild(script);
                })
                .catch(error => {
                    console.error('Error loading feedback widget:', error.message);
                });
        }
        // Initial load
        loadFeedbackWidget();
        // Check for updates every hour
        setInterval(loadFeedbackWidget, 3600000);
    </script>




</head>
    <body>
    @include('layouts.partials.navigation')
    @include('products.layouts.partials.show')
    @include('layouts.partials.footer')
    <script type="text/javascript">







    </script>

    <!-- Modal -->

    <div class="modal fade" id="bolModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">{{__('products.price_history')}} {!! __('products.price_bol') !!}</h4>
                </div>
                <div class="modal-body">
                    <canvas id="bolCanvas" width="568" height="300"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="amazonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">{{__('products.price_history')}} {!! __('products.price_amazon') !!}</h4>
                </div>
                <div class="modal-body">
                    <canvas id="amazonCanvas" width="568" height="300"></canvas>
                </div>
            </div>
        </div>
    </div>

    </body>
</html>
