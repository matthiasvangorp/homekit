@extends('layouts.frontend')

@section('content')
    <div class="container">
        <h1>{{ __('products.products_tagged_with') }}: {{ $tag->name }}</h1>

        <div class="d-flex justify-content-end align-items-start mb-3 select-div">
            <form method="GET" action="{{ route('products.by.tag', $tag->slug) }}" class="form-inline">
                <label for="sort" class="mr-2">{!! __('messages.sort.by') !!}</label>
                <select name="sort" id="sort" class="custom-select" onchange="this.form.submit()">
                    <option value="price_asc" {{ $currentSort == 'price_asc' ? 'selected' : '' }}>{!! __('messages.sort.price_asc') !!}</option>
                    <option value="price_desc" {{ $currentSort == 'price_desc' ? 'selected' : '' }}>{!! __('messages.sort.price_desc') !!}</option>
                    <option value="updated_desc" {{ $currentSort == 'updated_desc' || !$currentSort ? 'selected' : '' }}>{!! __('messages.sort.updated_desc') !!}</option>
                    <option value="updated_asc" {{ $currentSort == 'updated_asc' ? 'selected' : '' }}>{!! __('messages.sort.updated_asc') !!}</option>
                    <option value="name_asc" {{ $currentSort == 'name_asc' ? 'selected' : '' }}>{!! __('messages.sort.name_asc') !!}</option>
                    <option value="name_desc" {{ $currentSort == 'name_desc' ? 'selected' : '' }}>{!! __('messages.sort.name_desc') !!}</option>
                </select>
            </form>
        </div>

        <div class="row">
            @foreach ($products as $product)
                <div class="col-md-12 mb-4">
                    <div class="card">
                        <div class="row no-gutters pb-4">
                            <div class="col-md-3">
                                <a href="{{ route('products.show', $product->id) }}">
                                    <img src="{{ $product->image }}" alt="{{ $product->title }}" class="card-img" style="height: 200px; object-fit: cover;">
                                </a>
                            </div>
                            <div class="col-md-9">
                                <div class="card-body">
                                    <h4 class="card-title">{!! Str::limit($product->title, 100) !!}</h4>
                                    <div class="product-tags">
                                        {{ __('products.tags') }} :
                                        @foreach($product->tags as $tag)
                                            <a href="{{ route('products.by.tag', $tag->slug) }}" class="tag">
                                                {{ $tag->name }}
                                            </a>
                                        @endforeach
                                    </div>
                                    <p class="card-text">{!! Str::limit($product->description, 100) !!}</p>
                                    @if($product->country === 'UK')
                                        <p class="card-text"><strong>{!! render_currency('GPB') !!} {{ number_format($product->price, 2) }}</strong></p>
                                    @elseif($product->country === 'US')
                                        <p class="card-text"><strong>{!! render_currency('USD') !!} {{ number_format($product->price, 2) }}</strong></p>
                                    @else
                                        <p class="card-text"><strong>{!! render_currency('EUR') !!} {{ number_format($product->price, 2) }}</strong></p>
                                    @endif
                                    <a href="{{ route('products.show', $product->id) }}" class="btn btn-primary">{{ __('search.view_details') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        {{ $products->appends(request()->query())->links() }}
    </div>
@endsection