<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">

            @if($product->unavailable)
                <div class="alert alert-warning">
                    <strong>{!! trans('products.attention') !!}</strong> {!! trans('products.unavailable_warning') !!}
                    <a href="{{ URL::to('/') }}/categories/{{$product->category->id}}">{!! strtolower($product->category->name) !!}</a>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <h1>{!! $product->brand->name !!} {!!$product->title !!}
                        @if (Auth::check())
                            <a href="{!! route('affiliate-link.create', ['id' => $product->id]) !!}">
                                <span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </a>
                        @endif
                    </h1>

                    <div class="product-tags">
                        {{ __('products.tags') }} :
                        @foreach($product->tags as $tag)
                            <a href="{{ route('products.by.tag', $tag->slug) }}" class="tag">
                                {{ $tag->name }}
                            </a>
                        @endforeach
                    </div>
                    <h4>{!! $product->description !!}</h4>
                    <input type="number" name="rating" id="star_rating" class="rating"
                           value="{!! $product->score !!}" data-readonly data-icon-lib="fa"
                           data-active-icon="fa-star" data-inactive-icon="fa-star-o"/>
                    <br/>
                </div>
            </div>
            <div class="row">
                @if($product->thread)
                    <div class="col-md-4">
                        <img src="{{ URL::to('images/thread_logo.png') }}" alt="thread">
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-4">
                    <p>
                        @if($product->country === 'UK')
                            <a href="{!! $product->url !!}" target="_blank" class="referral_link amazon" onclick="trackProductClick({{ $product->id }}, 'UK'); return true;">
                                {!! render_currency('GPB') !!} {!! number_format($product->price, 2, ",", "."); !!} {!! trans('products.with') !!} {!! render_amazon($product->url) !!}
                            </a>
                        @elseif($product->country === 'US')
                            <a href="{!! $product->url !!}" target="_blank" class="referral_link amazon" onclick="trackProductClick({{ $product->id }}, 'US'); return true;">
                                {!! render_currency('USD') !!} {!! number_format($product->price, 2, ",", "."); !!} {!! trans('products.with') !!} {!! render_amazon($product->url) !!}
                            </a>
                        @else
                            <a href="{!! $product->url !!}" target="_blank" class="referral_link amazon" onclick="trackProductClick({{ $product->id }}, '{{ $product->country }}'); return true;">
                                {!! render_currency('EUR') !!} {!! number_format($product->price, 2, ",", "."); !!} {!! trans('products.with') !!} {!! render_amazon($product->url) !!}
                            </a>
                        @endif
                    </p>
                    @foreach($otherProducts as $otherProduct)
                        @if(isset($otherProduct->price))
                            <p>
                                @if($otherProduct->country === 'UK')
                                    <a href="{!! $otherProduct->url !!}" target="_blank" class="referral_link amazon" onclick="trackProductClick({{ $otherProduct->id }}, 'UK'); return true;">
                                        {!! render_currency('GPB') !!} {!! number_format($otherProduct->price, 2, ",", "."); !!} {!! trans('products.with') !!} {!! render_amazon($otherProduct->url) !!}
                                    </a>
                                @elseif($otherProduct->country === 'US')
                                    <a href="{!! $otherProduct->url !!}" target="_blank" class="referral_link amazon" onclick="trackProductClick({{ $otherProduct->id }}, 'US'); return true;">
                                        {!! render_currency('USD') !!} {!! number_format($otherProduct->price, 2, ",", "."); !!} {!! trans('products.with') !!} {!! render_amazon($otherProduct->url) !!}
                                    </a>
                                @else
                                    <a href="{!! $otherProduct->url !!}" target="_blank" class="referral_link amazon" onclick="trackProductClick({{ $otherProduct->id }}, '{{ $otherProduct->country }}'); return true;">
                                        {!! render_currency('EUR') !!} {!! number_format($otherProduct->price, 2, ",", "."); !!} {!! trans('products.with') !!} {!! render_amazon($otherProduct->url) !!}
                                    </a>
                                @endif
                            </p>
                        @endif
                    @endforeach
                </div>
                <div class="col-md-8">
                    <img src="{{$product->image}}" alt="{{$product->title}}"/>
                </div>
            </div>
            @if($product->getChartData() != "[]")
                <div class="row">
                    <div class="col-md-12">
                        <h3>{{__('products.price_history') }} </h3>
                        <div id="chart"></div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <p>
                        <a href=" {!! URL::to('/') !!}/{!! LaravelLocalization::getCurrentLocale() ."/brands/".$product->brand->id !!}">{{__('brands.brands_title', ['brandname' => $product->brand->name])}}</a>
                    </p>
                </div>
            </div>


        </div>
    </div>


</div>


<!-- /.container -->