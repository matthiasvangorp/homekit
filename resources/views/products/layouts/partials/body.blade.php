    <!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                <table>
                @foreach($products as $product)
                            <tr>
                                <td>{!! $product->brand->name . ' ' . $product->title !!}</td>
                                <td>
                                    {{ Form::open(array('url' => LaravelLocalization::localizeURL('/products/'.$product->id), 'class' => 'pull-right')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                                    {{ Form::close() }}
                                    <a class="btn btn-small btn-success" href="{{ URL::to('products/' . $product->id) }}">Show</a>
                                    <a class="btn btn-small btn-info" href="{{ URL::to('products/' . $product->id . '/edit') }}">Edit</a>
                                    <a class="btn btn-small btn-info" href="{{ URL::to('producttranslations/' . $product->id . '/edit') }}">Translate</a>
                                </td>
                                <td>&nbsp;</td>
                                <td>{{ Carbon::createfromformat('Y-m-d H:i:s', $product->updated_at)->format('d/m/Y H:i') }}</td>
                            </tr>
                @endforeach
                </table>

                <a href="{!! URL::to('products/create') !!}">Create new product</a>

            </div>

        </div>

    </div>

</div>
<!-- /.container -->