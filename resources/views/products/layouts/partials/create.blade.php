<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Create product</H1>
                {!! Form::open(['url' =>  LaravelLocalization::localizeURL('/products') , 'files' => true]) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('title', old('title'), array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::text('description', old('description'), array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {{ Form::label('category', 'Category') }}
                    {!! Form::select('category', $categoriesList, null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::button('Add category', array('class' => 'btn', 'id' => 'btn-add-category')) !!}
                </div>

                <div class="form-group">
                    {{ Form::label('brand', 'Brand') }}
                    {!! Form::select('brand', $brands, null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::button('Add brand', array('class' => 'btn', 'id' => 'btn-add-brand')) !!}
                </div>



                <div class="panel panel-default">
                    <div class="panel-heading">Affiliate URL's</div>
                    <div class="panel-body">
                        <input type="hidden" name="count" value="1" />
                        <div class="affiliate_link">
                            <div class="form-group">
                                {!! Form::label('urls', 'URL') !!}
                                {!! Form::text('urls[]', old('url'), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('stores', 'Store') !!}
                                {!! Form::select('stores[]', $stores, null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('affiliate_countries', 'Country') !!}
                                {!! Form::select('affiliate_countries[]', $countries, null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('currency', 'Currency') !!}
                                {!! Form::select('currency[]', ['EUR'=> 'EURO', 'GPB' => 'English Pound'], null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('price', 'Price') !!}
                                {!! Form::text('price[]', 0,  array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('external_id', 'External id') !!}
                                {!! Form::text('external_id[]', 0,  array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('languages', 'Language') !!}
                                <select class="form-control" name="languages[]">
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        @if ( LaravelLocalization::getCurrentLocale() == $localeCode)
                                            <option selected="selected" value="{!! $localeCode  !!}">{!! $properties['name'] !!}</option>
                                        @else
                                            <option value="{!! $localeCode  !!}">{!! $properties['name'] !!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button id="b1" class="btn add-more" type="button">+</button>

                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('score_conrad', 'Score conrad') !!}
                    {!! Form::text('score_conrad', '0', array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('score_coolblue', 'Score coolblue') !!}
                    {!! Form::text('score_coolblue', '0', array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('score_bol', 'Score bol') !!}
                    {!! Form::text('score_bol', '0', array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('score_amazon', 'Score amazon') !!}
                    {!! Form::text('score_amazon', '0', array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('needs_hub', 'Needs hub') !!}
                    {!! Form::checkbox('needs_hub', 1, false) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('tread', 'Thread') !!}
                    {!! Form::checkbox('thread', 1, false) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('unavailable', 'Unavailable') !!}
                    {!! Form::checkbox('unavailable', 1, false) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('countries', 'Available in these countries') !!}
                    {!! Form::select('countries[]', $countries, null, array('class' => 'form-control', 'multiple' =>'multiple', 'id' => 'countries')) !!}
                </div>

                    <div class="form-group">
                        {!! Form::label('Product Image') !!}
                    {!! Form::file('image', null) !!}
                </div>

                    {!! Form::submit('Create the product', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}


                <!-- Modal (Pop up when add brand button clicked) -->
                    <div class="modal fade" id="modalBrands" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Create Brand</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(['url' => 'brands', 'id' => 'form-brands']) !!}

                                    <div class="form-group">
                                        {!! Form::label('brandName', 'Name') !!}
                                        {!! Form::text('brandName', old('brandName'), array('class' => 'form-control')) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('brandDescription', 'Description') !!}
                                        {!! Form::text('brandDescription', old('brandDescription'), array('class' => 'form-control')) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('brandLink', 'Url') !!}
                                        {!! Form::text('brandLink', old('brandLink'), array('class' => 'form-control')) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-brand" value="add">Add brand</button>
                                </div>
                            </div>
                        </div>
                    </div>


                <!-- Modal (Pop up when add category button clicked) -->
                <div class="modal fade" id="modalCategories" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <h4 class="modal-title" id="myModalLabel">Create category</h4>
                            </div>
                            <div class="modal-body">
                                {!!Html::ul($errors->all()) !!}
                                {!! Form::open(['url' => 'categories']) !!}

                                <div class="form-group">
                                    {!! Form::label('categoryName', 'Name') !!}
                                    {!! Form::text('categoryName', old('categoryName'), array('class' => 'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('categoryDescription', 'Description') !!}
                                    {!! Form::text('categoryDescription', old('description'), array('class' => 'form-control')) !!}
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-category" value="add">Add Category</button>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container -->