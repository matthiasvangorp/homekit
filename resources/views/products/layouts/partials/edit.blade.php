<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Edit {!! $product->title !!}</H1>
                {!! Form::model($product, ['url' => array(LaravelLocalization::localizeURL('/products/'.$product->id)), 'files' => true, 'method' => 'PUT']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('title', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::text('description', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {{ Form::label('category', 'Category') }}
                    {!! Form::select('category', $categoriesList, $product->category_id, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {{ Form::label('brand', 'Brand') }}
                    {!! Form::select('brand', $brands, $product->brand_id, array('class' => 'form-control')) !!}
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Affiliate URL's</div>
                    <div class="panel-body">
                        @foreach ($product->affiliateLinks as $key => $link)
                            <div class="affiliate_link" id="{!! $link->id !!}">
                                <H1>Link <a href="#" onclick="deleteLink({!! $link->id !!});"><i class="far fa-trash-alt"></i>Delete</a></H1>
                                <div class="form-group">
                                    {!! Form::hidden('affiliate_link_ids[]', $link->id, array('id' => 'affiliate_link_id-'.$key)) !!}
                                    {!! Form::label('urls', 'URL') !!}
                                    {!! Form::text('urls[]', $link->url, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('stores', 'Store') !!}
                                    {!! Form::select('stores[]', $stores, $link->store, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('affiliate_countries', 'Country') !!}
                                    {!! Form::select('affiliate_countries[]', $countries, $link->country, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('currency', 'Currency') !!}
                                    {!! Form::select('currency[]', ['EUR'=> 'EURO', 'GPB' => 'English Pound'], $link->currency, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('price', 'Price') !!}
                                    {!! Form::text('price[]', $link->price, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('external_id', 'External id') !!}
                                    {!! Form::text('external_id[]', $link->external_id, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('languages', 'Language') !!}
                                    <select class="form-control" name="languages[]">
                                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                            @if ( $localeCode == $link->language)
                                                <option selected="selected" value="{!! $localeCode  !!}">{!! $properties['name'] !!}</option>
                                            @else
                                                <option value="{!! $localeCode  !!}">{!! $properties['name'] !!}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr/>
                        @endforeach
                        <!--div class="affiliate_link" id="affiliate_link">
                            <div class="form-group">
                                {!! Form::label('urls', 'URL') !!}
                                {!! Form::text('urls[]', old('url'), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('stores', 'Store') !!}
                                {!! Form::select('stores[]', $stores, null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('affiliate_countries', 'Country') !!}
                                {!! Form::select('affiliate_countries[]', $countries, null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('affiliate_currency', 'Currency') !!}
                                {!! Form::select('affiliate_currency[]', ['EUR'=> 'EURO', 'GBP' => 'English Pound'], null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('price', 'Price') !!}
                                {!! Form::text('price[]', 0,  array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('languages', 'Language') !!}
                                <select class="form-control" name="languages[]">
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        @if ( LaravelLocalization::getCurrentLocale() == $localeCode)
                                            <option selected="selected" value="{!! $localeCode  !!}">{!! $properties['name'] !!}</option>
                                        @else
                                            <option value="{!! $localeCode  !!}">{!! $properties['name'] !!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div-->
                        <button id="b1" class="btn add-more" type="button">+</button>

                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('score_conrad', 'Score conrad') !!}
                    {!! Form::text('score_conrad', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('score_coolblue', 'Score coolblue') !!}
                    {!! Form::text('score_coolblue', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('score_bol', 'Score bol') !!}
                    {!! Form::text('score_bol', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('score_amazon', 'Score amazon') !!}
                    {!! Form::text('score_amazon', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('needs_hub', 'Needs hub') !!}
                    {!! Form::checkbox('needs_hub', 1, $product->needs_hub) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('tread', 'Thread') !!}
                    {!! Form::checkbox('thread', 1, $product->thread) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('unavailable', 'Unavailable') !!}
                    {!! Form::checkbox('unavailable', 1, $product->unavailable) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('countries', 'Available in these countries') !!}
                    {!! Form::select('countries[]', $countries, $selectedCountries, array('class' => 'form-control', 'multiple' =>'multiple', 'id' => 'countries')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Product Image') !!}
                    {!! Form::file('image', null) !!}
                </div>

                    {!! Form::submit('Update the product', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}

            </div>

        </div>

    </div>

</div>
<!-- /.container -->