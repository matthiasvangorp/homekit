<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{!! trans('meta.homepage.description') !!}">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('search.search_results_for') }} "{{ $query }}"</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/languages.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/flag-icon.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/shop-homepage.css') }}">

    @if(isset($otherProducts))
        @foreach($otherProducts as $otherProduct)
            <link rel="alternate" hreflang="{{ $otherProduct->locale }}" href="{{ env('APP_URL') }}/{{strtolower($otherProduct->locale)}}/products/{{$otherProduct->id}}" />
        @endforeach
    @else
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
            <link rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, url()->current(), [], true) }}/" />
        @endforeach
    @endif

    <link rel="canonical" href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), url()->current(), [], true) }}" />


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>



    <![endif]-->

    <!-- <script src="https://letsdothis.live/build/assets/feedback-widget-BPNUstOq.js"></script> -->
    <script>
        function loadFeedbackWidget() {
            const clientIdentifier = 'V0F0ZGTOFR';
            fetch('https://letsdothis.live/api/feedback-widget-url', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({ client_identifier: clientIdentifier })
            })
                .then(response => {
                    if (!response.ok) {
                        return response.text().then(text => {
                            throw new Error(`HTTP error! status: ${response.status}, body: ${text}`);
                        });
                    }
                    return response.json();
                })
                .then(data => {
                    if (data.error) {
                        console.error('Error loading feedback widget:', data.error);
                        return;
                    }
                    console.log('Received widget URL:', data.url);
                    const script = document.createElement('script');
                    script.src = data.url;
                    script.onload = function() {
                        console.log('Widget script loaded successfully');
                        if (typeof initFeedbackWidgetWithClient === 'function') {
                            initFeedbackWidgetWithClient(clientIdentifier);
                        } else {
                            console.error('initFeedbackWidgetWithClient function not found');
                        }
                    };
                    script.onerror = function() {
                        console.error('Failed to load widget script');
                    };
                    document.head.appendChild(script);
                })
                .catch(error => {
                    console.error('Error loading feedback widget:', error.message);
                });
        }
        // Initial load
        loadFeedbackWidget();
        // Check for updates every hour
        setInterval(loadFeedbackWidget, 3600000);
    </script>






</head>
    <body>
    @include('layouts.partials.navigation')
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            @include('layouts.partials.categories')
            <div class="col-md-9">
                <h1 class="mb-4 pb-2">{{ __('search.search_results_for') }} "{{ $query }}"</h1>
                <br/>

                @if ($products->count() > 0)
                    <div class="row">
                        @foreach ($products as $product)
                            <div class="col-md-12 mb-4">
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-md-3">
                                            <a href="{{ route('products.show', $product->id) }}">
                                                <img src="{{ $product->image }}" alt="{{ $product->title }}" class="card-img" style="height: 200px; object-fit: cover;">
                                            </a>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="card-body">
                                                <h4 class="card-title">{!! Str::limit($product->title , 100) !!}</h4>
                                                <p class="card-text">{!! Str::limit($product->description, 100) !!}</p>
                                                @if($product->country === 'UK')
                                                    <p class="card-text"><strong>{!! render_currency('GPB') !!} {{ number_format($product->price, 2) }}</strong></p>
                                                @elseif($product->country === 'US')
                                                    <p class="card-text"><strong>{!! render_currency('USD') !!} {{ number_format($product->price, 2) }}</strong></p>
                                                @else
                                                    <p class="card-text"><strong>{!! render_currency('EUR') !!} {{ number_format($product->price, 2) }}</strong></p>
                                                @endif
                                                <a href="{{ route('products.show', $product->id) }}" class="btn btn-primary">{{ __('search.view_details') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="mt-4">
                        {{ $products->links() }}
                    </div>
                @else
                    <p class="lead">No products found matching your search.</p>
                @endif
            </div>
        </div>
    </div>
    <!-- /.container -->
    @include('layouts.partials.footer')
    </body>
</html>
