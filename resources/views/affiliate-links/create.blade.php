<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Homekit products</title>




    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/shop-homepage.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>
    <body>
    @include('layouts.partials.navigation')
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            @include('layouts.partials.categories')

            <div class="col-md-9">


                <div class="row">
                    {!!Html::ul($errors->all()) !!}
                    <h1>Add Affiliate Link for {{ $amazonProduct->title }}</h1>
                    <form method="POST" action="{{ route('affiliate-link.store', ['id' => $amazonProduct->id]) }}">
                        @csrf
                        <div class="form-group">
                            <label for="url">URL:</label>
                            <input type="text" name="url" id="url" class="form-control" value="{{ old('url') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="locale">Locale:</label>
                            <select class="form-control" name="locale" id="locale">
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    @if ( LaravelLocalization::getCurrentLocale() == $localeCode)
                                        <option selected="selected" value="{!! LaravelLocalization::getLocalizedURL($localeCode, null, [], true)   !!} ">{!! $properties['name'] !!}</option>
                                    @else
                                        <option value="{!! LaravelLocalization::getLocalizedURL($localeCode, null, [], true)   !!} ">{!! $properties['name'] !!}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="country">Country:</label>
                            <select name="country" id="country" class="form-control" required>
                                @foreach(config('countries') as $localeCode => $name)
                                    @if (\Illuminate\Support\Facades\Cookie::get('country') == $localeCode)
                                        <option selected="selected" value="{{ $localeCode }}">{{ $name }}</option>
                                    @else
                                        <option value="{{ $localeCode }}">{{ $name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="currency">Currency:</label>
                            <input type="text" name="currency" id="currency" class="form-control" value="HUF" required>
                        </div>
                        <div class="form-group">
                            <label for="price">Price:</label>
                            <input type="number" step="0.01" min="0" name="price" id="price" class="form-control" value="{{ old('price') }}" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Add affiliate link</button>
                    </form>









            </div>
        </div>

    </div>

    <!-- /.container -->
    @include('layouts.partials.footer-backend')

    </body>
</html>
