<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>{!! trans('articles.create_article') !!}</H1>
                {!! Form::open(['url' =>  LaravelLocalization::localizeURL('/articles') , 'files' => true]) !!}

                <div class="form-group">
                    {!! Form::label('title', trans('articles.title')) !!}
                    {!! Form::text('title', old('title'), array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('subtitle', trans('articles.subtitle')) !!}
                    {!! Form::text('subtitle', old('subtitle'), array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('article', trans('articles.article')) !!}
                    {!! Form::textarea('article', old('article'), array('class' => 'form-control')) !!}
                </div>

                    {!! Form::submit(trans('articles.create_article'), array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}

                </div>
            </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container -->