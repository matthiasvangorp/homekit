<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>{!! trans('articles.create_article') !!}</H1>
                {!! Form::open(['url' =>  LaravelLocalization::localizeURL('/articles/'.$article->id) , 'method' => 'PUT']) !!}

                <div class="form-group">
                    {!! Form::label('title', trans('articles.title')) !!}
                    {!! Form::text('title', $article->title, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('subtitle', trans('articles.subtitle')) !!}
                    {!! Form::text('subtitle', $article->subtitle, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('article', trans('articles.article')) !!}
                    {!! Form::textarea('article', $article->article, array('class' => 'form-control')) !!}
                </div>

                    {!! Form::submit(trans('articles.edit_article'), array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}

                </div>
            </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container -->