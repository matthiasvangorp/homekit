<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-9">


            <div class="row">
                <h1>{!! $article->title!!}
                    @if (Auth::check())
                        <a href="{!! URL::to('articles/' . $article->id . '/edit') !!}"><span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                    @endif</h1></h1>
                <h2>{!! $article->subtitle!!}</h2>
                <span class="date">{{ Carbon\Carbon::parse($article->created_at)->format('d-m-Y') }}</span>
                {!! $article->article !!}

            </div>

        </div>

    </div>

</div>
<!-- /.container -->