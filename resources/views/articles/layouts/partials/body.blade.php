<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-12">


            <div class="row">
                @foreach($articles as $article)
                    <h1>{!! $article->title !!}
                        @if (Auth::check())
                            <a href="{!! URL::to('articles/' . $article->id . '/edit') !!}"><span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                        @endif</h1>
                    <h2>{!! $article->subtitle !!}</h2>
                    <span class="date">{{ Carbon\Carbon::parse($article->created_at)->format('d-m-Y') }}</span>
                    {!! Str::limit($article->article, 300)  !!}
                    <a href="{!! LaravelLocalization::localizeURL('/articles/'.$article->id) !!}" title="{!! $article->title !!}">{!! trans('articles.read_more') !!}</a>
                    <hr/>
                @endforeach


            </div>

        </div>

    </div>

</div>
<!-- /.container -->