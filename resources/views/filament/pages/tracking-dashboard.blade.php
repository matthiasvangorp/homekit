<x-filament-panels::page>
    <form method="GET" action="{{ route('filament.admin.pages.tracking-dashboard') }}">
        {{ $this->form }}

        <x-filament::button type="submit">
            Update
        </x-filament::button>
    </form>

    <div class="mt-4 grid grid-cols-1 gap-4 sm:grid-cols-2 lg:grid-cols-3">
        <x-filament::card>
            <h3 class="text-lg font-medium">Total Visitors</h3>
            <p class="mt-2 text-3xl font-bold">{{ $this->data['totalVisitors'] }}</p>
        </x-filament::card>

        <x-filament::card>
            <h3 class="text-lg font-medium">Page Views</h3>
            <p class="mt-2 text-3xl font-bold">{{ $this->data['pageViews'] }}</p>
        </x-filament::card>
    </div>

    <div class="mt-8">
        <h3 class="text-lg font-medium mb-4">Top Pages</h3>
        {{ $this->table }}
    </div>

    <div x-data="charts" x-init="initCharts()" class="mt-8 grid grid-cols-1 gap-4 lg:grid-cols-2">
        <x-filament::card>
            <h3 class="text-lg font-medium mb-4">Visitor Trend</h3>
            <div style="height: 300px;">
                <canvas id="visitorTrendChart"></canvas>
            </div>
        </x-filament::card>

        <x-filament::card>
            <h3 class="text-lg font-medium mb-4">Top Cities</h3>
            <div style="height: 300px;">
                <canvas id="topCitiesChart"></canvas>
            </div>
        </x-filament::card>

        <x-filament::card>
            <h3 class="text-lg font-medium mb-4">Top Countries</h3>
            <div style="height: 300px;">
                <canvas id="topCountriesChart"></canvas>
            </div>
        </x-filament::card>

        <x-filament::card>
            <h3 class="text-lg font-medium mb-4">Top Referrers</h3>
            <div style="height: 300px;">
                <canvas id="topReferrersChart"></canvas>
            </div>
        </x-filament::card>
    </div>

    @script
    <script>
        Alpine.data('charts', () => ({
            charts: {},
            initCharts() {
                this.createVisitorTrendChart();
                this.createPieChart('topCitiesChart', @json($this->data['topCities']), 'city', 'total');
                this.createPieChart('topCountriesChart', @json($this->data['topCountries']), 'country', 'total');
                this.createPieChart('topReferrersChart', @json($this->data['topReferrers']), 'referrer', 'total');
            },
            createVisitorTrendChart() {
                const ctx = document.getElementById('visitorTrendChart');
                const data = @json($this->data['dailyVisitorData']);
                this.charts.visitorTrend = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: data.map(item => item.date),
                        datasets: [{
                            label: 'Daily Visitors',
                            data: data.map(item => item.visitors),
                            borderColor: 'rgb(75, 192, 192)',
                            tension: 0.1
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
            },
            createPieChart(elementId, data, labelKey, valueKey) {
                const ctx = document.getElementById(elementId);
                this.charts[elementId] = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: data.map(item => item[labelKey]),
                        datasets: [{
                            data: data.map(item => item[valueKey]),
                            backgroundColor: [
                                '#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0', '#9966FF',
                                '#FF9F40', '#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0'
                            ]
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            legend: {
                                position: 'bottom',
                            },
                            tooltip: {
                                callbacks: {
                                    label: function(context) {
                                        const label = context.label || '';
                                        const value = context.parsed || 0;
                                        const total = context.dataset.data.reduce((acc, data) => acc + data, 0);
                                        const percentage = ((value / total) * 100).toFixed(1);
                                        return `${label}: ${value} (${percentage}%)`;
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }));
    </script>
    @endscript
</x-filament-panels::page>