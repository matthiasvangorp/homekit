<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($locales as $key => $value)

        <url>
            <loc>{{ url('/') }}/{{$key}}</loc>
            <changefreq>weekly</changefreq>
            <priority>0.8</priority>
        </url>
        @foreach ($categories as $category)
            <url>
                <loc>{{ url('/') }}/{{$key}}/categories/{{ $category->id }}</loc>
                @if(!is_null($category->updated_at))
                    <lastmod>{{ $category->updated_at->tz('UTC')->toAtomString() }}</lastmod>
                @endif
                <changefreq>weekly</changefreq>
                <priority>0.8</priority>
            </url>
        @endforeach
            @foreach ($brands as $brand)
                <url>
                    <loc>{{ url('/') }}/{{$key}}/brands/{{ $brand->id }}</loc>
                    @if(!is_null($brand->updated_at))
                        <lastmod>{{ $brand->updated_at->tz('UTC')->toAtomString() }}</lastmod>
                    @endif
                    <changefreq>weekly</changefreq>
                    <priority>0.8</priority>
                </url>
            @endforeach
    @endforeach

        @foreach ($products as $product)
            <url>
                <loc>{{ url('/') }}/{{strtolower($product->locale)}}/products/{{ $product->id }}</loc>
                @if(!is_null($product->updated_at))
                    <lastmod>{{ $product->updated_at->tz('UTC')->toAtomString() }}</lastmod>
                @endif
                <changefreq>weekly</changefreq>
                <priority>0.8</priority>
            </url>
        @endforeach
</urlset>