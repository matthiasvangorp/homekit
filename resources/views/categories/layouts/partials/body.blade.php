<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                <table>
                @foreach($categories as $category)
                    <tr>
                        <td>{!! $category->name !!}</td>
                        <td>
                            {{ Form::open(array('url' => 'categories/' . $category->id, 'class' => 'pull-right')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::submit('Delete this category', array('class' => 'btn btn-warning')) }}
                            {{ Form::close() }}
                            <a class="btn btn-small btn-success" href="{{ URL::to('products/' . $category->id) }}">Show this Category</a>
                            <a class="btn btn-small btn-info" href="{{ URL::to('products/' . $category->id . '/edit') }}">Edit this Category</a>
                        </td>
                    </tr>
                @endforeach
                </table>

                <a href="{!! URL::to('categories/create') !!}">Create new Category</a>

            </div>

        </div>

    </div>

</div>
<!-- /.container -->