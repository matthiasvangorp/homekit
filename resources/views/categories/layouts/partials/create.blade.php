<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Create category</H1>
                {!! Form::open(['url' => LaravelLocalization::localizeURL('/categories')]) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', old('name'), array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::text('description', old('description'), array('class' => 'form-control')) !!}
                </div>

                    {!! Form::submit('Create the category', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}

            </div>

        </div>

    </div>

</div>
<!-- /.container -->