<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Edit {!! $product->title !!}</H1>
                <h2>{{$product->asin}}</h2>
                {!! Form::model($product, ['url' => array('producttranslations', $product->id), 'files' => true, 'method' => 'PUT']) !!}

                <div class="form-group">
                    {!! Form::hidden('id', $product->id) !!}
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('title', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::text('description', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('url', 'Url') !!}
                    {!! Form::text('url', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('price', 'price') !!}
                    {!! Form::text('price', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {{ Form::label('language', 'Language') }}
                    {!! Form::select('language', ['nl'=>'Nederlands', 'fr'=>'Frans', 'de'=>'Duits', 'es'=>'Spaans'],  array('class' => 'form-control')) !!}
                </div>

                    {!! Form::submit('Translate the product', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}

            </div>

        </div>

    </div>

</div>
<!-- /.container -->