<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                <h1>{!! $product->title !!}</h1>
                <h2>{!! $product->description !!}</h2>
                <p>
                    Brand : {!! $product->brand->name !!}
                </p>
                <p>
                    Category : {!! $product->category->name !!}
                </p>

                <img src="{{ URL::to('/') }}/images/{!! $product->image !!}"/>

            </div>

        </div>

    </div>

</div>
<!-- /.container -->