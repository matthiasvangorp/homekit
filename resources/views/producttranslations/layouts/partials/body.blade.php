<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                <table>
                @foreach($products as $product)
                            <tr>
                                <td>{!! $product->title !!}</td>
                                <td>
                                    {{ Form::open(array('url' => 'products/' . $product->id, 'class' => 'pull-right')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Delete this product', array('class' => 'btn btn-warning')) }}
                                    {{ Form::close() }}
                                    <a class="btn btn-small btn-success" href="{{ URL::to('products/' . $product->id) }}">Show this Product</a>
                                    <a class="btn btn-small btn-info" href="{{ URL::to('products/' . $product->id . '/edit') }}">Edit this Product</a>
                                </td>
                            </tr>
                @endforeach
                </table>

                <a href="{!! URL::to('products/create') !!}">Create new product</a>

            </div>

        </div>

    </div>

</div>
<!-- /.container -->