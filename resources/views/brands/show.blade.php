<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Homekit products</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/shop-homepage.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- <script src="https://letsdothis.live/build/assets/feedback-widget-BPNUstOq.js"></script> -->
    <script>
        function loadFeedbackWidget() {
            const clientIdentifier = 'V0F0ZGTOFR';
            fetch('https://letsdothis.live/api/feedback-widget-url', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({ client_identifier: clientIdentifier })
            })
                .then(response => {
                    if (!response.ok) {
                        return response.text().then(text => {
                            throw new Error(`HTTP error! status: ${response.status}, body: ${text}`);
                        });
                    }
                    return response.json();
                })
                .then(data => {
                    if (data.error) {
                        console.error('Error loading feedback widget:', data.error);
                        return;
                    }
                    console.log('Received widget URL:', data.url);
                    const script = document.createElement('script');
                    script.src = data.url;
                    script.onload = function() {
                        console.log('Widget script loaded successfully');
                        if (typeof initFeedbackWidgetWithClient === 'function') {
                            initFeedbackWidgetWithClient(clientIdentifier);
                        } else {
                            console.error('initFeedbackWidgetWithClient function not found');
                        }
                    };
                    script.onerror = function() {
                        console.error('Failed to load widget script');
                    };
                    document.head.appendChild(script);
                })
                .catch(error => {
                    console.error('Error loading feedback widget:', error.message);
                });
        }
        // Initial load
        loadFeedbackWidget();
        // Check for updates every hour
        setInterval(loadFeedbackWidget, 3600000);
    </script>



</head>
    <body>
    @include('layouts.partials.navigation')
    @include('brands.layouts.partials.show')
    @include('layouts.partials.footer')

    </body>
</html>
