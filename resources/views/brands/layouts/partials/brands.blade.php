<div class="col-md-3">
    <p class="lead">Brands</p>
    <div class="list-group">
        @foreach($brands as $brand)
        <a href="{{ URL::to('/') }}/brand/{{$brand->id}}" class="list-group-item">{{$brand->name}}</a>
        @endforeach
    </div>
</div>