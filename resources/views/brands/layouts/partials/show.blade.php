<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-12">
            <h1>{{ __('brands.brands_title', ['brandname' => $brand->name])}}</h1>
            <div class="row">
                @foreach($products as $product)
                    @if(!$product->unavailable)
                        <a href=" {!! URL::to('/') !!}/{!! LaravelLocalization::getCurrentLocale() ."/products/".$product->id !!}">
                    @endif
                            <div class="col-sm-4 col-lg-4 col-md-4 product">
                                @if($product->unavailable)
                                    <div class="unavailable">{!! trans('products.unavailable') !!}</div>
                                @endif
                                <div class="thumbnail">
                                    <img src="{!!$product->image!!}" alt="{!!$product->title!!}"/>
                                    <div class="caption">
                                        <h4>
                                            @if(!$product->unavailable)
                                                <a href=" {!! URL::to('/') !!}/{!! LaravelLocalization::getCurrentLocale() ."/products/".$product->id !!}">
                                            @endif
                                        </h4>
                                        <input type="number" name="rating" id="star_rating" class="rating" value="{!! $product->score !!}" data-readonly data-icon-lib="fa" data-active-icon="fa-star" data-inactive-icon="fa-star-o"/>
                                        <h5 style="text-decoration: none;color:black;display: inline">{!! $product->brand->name !!}</h5> <span style="text-decoration: none;color:black">{!! $product->title !!}</span><br/>
                                        <p id="product-description">{!! Str::limit($product->description, 90)  !!}</p>
                                        @if($product->price < $product->getMaxPriceOverLastMonth())
                                            <div class="product-price-green">
                                                {!! $currency !!} {!! number_format($product->price, 2, ",", "."); !!}
                                            </div>
                                        @else
                                            <div class="product-price">
                                                {!! $currency !!} {!! number_format($product->price, 2, ",", "."); !!}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </a>
                @endforeach

            </div>

        </div>

    </div>

    <!-- modal -->
    <div class="modal fade" id="productModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <div class="modal-body" id="modal-body">

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>
<!-- /.container -->