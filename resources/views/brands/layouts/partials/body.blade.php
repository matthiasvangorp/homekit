<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                @foreach($brands as $brand)
                            <h4>{{$brand->name}}</h4>
                @endforeach

                <a href="{!! URL::to('brands/create') !!}">Create new brand</a>

            </div>

        </div>

    </div>

</div>
<!-- /.container -->