<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Edit {!! $brand->title !!}</H1>
                {!! Form::model($brand, ['url' => LaravelLocalization::localizeURL('/brands').$brand->id, 'files' => true, 'method' => 'PUT']) !!}

                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', null, array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Description') !!}
                        {!! Form::text('description', null, array('class' => 'form-control')) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('link', 'Url') !!}
                        {!! Form::text('link', null, array('class' => 'form-control')) !!}
                    </div>

                {!! Form::submit('Update the brand', array('class' => 'btn btn-primary')) !!}

                {!! Form::close() !!}

            </div>

        </div>

    </div>

</div>
<!-- /.container -->