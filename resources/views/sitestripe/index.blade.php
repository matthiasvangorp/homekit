<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Homekit products</title>


    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/shop-homepage.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>
    <body>
    @include('layouts.partials.navigation')
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            @include('layouts.partials.categories')

            <div class="col-md-9">


                <div class="row">
                    <p>Number of products : {!! $numberOfProducts !!}</p>
                    <a href="{!! URL::to('sitestripe/create') !!}">Create new product</a>

                    <table>
                        @foreach($products as $product)
                            <tr>
                                <td>{!! $product->brand->name . ' ' . $product->title !!}</td>
                                @if((isset($product->category)))
                                    <td>{!! $product->category->name !!}</td>
                                @endif

                                <td>{!! $product->locale !!}</td>
                                <td>{!! $product->country !!}</td>

                                <td>
                                    {{ Form::open(array('url' => LaravelLocalization::localizeURL('/sitestripe/'.$product->id), 'class' => 'pull-right')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                                    {{ Form::close() }}
                                    <a class="btn btn-small btn-success" href="{{ URL::to('sitestripe/' . $product->id) }}">Show</a>
                                    <a class="btn btn-small btn-info" href="{{ URL::to('sitestripe/' . $product->id . '/edit') }}">Edit</a>
                                </td>
                                <td>&nbsp;</td>
                                <td>{{ Carbon::createfromformat('Y-m-d H:i:s', $product->updated_at)->format('d/m/Y H:i') }}</td>
                            </tr>
                        @endforeach
                    </table>

                    <a href="{!! URL::to('sitestripe/create') !!}">Create new product</a>

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->
    @include('layouts.partials.footer')

    </body>
</html>
