<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Homekit products</title>




    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/shop-homepage.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>
    <body>
    @include('layouts.partials.navigation')
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            @include('layouts.partials.categories')

            <div class="col-md-9">


                <div class="row">
                    {!!Html::ul($errors->all()) !!}
                    <H1>Create product</H1>
                    {!! Form::open(['url' =>  LaravelLocalization::localizeURL('/sitestripe') , 'files' => true]) !!}

                    <div class="form-group">
                        {!! Form::label('asin', 'Asin') !!}
                        {!! Form::text('asin', old('asin'), array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('title', 'Title') !!}
                        {!! Form::text('title', old('title'), array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('image', 'Image') !!}
                        {!! Form::text('image', old('image'), array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('url', 'Url') !!}
                        {!! Form::text('url', old('url'), array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('price', 'Price') !!}
                        {!! Form::text('price', old('price'), array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Description') !!}
                        {!! Form::textarea('description', old('description') ? old('description') : '<ul><li></li><li></li></ul>', array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {{ Form::label('brand', 'Brand') }}
                        {!! Form::select('brand', $brands, null, array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {{ Form::label('category', 'Category') }}
                        {!! Form::select('category', $categoriesForSelect, null, array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('publish', 'Publish') !!}
                        {!! Form::checkbox('publish', 1, 1) !!}
                    </div>






                {!! Form::submit('Create the product', array('class' => 'btn btn-primary')) !!}

                {!! Form::close() !!}

            </div>
        </div>

    </div>

    <!-- /.container -->
    @include('layouts.partials.footer-backend')

    </body>
</html>
