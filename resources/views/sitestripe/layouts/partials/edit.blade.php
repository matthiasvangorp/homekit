<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Edit {!! $product->title !!}</H1>
                {!! Form::model($product, ['url' => array(LaravelLocalization::localizeURL('/amazon_products/'.$product->id)), 'files' => true, 'method' => 'PUT']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('title', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::text('description', null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {{ Form::label('category', 'Category') }}
                    {!! Form::select('category', $categoriesList, $product->category_id, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {{ Form::label('brand', 'Brand') }}
                    {!! Form::select('brand', $brands, $product->brand_id, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('publish', 'Publish') !!}
                    {!! Form::checkbox('publish', 1, $product->publish) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('tread', 'Thread') !!}
                    {!! Form::checkbox('thread', 1, $product->thread) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('unavailable', 'Unavailable') !!}
                    {!! Form::checkbox('unavailable', 1, $product->unavailable) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Product Image') !!}
                    {!! Form::file('image', null) !!}
                </div>

                <div class="form-group">
                   
                    <img src="{!! $product->image !!}">
                </div>

                    {!! Form::submit('Update the product', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}

            </div>

        </div>

    </div>

</div>
<!-- /.container -->