<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>{!! trans('links.create_link') !!}</H1>
                {!! Form::open(['url' =>  LaravelLocalization::localizeURL('/links') , 'files' => true]) !!}

                <div class="form-group">
                    {!! Form::label('title', trans('links.title')) !!}
                    {!! Form::text('title', old('title'), array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('link', trans('links.link')) !!}
                    {!! Form::text('link', old('link'), array('class' => 'form-control')) !!}
                </div>

                {!! Form::submit(trans('links.create_link'), array('class' => 'btn btn-primary')) !!}

                {!! Form::close() !!}

                </div>
            </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container -->