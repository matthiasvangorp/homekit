<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-12">


            <div class="row">
                @foreach($links as $link)
                    <a href="{!! $link->link !!}" target="_blank">{!! $link->title !!}</a>
                    <hr/>
                @endforeach


            </div>

        </div>

    </div>

</div>
<!-- /.container -->