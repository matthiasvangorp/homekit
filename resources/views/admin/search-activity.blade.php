@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Search Activity</h1>

        <form action="{{ route('admin.search-activity') }}" method="GET" class="mb-4">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="date" class="form-control" id="start_date" name="start_date" value="{{ \Carbon\Carbon::parse($startDate)->format('Y-m-d') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="date" class="form-control" id="end_date" name="end_date" value="{{ \Carbon\Carbon::parse($endDate)->format('Y-m-d') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-primary form-control">Filter</button>
                    </div>
                </div>
            </div>
        </form>

        @if($searchQueries->isNotEmpty())
            <table class="table">
                <thead>
                <tr>
                    <th>Date and Time</th>
                    <th>Search Term</th>
                    <th>Country</th>
                    <th>City</th>
                    <th>Full URL</th>
                </tr>
                </thead>
                <tbody>
                @foreach($searchQueries as $query)
                    <tr>
                        <td>{{ \Carbon\Carbon::parse($query->timestamp)->format('d/m/Y H:i:s') }}</td>
                        <td>{{ $query->search_term }}</td>
                        <td>{{ $query->country }}</td>
                        <td>{{ $query->city }}</td>
                        <td><a href="{{ $query->url }}" target="_blank">{{ $query->url }}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p>No search activity found for the selected date range.</p>
        @endif
    </div>
@endsection