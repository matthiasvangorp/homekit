@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Most Popular Products by Country</h1>

        <form action="{{ route('admin.popular-products') }}" method="GET" class="mb-4">
            <div class="row">
                <div class="col-md-4">
                    <label for="start_date">Start Date:</label>
                    <input type="date" id="start_date" name="start_date" value="{{ \Carbon\Carbon::parse($startDate)->format('Y-m-d') }}" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="end_date">End Date:</label>
                    <input type="date" id="end_date" name="end_date" value="{{ \Carbon\Carbon::parse($endDate)->format('Y-m-d') }}" class="form-control">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary mt-4">Filter</button>
                </div>
            </div>
        </form>

        @foreach($popularProducts as $country => $products)
            <h2>{{ $country }}</h2>
            <table class="table">
                <thead>
                <tr>
                    <th>Product ID</th>
                    <th>Product Name</th>
                    <th>Total Clicks</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td><a href="/en/products/{{ $product->amazon_product_id }}">{{ $product->amazon_product_id }}</a></td>
                        <td>{{ \Illuminate\Support\Str::substr($product->amazonProduct->title ?? 'N/A', 0, 100) }} ...</td>
                        <td>{{ $product->total_clicks }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endforeach
    </div>
@endsection