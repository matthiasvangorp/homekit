@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Tracking Dashboard</h1>

        <form method="GET" action="{{ route('admin.analytics') }}" class="mb-4">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="date" class="form-control" id="start_date" name="start_date" value="{{ $startDate->format('Y-m-d') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="date" class="form-control" id="end_date" name="end_date" value="{{ $endDate->format('Y-m-d') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary mt-4">Filter</button>
                </div>
            </div>
        </form>

        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Total Visitors</h5>
                        <p class="card-text">{{ $totalVisitors }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Page Views</h5>
                        <p class="card-text">{{ $pageViews }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-6">
                <h2>Top Pages</h2>
                <ul class="list-group">
                    @foreach ($topPages as $page)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            {{ $page->url }}
                            <span class="badge bg-primary rounded-pill">{{ $page->total }}</span>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
                <h2>Top Referrers</h2>
                <ul class="list-group">
                    @foreach ($topReferrers as $referrer)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="{{ $referrer->referrer }}" target="_blank">{{ $referrer->referrer }}</a>
                            <span class="badge bg-primary rounded-pill">{{ $referrer->total }}</span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-3">
                <h2>Top Countries</h2>
                <ul class="list-group">
                    @foreach ($topCountries as $country)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            {{ $country->country }}
                            <span class="badge bg-primary rounded-pill">{{ $country->total }}</span>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3">
                <h2>Top Cities</h2>
                <ul class="list-group">
                    @foreach ($topCities as $city)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            {{ $city->location }}
                            <span class="badge bg-primary rounded-pill">{{ $city->total }}</span>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3">
                <h2>Top Browsers</h2>
                <ul class="list-group">
                    @foreach ($topBrowsers as $browser)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            {{ $browser->browser }}
                            <span class="badge bg-primary rounded-pill">{{ $browser->total }}</span>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3">
                <h2>Top Locales</h2>
                <ul class="list-group">
                    @foreach ($topLocales as $locale)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            {{ $locale->browser_locale }}
                            <span class="badge bg-primary rounded-pill">{{ $locale->total }}</span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Visitor Trend</h5>
                    <canvas id="visitorTrendChart"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row mt-4">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Top Cities</h5>
                    <canvas id="topCitiesChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Top Countries</h5>
                    <canvas id="topCountriesChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Top Referrers</h5>
                    <canvas id="topReferrersChart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <style>
        .table-responsive {
            max-height: 500px;
            overflow-y: auto;
        }
        .table th {
            position: sticky;
            top: 0;
            background-color: #f8f9fa;
            z-index: 1;
        }
    </style>
@endsection

@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const startDate = document.getElementById('start_date');
            const endDate = document.getElementById('end_date');

            startDate.addEventListener('change', function() {
                if (startDate.value > endDate.value) {
                    endDate.value = startDate.value;
                }
            });

            endDate.addEventListener('change', function() {
                if (endDate.value < startDate.value) {
                    startDate.value = endDate.value;
                }
            });
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const ctx = document.getElementById('visitorTrendChart').getContext('2d');
            const dailyVisitorData = @json($dailyVisitorData);

            new Chart(ctx, {
                type: 'line',
                data: {
                    labels: dailyVisitorData.map(item => item.date),
                    datasets: [{
                        label: 'Daily Visitors',
                        data: dailyVisitorData.map(item => item.visitors),
                        borderColor: 'rgb(75, 192, 192)',
                        tension: 0.1
                    }]
                },
                options: {
                    responsive: true,
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Function to create a pie chart
            function createPieChart(elementId, data, labelKey, valueKey) {
                const ctx = document.getElementById(elementId).getContext('2d');
                new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: data.map(item => item[labelKey]),
                        datasets: [{
                            data: data.map(item => item[valueKey]),
                            backgroundColor: [
                                '#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0', '#9966FF',
                                '#FF9F40', '#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0'
                            ]
                        }]
                    },
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'bottom',
                            },
                            tooltip: {
                                callbacks: {
                                    label: function(context) {
                                        const label = context.label || '';
                                        const value = context.parsed || 0;
                                        const total = context.dataset.data.reduce((acc, data) => acc + data, 0);
                                        const percentage = ((value / total) * 100).toFixed(1);
                                        return `${label}: ${value} (${percentage}%)`;
                                    }
                                }
                            }
                        }
                    }
                });
            }

            // Create Top Cities Chart
            createPieChart('topCitiesChart', @json($topCities), 'location', 'total');

            // Create Top Countries Chart
            createPieChart('topCountriesChart', @json($topCountries), 'country', 'total');

            // Create Top Referrers Chart
            createPieChart('topReferrersChart', @json($topReferrers), 'referrer', 'total');
        });
    </script>
@endpush