@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Product Clicks</h1>

        <form action="{{ route('admin.product-clicks') }}" method="GET" class="mb-4">
            <div class="row">
                <div class="col-md-4">
                    <label for="start_date">Start Date:</label>
                    <input type="date" id="start_date" name="start_date" value="{{ \Carbon\Carbon::parse($startDate)->format('Y-m-d') }}" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="end_date">End Date:</label>
                    <input type="date" id="end_date" name="end_date" value="{{ \Carbon\Carbon::parse($endDate)->format('Y-m-d') }}" class="form-control">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary mt-4">Filter</button>
                </div>
            </div>
        </form>

        <!-- Chart container -->
        <div class="mb-4">
            <canvas id="clicksChart"></canvas>
        </div>

        <!-- ... (filter form remains the same) ... -->

        @if($productClicks->isNotEmpty())
            <table class="table">
                <thead>
                <tr>
                    <th>Date and Time</th>
                    <th>User Session</th>
                    <th>Product ID</th>
                    <th>Product Name</th>
                    <th>Country</th>
                    <th>Click Count</th>
                    <th>Entry Page</th>
                    <th>Entry Referrer</th>
                    <th>Page Visits</th>
                </tr>
                </thead>
                <tbody>
                @php $previousDate = null; @endphp
                @foreach($productClicks as $click)
                    @php $currentDate = \Carbon\Carbon::parse($click->clicked_at)->addHours(2)->format('Y-m-d'); @endphp
                    @if($previousDate !== null && $currentDate !== $previousDate)
                        <tr><td colspan="8" style="height: 20px;"></td></tr>
                    @endif
                    <tr>
                        <td>{{ \Carbon\Carbon::parse($click->clicked_at)->addHours(2)->format('d/m/Y H:i:s') }}</td>
                        <td>
                            @if($click->user_session_id)
                                <a href="{{ route('admin.user-session.show', $click->user_session_id) }}">View Details</a>
                            @else
                                No session found
                            @endif
                        </td>
                        <td><a href="/en/products/{{ $click->amazon_product_id }}">{{ $click->amazon_product_id }}</a></td>
                        <td>{{ \Illuminate\Support\Str::substr($click->amazonProduct->title, 0, 100) ?? 'N/A' }} ...</td>
                        <td>{{ $click->country }}</td>
                        <td>{{ $click->click_count }}</td>
                        <td>{{ $click->entry_page }}</td>
                        <td>{{ $click->entry_referrer }}</td>
                        <td>
                            @if($click->page_visits)
                                <ul>
                                    @foreach($click->page_visits as $visit)
                                        <li>
                                            {{ \Carbon\Carbon::parse($visit->timestamp)->format('H:i:s') }} - {{ $visit->url }}
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                No page visits recorded
                            @endif
                        </td>
                    </tr>
                    @php $previousDate = $currentDate; @endphp
                @endforeach
                </tbody>
            </table>
        @else
            <p>No product clicks found for the selected date range.</p>
        @endif
    </div>

@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var ctx = document.getElementById('clicksChart').getContext('2d');
            var chartData = @json($chartData);
            var clicksChart = new Chart(ctx, {
                type: 'bar',
                data: chartData,
                options: {
                    responsive: true,
                    scales: {
                        x: {
                            title: {
                                display: true,
                                text: 'Date'
                            },
                            ticks: {
                                maxRotation: 45,
                                minRotation: 45
                            }
                        },
                        y: {
                            beginAtZero: true,
                            title: {
                                display: true,
                                text: 'Number of Clicks'
                            }
                        }
                    },
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        tooltip: {
                            mode: 'index',
                            intersect: false
                        }
                    }
                }
            });
        });
    </script>
@endpush