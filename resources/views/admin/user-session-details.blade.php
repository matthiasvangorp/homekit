@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>User Session Details</h1>
        <a href="{{ url()->previous() }}" class="btn btn-secondary mb-3">Back to Product Clicks</a>

        <h2>Session Information</h2>
        <table class="table">
            <tr>
                <th>Tracking ID</th>
                <td>{{ $userSession->tracking_id }}</td>
            </tr>
            <tr>
                <th>Entry Time</th>
                <td>{{ \Carbon\Carbon::parse($userSession->entry_time)->format('d/m/Y H:i:s') }}</td>
            </tr>
            <tr>
                <th>Entry Page</th>
                <td>{{ $userSession->entry_page }}</td>
            </tr>
            <tr>
                <th>Referrer</th>
                <td>{{ $userSession->referrer ?? 'N/A' }}</td>
            </tr>
            <tr>
                <th>IP Address</th>
                <td>{{ $userSession->ip_address }}</td>
            </tr>
            <tr>
                <th>Country</th>
                <td>{{ $userSession->country ?? 'N/A' }}</td>
            </tr>
            <tr>
                <th>City</th>
                <td>{{ $userSession->city ?? 'N/A' }}</td>
            </tr>
            <tr>
                <th>Browser Locale</th>
                <td>{{ $userSession->browser_locale ?? 'N/A' }}</td>
            </tr>
            <tr>
                <th>Browser</th>
                <td>{{ $userSession->browser ?? 'N/A' }}</td>
            </tr>
            <tr>
                <th>Device Type</th>
                <td>{{ $userSession->device_type ?? 'N/A' }}</td>
            </tr>
            <tr>
                <th>OS</th>
                <td>{{ $userSession->os ?? 'N/A' }}</td>
            </tr>
        </table>

        <h2>Product Clicks</h2>
        @if($productClicks->isNotEmpty())
            <table class="table">
                <thead>
                <tr>
                    <th>Time</th>
                    <th>Product ID</th>
                    <th>Product Name</th>
                    <th>Country</th>
                </tr>
                </thead>
                <tbody>
                @foreach($productClicks as $click)
                    <tr>
                        <td>{{ \Carbon\Carbon::parse($click->clicked_at)->format('d/m/Y H:i:s') }}</td>
                        <td><a href="/en/products/{{ $click->amazon_product_id }}">{{ $click->amazon_product_id }}</a></td>
                        <td>{{ \Illuminate\Support\Str::substr($click->amazonProduct->title ?? 'N/A', 0, 100) }} ...</td>
                        <td>{{ $click->country }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p>No product clicks recorded for this session.</p>
        @endif

        <h2>Page Visits</h2>
        @if($pageVisits->isNotEmpty())
            <table class="table">
                <thead>
                <tr>
                    <th>Time</th>
                    <th>URL</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pageVisits as $visit)
                    <tr>
                        <td>{{ \Carbon\Carbon::parse($visit->timestamp)->format('d/m/Y H:i:s') }}</td>
                        <td>{{ $visit->url }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p>No page visits recorded for this session.</p>
        @endif
    </div>
@endsection