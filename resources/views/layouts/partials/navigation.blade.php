<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{!! URL::to('/') !!}">{!! trans('messages.homekit_products') !!}</a>
        </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    @if ( Auth::guest() )
                    @else
                        <li><a href="#">Welkom, {!!   Auth::user()->name !!}</a></li>
                    @endif
                </ul>
                <!-- Add search form here -->
                <form class="navbar-form navbar-left center-search" action="{{ route('products.search') }}" method="GET">
                    <div class="form-group" style="display: inline-block;">
                        <div class="input-group">
                            <input type="text" name="query" class="form-control" placeholder="{{trans('search.placeholder') }}">
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">
                                <i class="fas fa-search"></i>
                            </button>
                        </span>
                        </div>
                    </div>
                </form>

                <!--div class="btn-group dropdown nav navbar-nav navbar-right">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        {!! \Illuminate\Support\Facades\Cookie::get('country') !!} <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        @foreach(config('countries') as $localeCode => $name)
                            <a rel="alternate" hreflang="{{ $localeCode }}" href="#"  class="countrySelector">
                                <li> {!! $name !!}</li>
                            </a>
                        @endforeach
                    </ul>
                </div-->
                <!--div class="btn-group dropdown nav navbar-nav navbar-right">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span class="lang-sm lang-lbl-en" lang="{{ LaravelLocalization::getCurrentLocale() }}"></span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}/">
                                <li><span class="lang-sm lang-lbl-en" lang="{{ $localeCode }}"></span></li>
                            </a>
                        @endforeach
                    </ul>
                </div-->
                <div class="btn-group dropdown nav navbar-nav navbar-right">
                    <button type="button" class="btn btn-default" id="btn-localization">
                        <span class="flag-icon flag-icon-{!! strtolower(\Illuminate\Support\Facades\Cookie::get('country')) !!}"></span>
                        {!! strtolower(\Illuminate\Support\Facades\Cookie::get('country')) !!}-{{ strtoupper(LaravelLocalization::getCurrentLocale()) }}
                    </button>
                </div>

            <!-- /.navbar-collapse -->
        </div>
    </div>
    <!-- /.container -->
</nav>