<div class="container">

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; {!! trans('messages.homekit_products') !!} {{ Carbon\Carbon::now()->format('Y') }} | <a href="http://www.works-with-alexa.eu" target="_blank">Works with Alexa</a></p>
            </div>
        </div>
    </footer>

</div>
<!-- modal -->
<div class="modal fade" id="localizationModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="modal-title">{!! trans('messages.language_and_country') !!}</h4>
            </div>
            <div class="modal-body" id="modal-body">
                {!! Form::open(['url' => '/', 'files' => true, 'method' =>  'POST']) !!}

                <div class="form-group">
                    {!! Form::Label('country', trans('messages.country')) !!}
                    <select class="form-control" name="country" id="country">
                        @foreach(config('countries') as $localeCode => $name)
                            @if ( \Illuminate\Support\Facades\Cookie::get('country') == $localeCode)
                                <option selected="selected" value="{!! $localeCode  !!} ">{!! $name !!}</option>
                            @else
                                <option value="{!! $localeCode  !!} ">{!! $name !!}</option>
                            @endif
                        @endforeach
                    </select>
                    <br/>
                    {!! Form::Label('language', trans('messages.language')) !!}
                    <select class="form-control" name="language" id="language">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            @if ( LaravelLocalization::getCurrentLocale() == $localeCode)
                                <option selected="selected" value="{!! LaravelLocalization::getLocalizedURL($localeCode, null, [], true)   !!} ">{!! $properties['name'] !!}</option>
                            @else
                                <option value="{!! LaravelLocalization::getLocalizedURL($localeCode, null, [], true)   !!} ">{!! $properties['name'] !!}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-localization" value="add">{!! trans('messages.save') !!}</button>
            </div>
        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.container -->

<meta name="_token" content="{!! csrf_token() !!}" />
<!-- jQuery 2-->
<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script src="{{ URL::asset('js/custom.js') }}"></script>


<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/b06f924ef7.js" crossorigin="anonymous"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>


<script src="{{ URL::asset('js/bootstrap-rating-input.js') }}"></script>
<script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

<link rel="stylesheet" href="{{ URL::asset('assets/css/shop-homepage.css') }}">
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-RCC2HB7L1S"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-RCC2HB7L1S');
</script>