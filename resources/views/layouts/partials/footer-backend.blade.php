<div class="container">

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; {!! trans('messages.homekit_products') !!} {!! \Carbon\Carbon::now()->format('Y') !!}</p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<meta name="_token" content="{!! csrf_token() !!}" />
<!-- jQuery 2-->
<script src="{{ URL::asset('js/jquery.js') }}"></script>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">


<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!-- Ajax -->
<script src="{{ URL::asset('js/ajax-crud.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-multiselect.js') }}"></script>

<script src="{{ URL::asset('js/bootstrap-rating-input.js') }}"></script>
<script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

<link rel="stylesheet" href="{{ URL::asset('assets/css/shop-homepage.css') }}">
<link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-multiselect.css') }}">
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-RCC2HB7L1S"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-RCC2HB7L1S');
</script>