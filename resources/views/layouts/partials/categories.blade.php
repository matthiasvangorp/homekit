<div class="col-md-3">
    <p class="lead">{!! __('messages.categories') !!}</p>
    <div class="list-group">
        @foreach($categories as $category)
        <a href="{{ URL::to('/') }}/{!! LaravelLocalization::getCurrentLocale() !!}/categories/{{$category->id}}" class="list-group-item">{!! trans('categories.'.strtolower(str_replace(' ', '_', $category->name))) !!}</a>
        @endforeach
    </div>
</div>