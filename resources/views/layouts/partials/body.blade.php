<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">
            @if(isset($categoryName))
                <h1>{!! trans('categories.homekit_compatible') !!} {!! strtolower(trans('categories.'.strtolower(str_replace(' ', '_', $categoryName))))!!}</h1>
            @endif
                <div class="d-flex justify-content-end align-items-start mb-3 select-div">
                <form method="GET" action="{{ $route }}" class="form-inline">
                        <label for="sort" class="mr-2">{!! __('messages.sort.by')!!}</label>
                        <select name="sort" id="sort" class="custom-select" onchange="this.form.submit()">
                            <option value="price_asc" {{ request('sort') == 'price_asc' ? 'selected' : '' }}>{!! __('messages.sort.price_asc') !!}</option>
                            <option value="price_desc" {{ request('sort') == 'price_desc' ? 'selected' : '' }}>{!! __('messages.sort.price_desc') !!}</option>
                            <option value="updated_desc" {{ request('sort') == 'updated_desc' || !request('sort') ? 'selected' : '' }}>{!! __('messages.sort.updated_desc') !!}</option>
                            <option value="updated_asc" {{ request('sort') == 'updated_asc' ? 'selected' : '' }}>{!! __('messages.sort.updated_asc') !!}</option>
                            <option value="name_asc" {{ request('sort') == 'name_asc' ? 'selected' : '' }}>{!! __('messages.sort.name_asc') !!}</option>
                            <option value="name_desc" {{ request('sort') == 'name_desc' ? 'selected' : '' }}>{!! __('messages.sort.name_desc') !!}</option>
                        </select>
                    </form>
                </div>
            <div class="row product-row">
                @foreach($products as $product)
                    @if(!$product->unavailable)
                        <a href=" {!! URL::to('/') !!}/{!! LaravelLocalization::getCurrentLocale() ."/products/".$product->id !!}">
                            <div class="col-sm-4 col-lg-4 col-md-4 product">
                                <div class="thumbnail">
                                    <img src="{!!$product->image!!}"
                                         alt="{!!$product->title!!}"/>
                                    <div class="caption">
                                        <h4>
                                            @if(!$product->unavailable)
                                                <a href=" {!! URL::to('/') !!}/{!! LaravelLocalization::getCurrentLocale() ."/products/".$product->id !!}">
                                            @endif
                                        </h4>
                                        <input type="number" name="rating" id="star_rating" class="rating"
                                               value="{!! $product->score !!}" data-readonly data-icon-lib="fa"
                                               data-active-icon="fa-star" data-inactive-icon="fa-star-o"/>
                                        @if(isset($product->brand))
                                            <h5 style="text-decoration: none;color:black;display: inline">{!! $product->brand->name !!}</h5>
                                        @endif
                                        <span style="text-decoration: none;color:black">{!! $product->title !!}</span>
                                        <br/>
                                        <p id="product-description">{!! Str::limit($product->description, 90)  !!}</p>
                                        @if($product->price < $product->getMaxPriceOverLastMonth())
                                            <div class="product-price-green">
                                                {!! $currency !!} {!! number_format($product->price, 2, ",", "."); !!}
                                            </div>
                                        @else
                                            <div class="product-price">
                                               {!! $currency !!} {!! number_format($product->price, 2, ",", "."); !!}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                @endforeach

            </div>

        </div>

    </div>
    <div class="row" style="text-align: center">
        {{ $products->appends(request()->query())->links() }}
    </div>
    @if(isset($categoryName))
        <div class="row seotext">
            <div class="col-lg-12">
                <p class="text-center">{!! trans('categories.seo.'.strtolower(str_replace(' ', '_', $categoryName)))!!}</p>
            </div>

        </div>
    @endif

</div>
<!-- /.container -->