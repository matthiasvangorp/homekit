<!-- Page Content -->
<div class="container">
    <h1>{!! __('messages.contact_us') !!}</h1>

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif

    {!! Form::open(['route'=>'contactus.store']) !!}

    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        {!! Form::label(__('messages.name')) !!}
        {!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=>__('messages.enter_name')]) !!}
        <span class="text-danger">{{ $errors->first('name') }}</span>
    </div>

    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
        {!! Form::label(__('messages.email')) !!}
        {!! Form::text('email', old('email'), ['class'=>'form-control', 'placeholder'=>__('messages.enter_email')]) !!}
        <span class="text-danger">{{ $errors->first('email') }}</span>
    </div>

    <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
        {!! Form::label(__(__('messages.message'))) !!}
        {!! Form::textarea('message', old('message'), ['class'=>'form-control', 'placeholder'=>__('messages.enter_message')]) !!}
        <span class="text-danger">{{ $errors->first('message') }}</span>
    </div>

    <div class="form-group">
        <button class="btn btn-success">{!! __('messages.contact_us') !!}</button>
    </div>

    {!! Form::close() !!}

</div>
<!-- /.container -->