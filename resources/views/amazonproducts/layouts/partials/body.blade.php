<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                <p>Number of German products : {!! $numberOfProducts !!}</p>
                <p>Number of Dutch products : {!! $dutchProductsCount !!}</p>
                <p>Number of French products : {!! $frenchProductsCount !!}</p>
                <p>Number of Spanish products : {!! $spanishProductsCount !!}</p>
                <p>Number of Italian products : {!! $italianProductsCount !!}</p>
                <p>Number of UK products : {!! $ukProductsCount !!}</p>
                <p>Number of US products : {!! $usProductsCount !!}</p>
                <a href="{!! URL::to('amazon_products/create') !!}">Create new product</a>

                <table>
                    @foreach($products as $product)
                        <tr>
                            <td>{!! $product->brand->name . ' ' . \Illuminate\Support\Str::substr($product->title, 0, 50) !!}
                                ... ({{$product->asin}})
                            </td>
                            @if((isset($product->category)))
                                <td>{!! $product->category->name !!}</td>
                            @endif

                            <td>
                                {{ Form::open(array('url' => LaravelLocalization::localizeURL('/amazon_products/'.$product->id), 'class' => 'pull-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                                {{ Form::close() }}
                                <a class="btn btn-small btn-success"
                                   href="{{ URL::to('amazon_products/' . $product->id) }}">Show</a>
                                <a class="btn btn-small btn-info"
                                   href="{{ URL::to('amazon_products/' . $product->id . '/edit') }}">Edit</a>
                                <a class="btn btn-small btn-info"
                                   href="{{ URL::to('producttranslations/' . $product->id . '/edit') }}">Translate</a>
                            </td>
                            @php
                                $otherProducts = \App\Models\AmazonProduct::where('asin', $product->asin)
                                                                   ->where('locale', '!=', 'DE')
                                                                   ->get();
                            @endphp
                            <td>@foreach ($otherProducts as $otherProduct)
                                    <a href="../{{strtolower($otherProduct->locale)}}/products/{{$otherProduct->id}}"> {{$otherProduct->locale}}</a>
                                @endforeach
                            </td>
                            <td>{{ Carbon::createfromformat('Y-m-d H:i:s', $product->updated_at)->format('d/m/Y H:i') }}</td>
                        </tr>
                    @endforeach
                </table>

                <a href="{!! URL::to('amazon_products/create') !!}">Create new product</a>

            </div>

        </div>

    </div>

</div>
<!-- /.container -->