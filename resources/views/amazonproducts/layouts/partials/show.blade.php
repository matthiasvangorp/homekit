<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">

            @if($product->unavailable)
                <div class="alert alert-warning">
                    <strong>{!! trans('products.attention') !!}</strong> {!! trans('products.unavailable_warning') !!}
                    <a href="{{ URL::to('/') }}/categories/{{$product->category->id}}">{!! strtolower($product->category->name) !!}</a>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <h1>{!! $product->brand->name !!} {!!$product->title !!}
                        @if (Auth::check())
                            <a href="{!! URL::to('products/' . $product->id . '/edit') !!}"><span
                                        class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                        @endif
                    </h1>
                    <h4>{!! $product->description !!}</h4>
                    <input type="number" name="rating" id="star_rating" class="rating"
                           value="{!! $product->score !!}" data-readonly data-icon-lib="fa"
                           data-active-icon="fa-star" data-inactive-icon="fa-star-o"/>
                    <br/>
                </div>
            </div>
            <div class="row">
                @if($product->thread)
                    <div class="col-md-4">
                        <img src="{{ URL::to('images/thread_logo.png') }}" alt="thread">
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-4">
                    @foreach($product->affiliateLinks as $affiliateLink)
                        @if ($affiliateLink->store == "bol")
                            <p>
                                <a href="{!! $affiliateLink->url !!}" target="_blank" class="referral_link bol">
                                    @if(is_null($affiliateLink->latestAffiliateLinkPrice()))
                                    &euro; {!! $affiliateLink->price !!} {!! trans('products.price_bol') !!}
                                    @else
                                    &euro; {!! $affiliateLink->latestAffiliateLinkPrice()->price !!} {!! trans('products.price_bol') !!}
                                    @endif
                                </a>
                                <a href="#" data-toggle="modal" data-target="#bolModal"><i class="fas fa-chart-bar"
                                                                                           title="{{__('products.price_history')}}"></i></a>
                            </p>
                        @endif

                        @if ($affiliateLink->store == "amazon")
                            <p>
                                <a href="{!! $affiliateLink->url !!}" target="_blank" class="referral_link amazon">
                                    @if(is_null($affiliateLink->latestAffiliateLinkPrice()))
                                        {!! render_currency($affiliateLink->currency) !!} {!! $affiliateLink->price !!} {!! trans('products.with')   !!} {!! render_amazon($affiliateLink->url) !!}
                                    @else
                                        {!! render_currency($affiliateLink->currency) !!} {!! $affiliateLink->latestAffiliateLinkPrice()->price !!} {!! trans('products.with')   !!} {!! render_amazon($affiliateLink->url) !!}
                                    @endif
                                </a>
                                @if($affiliateLink->country === "DE")
                                    <a href="#" data-toggle="modal" data-target="#amazonModal"><i
                                                class="fas fa-chart-bar"
                                                title="{{__('products.price_history')}}"></i></a>
                                @endif
                            </p>
                        @endif
                    @endforeach
                </div>
                <div class="col-md-8">
                    <img src="{{ URL::to('/photo/500x500/') }}/{{$product->image}}" alt="{{$product->title}}"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>
                    <h4>{!! trans('products.available_in') !!} :</h4>
                    @foreach($product->countries as $country)
                        {!! trans('countries.'.$country->code) !!} <br/>
                        @endforeach
                        </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <a href=" {!! URL::to('/') !!}/{!! LaravelLocalization::getCurrentLocale() ."/brands/".$product->brand->id !!}">{{__('brands.brands_title', ['brandname' => $product->brand->name])}}</a>
                    </p>
                </div>
            </div>
        </div>
    </div>


</div>


<!-- /.container -->