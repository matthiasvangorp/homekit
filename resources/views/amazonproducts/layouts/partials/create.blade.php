<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('layouts.partials.categories')

        <div class="col-md-9">


            <div class="row">
                {!!Html::ul($errors->all()) !!}
                <H1>Create product</H1>
                {!! Form::open(['url' =>  LaravelLocalization::localizeURL('/amazon_products') , 'files' => true]) !!}

                <div class="form-group">
                    {!! Form::label('asin', 'Asin') !!}
                    {!! Form::text('asin', old('asin'), array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {{ Form::label('brand', 'Brand') }}
                    {!! Form::select('brand', $brands, null, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::button('Add brand', array('class' => 'btn', 'id' => 'btn-add-brand')) !!}
                </div>





                    {!! Form::submit('Create the product', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}


                <!-- Modal (Pop up when add brand button clicked) -->
                    <div class="modal fade" id="modalBrands" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Create Brand</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(['url' => 'brands', 'id' => 'form-brands']) !!}

                                    <div class="form-group">
                                        {!! Form::label('brandName', 'Name') !!}
                                        {!! Form::text('brandName', old('brandName'), array('class' => 'form-control')) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('brandDescription', 'Description') !!}
                                        {!! Form::text('brandDescription', old('brandDescription'), array('class' => 'form-control')) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('brandLink', 'Url') !!}
                                        {!! Form::text('brandLink', old('brandLink'), array('class' => 'form-control')) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary ajaxbutton" id="btn-save-brand" value="add">Add brand</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container -->