<?php

return [
    'search_results_for' => 'Risultati di ricerca per',
    'placeholder' => 'Cerca prodotti...',
    'view_details' => 'Visualizza dettagli',
];
