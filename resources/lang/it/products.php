<?php

return [
    'price_bol' => 'su Bol.com',
    'price_coolblue' => 'su Coolblue',
    'price_amazon' => 'su Amazon',
    'price_conrad' => 'su Conrad',
    'with' => 'su',
    'homekit_product_guide' => 'Guida ai prodotti Homekit | Acquista prodotti compatibili con Apple Homekit',
    'more_info' => 'Più informazioni',
    'available_in' => 'Disponibile in',
    'and' => 'e',
    'unavailable' => 'Non disponibile',
    'attention' => 'Attenzione',
    'unavailable_warning' => 'Questo prodotto non è attualmente disponibile. Dai un\'occhiata ad altri',
    'price_history' => 'Storico prezzi',
    'tags' => 'Etichette',
    'products_tagged_with' => 'Prodotti etichettati con',
];
