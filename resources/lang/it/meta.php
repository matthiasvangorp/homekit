<?php

return [
    'homepage' => [
        'description' => "Una panoramica dei prodotti compatibili con Homekit in vendita al momento",
    ],
    'categories' => [
        'description' => "Una panoramica dei prodotti compatibili con Homekit nella categoria :category in vendita al momento",
    ],
];
