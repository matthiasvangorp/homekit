<?php

return [
    'homekit_products' => 'Prodotti Homekit',
    'categories' => 'Categorie',
    'message' => 'Messaggio',
    'contact_us' => 'Contattaci',
    'name' => 'Nome',
    'email' => 'Email',
    'enter_name' => 'Inserisci nome',
    'enter_email' => 'Inserisci email',
    'enter_message' => 'Inserisci messaggio',
    'homekit_products' => 'Prodotti Homekit',
    'about' => 'Informazioni',
    'contact' => 'Contatto',
    'links' => 'Collegamenti',
    'articles' => 'Articoli',
    'products' => 'Prodotti',
    'language_and_country' => 'Lingua e paese',
    'country' => 'Paese',
    'language' => 'Lingua',
    'save' => 'Salva',
    'close' => 'Chiudi',
    'sort'=> [
        'by' => 'Ordina per:',
        'price_asc' => 'Prezzo (dal più basso al più alto)',
        'price_desc' => 'Prezzo (dal più alto al più basso)',
        'updated_desc' => 'Data (dal più recente al più vecchio)',
        'updated_asc' => 'Data (dal più vecchio al più recente)',
        'name_asc' => 'Nome (dalla A alla Z)',
        'name_desc' => 'Nome (dalla Z alla A)',
    ]

];
