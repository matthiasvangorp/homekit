<?php

return [
    'search_results_for' => 'Resultados de búsqueda para',
    'placeholder' => 'Buscar productos...',
    'view_details' => 'Ver detalles',
];
