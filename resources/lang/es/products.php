<?php

return [
    'price_bol' => 'en Bol.com',
    'price_coolblue' => 'en Coolblue',
    'price_amazon' => 'en Amazon',
    'price_conrad' => 'en Conrad',
    'with' => 'en',
    'homekit_product_guide' => 'Guía de productos de Homekit | Compre productos compatibles con Apple Homekit',
    'more_info' => 'Más información',
    'available_in' => 'disponible en',
    'and' => 'y',
    'unavailable' => 'No disponible',
    'attention' => 'Atención',
    'unavailable_warning' => 'Este producto no está disponible en este momento. Pagar otro',
    'price_history' => 'Historial de precios',
    'tags' => 'Etiquetas',
    'products_tagged_with' => 'Productos etiquetados con',
];
