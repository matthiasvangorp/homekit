<?php

return [
    'homepage' => [
        'description' => 'Una descripción general de los productos compatibles con Homekit a la venta ahora mismo',
    ],
    'categories' => [
        'description' => 'Una descripción general de los productos de :category compatibles con Homekit a la venta ahora mismo',
    ],
];
