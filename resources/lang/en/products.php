<?php

return [
    'price_bol' => 'at Bol.com',
    'price_coolblue' => 'at Coolblue',
    'price_amazon' => 'at Amazon',
    'price_conrad' => 'at Conrad',
    'with' => 'at',
    'homekit_product_guide' => 'Homekit product guide | Buy Apple Homekit compatible products',
    'more_info' => 'More info',
    'available_in' => 'Available in',
    'and' => 'and',
    'unavailable' => 'Unavailable',
    'attention' => 'Attention',
    'unavailable_warning' => 'This product is currently unavailable. Checkout other',
    'price_history' => 'Price history',
    'tags' => 'Tags',
    'products_tagged_with' => 'Products tagged with',
];
