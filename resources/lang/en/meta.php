<?php

return [
  'homepage' => [
        'description' => 'An overview of homekit compatible products for sale right now',
  ],
  'categories' => [
    'description' => 'An overview of homekit compatible :category products for sale right now',
  ],
];
