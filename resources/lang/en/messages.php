<?php

return [
    'homekit_products' => 'Homekit products',
    'categories' => 'Categories',
    'message' => 'Message',
    'contact_us' => 'Contact us',
    'name' => 'Name',
    'email' => 'Email',
    'enter_name' => 'Enter name',
    'enter_email' => 'Enter email',
    'enter_message' => 'Enter message',
    'homekit_products' => 'Homekit products',
    'about' => 'About',
    'contact' => 'Contact',
    'links' => 'Links',
    'articles' => 'Articles',
    'products' => 'Products',
    'language_and_country' => 'Language and country',
    'country' => 'Country',
    'language' => 'Language',
    'save' => 'Save',
    'close' => 'Close',
    'sort'=> [
        'by' => 'Sort by:',
        'price_asc' => 'Price (Low to High)',
        'price_desc' => 'Price (High to Low)',
        'updated_desc' => 'Date (Newest to Oldest)',
        'updated_asc' => 'Date (Oldest to Newest)',
        'name_asc' => 'Name (A to Z)',
        'name_desc' => 'Name (Z to A)',
    ]

];
