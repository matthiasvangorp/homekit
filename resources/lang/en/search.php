<?php

return [
    'search_results_for' => 'Search Results for',
    'placeholder' => 'Search for products...',
    'view_details' => 'View details',
];
