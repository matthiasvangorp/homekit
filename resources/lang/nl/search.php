<?php

return [
    'search_results_for' => 'Zoekresultaten voor',
    'placeholder' => 'Zoek naar producten...',
    'view_details' => 'Details bekijken',
];
