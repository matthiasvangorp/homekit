<?php

return [
  'homepage' => [
        'description' => 'Een overzicht van HomeKit compatibele producten die je nu kan kopen.',
  ],
  'categories' => [
    'description' => 'Een overzicht van HomeKit compatibele :category die je nu kan kopen.',
  ],
];
