<?php

return [
    'price_bol' => 'bij Bol.com',
    'price_coolblue' => 'bij Coolblue',
    'price_amazon' => 'bij Amazon',
    'price_conrad' => 'bij Conrad',
    'with' => 'bij',
    'homekit_product_guide' => 'Homekit productgids | Apple HomeKit compatibele producten kopen',
    'more_info' => 'Meer info',
    'available_in' => 'Beschikbaar in',
    'and' => 'en',
    'unavailable' => 'Niet beschikbaar',
    'unavailable_warning' => 'Dit product is momenteel niet beschikbaar. Bekijk andere',
    'price_history' => 'Prijshistoriek',
    'tags' => 'Tags',
    'products_tagged_with' => 'Producten getagged met',
];
