<?php

return [
    'create_article' => 'Creeër Artikel',
    'edit_article' => 'Bewerk Artikel',
    'title' => 'Titel',
    'subtitle' => 'Subtitel',
    'article' => 'Artikel',
    'read_more' => 'Lees meer',
];
