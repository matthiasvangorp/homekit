<?php

return [
    'create_article' => 'Créer un article',
    'edit_article' => 'Modifier l\'article',
    'title' => 'Titre',
    'subtitle' => 'Sous-titre',
    'article' => 'Article',
    'read_more' => 'Lire plus',
];
