<?php

return [
    'price_bol' => 'chez Bol.com',
    'price_coolblue' => 'chez Coolblue',
    'price_amazon' => 'chez Amazon',
    'price_conrad' => 'chez Conrad',
    'with' => 'chez',
    'homekit_product_guide' => 'Guide du produit Apple Homekit',
    'more_info' => 'Meer info',
    'available_in' => 'Disponible en',
    'and' => 'et',
    'unavailable' => 'Indisponible',
    'unavailable_warning' => 'Ce produit est actuellement indisponible. Regardez d\'autres',
    'price_history' => 'Historique des prix',
    'tags' => 'Étiquettes',
    'products_tagged_with' => 'Produits étiquetés avec',

];
