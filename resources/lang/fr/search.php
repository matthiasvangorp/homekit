<?php

return [
    'search_results_for' => 'Résultats de recherche pour',
    'placeholder' => 'Rechercher des produits...',
    'view_details' => 'Voir les détails',
];
