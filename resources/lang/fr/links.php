<?php

return [
    'create_line' => 'Créer un lien',
    'edit_line' => 'Modifier le lien',
    'title' => 'Titre',
    'line' => 'Article',
];
