<?php

return [
  'homepage' => [
        'description' => 'Un aperçu des produits compatibles Homekit à vendre en ce moment',
  ],
  'categories' => [
    'description' => 'Un aperçu des :category compatibles Homekit à vendre en ce moment',
  ],
];
