<?php

return [
    'price_bol' => 'bei Bol.com',
    'price_coolblue' => 'bei Coolblue',
    'price_amazon' => 'bei Amazon',
    'price_conrad' => 'bei Conrad',
    'with' => 'bei',
    'homekit_product_guide' => 'Homekit Produktführer | Apple Homekit kompatible Produkte',
    'more_info' => 'Mehr Infos',
    'available_in' => 'Verfügbar in',
    'and' => 'und',
    'unavailable' => 'Nicht verfügbar',
    'attention' => 'Aufmerksamkeit',
    'unavailable_warning' => 'Dieses Produkt ist zur Zeit nicht verfügbar. Kasse andere',
    'price_history' => 'Preisentwicklung',
    'tags' => 'Schlagwörter',
    'products_tagged_with' => 'Produkte mit Schlagwort',
];
