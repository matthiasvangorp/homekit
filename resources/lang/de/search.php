<?php

return [
    'search_results_for' => 'Suchergebnisse für',
    'placeholder' => 'Produkte suchen...',
    'view_details' => 'Details anzeigen',
];
