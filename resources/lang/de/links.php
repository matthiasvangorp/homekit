<?php

return [
    'create_link' => 'Create Link',
    'edit_link' => 'Edit Link',
    'title' => 'Title',
    'link' => 'Link',
];
