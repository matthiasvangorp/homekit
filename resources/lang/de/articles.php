<?php

return [
    'create_article' => 'Create Article',
    'edit_article' => 'Edit Article',
    'title' => 'Title',
    'subtitle' => 'Subtitle',
    'article' => 'Article',
    'read_more' => 'Read more',
];
