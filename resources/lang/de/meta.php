<?php

return [
  'homepage' => [
        'description' => 'Eine Übersicht über Homekit kompatible Produkte zum Verkauf',
  ],
  'categories' => [
    'description' => 'Eine Übersicht über Homekit-kompatibel :category Produkte zum Verkauf jetzt',
  ],
];
